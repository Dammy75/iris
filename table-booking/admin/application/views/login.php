<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/dashtreme/demo/transparent-admin/vertical-layout/form-step-wizard.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 11:10:32 GMT -->
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>Iris Premiere- Table Booking</title>
  <!-- loader-->
  <link href="<?php echo base_url();?>assets/css/pace.min.css" rel="stylesheet"/>
  <script src="<?php echo base_url();?>assets/js/pace.min.js"></script>
  <!--favicon-->
  <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">
  <!-- jquery steps CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugins/jquery.steps/css/jquery.steps.css">
  <!-- simplebar CSS-->
  <link href="<?php echo base_url();?>assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="<?php echo base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Metismenu CSS-->
  <link href="<?php echo base_url();?>assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="<?php echo base_url();?>assets/css/app-style.css" rel="stylesheet"/>
  
</head>

<body class="bg-theme bg-theme3">

<!-- Start wrapper-->
 <div id="wrapper">

  

<!--Start topbar header-->

<!--End topbar header-->

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
     <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm">
		    <!--<h4 class="page-title">Remo Login</h4>-->
		    
	   </div>
	   
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-9">
          <div class="card">
            <div class="card-header text-uppercase">
             Login
            </div>
            <div class="card-body">
			
			 <form class="form-horizontal form-material" id="loginform" method="POST" action="<?php echo site_url('login/check_login'); ?>">
				
				<br/> 
				<h4 class="box-title m-b-0 text-center" style="color:red;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('error');?> <?php echo $this->session->flashdata('suspend');?> </h4>				
				<h4 class="box-title m-b-0 text-center" style="color:green;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('success');?> </h4>				
				   <h3 class="box-title m-b-20 text-center"><b>Sign In</b></h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input id="username" onkeyup="check();" style="text-transform:lowercase;" class="form-control" name="username" type="text" required="" placeholder="Username" autocomplete="off" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input id="passwords" onkeyup="check();" class="form-control" name="password" type="password" required="" placeholder="Password" autocomplete="off" />
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <div class="col-xs-12">
                            <button id="button" disabled class="btn btn-light btn-round px-5" name="submit" type="submit"><i class="icon-lock"></i>Log In</button>
                        </div>
                    </div>
					
                 </form>
				
              <!-- <form id="loginform" method="POST" action="<?php echo site_url('login/check_login'); ?>">
                  <div>
                    
                      <section>
                          <div class="form-group">
                              <label for="userName">User name *</label>
                              <input class="form-control" type="text" id="userName">
                          </div>
                          <div class="form-group">
                              <label for="password"> Password *</label>
                              <input type="text" class="form-control" id="password">
                          </div>
                          
                          <div class="form-group">
                              <label class="col-lg-12 control-label">(*) Mandatory</label>
                          </div>
						  
						   <div class="form-group">
            <button type="submit" class="btn btn-light btn-round px-5"><i class="icon-lock"></i> Login</button>
          </div>
                      </section>
                     </div>
              </form> -->
            </div>
          </div>
        </div>
      </div><!-- End Row-->

     
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<footer class="footer">
      <div class="container">
        <div class="text-center">
        <center>  Copyright © 2020 <a target="_blank" href="http://onevoicetranmedia.com/">Onevoice Transmedia</a></center>
        </div>
      </div>
    </footer>
	<!--End footer-->
	
   
  </div><!--End wrapper-->


  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="<?php echo base_url();?>assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- Metismenu js -->
  <script src="<?php echo base_url();?>assets/plugins/metismenu/js/metisMenu.min.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url();?>assets/js/app-script.js"></script>

  <!--Form Wizard-->
  <script src="<?php echo base_url();?>assets/plugins/jquery.steps/js/jquery.steps.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
  <!--wizard initialization-->
  <script src="<?php echo base_url();?>assets/plugins/jquery.steps/js/jquery.wizard-init.js"></script>
	<script>
setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
	setTimeout(function() {
            $('#timeout1').fadeToggle('slow');
            }, 3000);
			
function check()
	{
			var user_name=document.getElementById('username').value;
			var pass_word=document.getElementById('passwords').value;
			
			if(user_name.length > 0 && pass_word.length > 0)
				{
					document.getElementById('button').disabled = false;
				}
				else
				{
					document.getElementById('button').disabled = true;
				}
	}	
</script>
</body>

<!-- Mirrored from codervent.com/dashtreme/demo/transparent-admin/vertical-layout/form-step-wizard.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 11:10:43 GMT -->
</html>
