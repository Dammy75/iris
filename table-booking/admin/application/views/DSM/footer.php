<!--Start footer-->
	<footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2020 <a target="_blank" href="http://onboardideas.com/">OnBoard Ideas & Innovation</a>
        </div>
      </div>
    </footer>
	<!--End footer-->
  	 
  </div><!--End wrapper-->

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	
 <!-- simplebar js -->
  <script src="<?php echo base_url();?>assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- Metismenu js -->
  <script src="<?php echo base_url();?>assets/plugins/metismenu/js/metisMenu.min.js"></script>
  <!-- loader scripts -->
  <script src="<?php echo base_url();?>assets/js/jquery.loading-indicator.html"></script>
  <!-- Custom scripts -->
  <script src="<?php echo base_url();?>assets/js/app-script.js"></script>
  <!-- Chart js -->
  
  <script src="<?php echo base_url();?>assets/plugins/Chart.js/Chart.min.js"></script>
  <!-- Vector map JavaScript -->
  <script src="<?php echo base_url();?>assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- Easy Pie Chart JS -->
  <script src="<?php echo base_url();?>assets/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
  <!-- Sparkline JS -->
  <script src="<?php echo base_url();?>assets/plugins/sparkline-charts/jquery.sparkline.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/jquery-knob/excanvas.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/jquery-knob/jquery.knob.js"></script>
    
    <script>
        $(function() {
            $(".knob").knob();
        });
    </script>
  <!-- Index js -->
  <script src="<?php echo base_url();?>assets/js/index.js"></script>

  
</body>

</html>
