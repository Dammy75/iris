<?php include "dsmhead/head.php";?>
<?php include "dsmhead/header.php";?>
<?php include "dsmhead/sidebar.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">View All Events</h4>
		   
	   </div>
	   <div class="col-sm-3">
       
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
		
			
                    <tr>
                      <td>Event Title *</td>
                      <td><?php echo $singleevent[0]['title'];?></td>
                    </tr>
					
                     <?php
						$str = $singleevent[0]['speaker_name'];
						$myspeakerarray = explode(",",$str);
						$size = count($myspeakerarray);
						//print_r($mydocarray);die();
							for($i=0; $i<$size; $i++){
							?>
								<tr>
								  <th scope="row">Speaker <?php echo $i+1; ?></th>
								  <td><?php echo $myspeakerarray[$i]; ?></td>
								</tr>
							<?php } ?>
                    <tr>
                      <td>Event Date *</td>
                      <td><?php echo $singleevent[0]['date_time'];?></td>
                    </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
		<div class="col-lg-12">
			<h4 class="page-title">Doctors List</h4>
			<div class="card">
            <div class="card-body">
              <div class="table-responsive">
			  
                <table class="table">
                  <thead>
                    <tr>
					
                     <th scope="col">Sr. No</th>
                      <th scope="col">Attendee Name</th>
                    </tr>
                  </thead>
                  <tbody>
					  <?php
						$str = $singleevent[0]['event_send_names'];
						$mydocarray = explode(",",$str);
						$size = count($mydocarray);
						//print_r($mydocarray);die();
							for($i=0; $i<$size; $i++){
							?>
								<tr>
								  <th scope="row"><?php echo $i+1; ?></th>
								  <td><?php echo $mydocarray[$i]; ?></td>
								</tr>
							<?php } ?>
                  </tbody>
                </table>
           
              </div>
            </div>
          </div>
		</div>
      </div><!--End Row-->
	  
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
		
<?php include "dsmhead/footer.php";?>