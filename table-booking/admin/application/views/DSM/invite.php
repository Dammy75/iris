<!Doctype html>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

 <style>
 	body{
 		font-family:Lucida Calligraphy; 
 	}
 	#h{
 		background-image: url('./images/Slider1.png');
 		width: 100%;
    height: 80%;
    background-size: cover;
 	}
 </style>
</head>

<body>
	
<div id="h" class="container" style="border:1px solid black;margin-bottom:20px;">
		<img src="images/glenmark.png" class="mx-auto d-block" alt="Glenmark" style="margin-top:90px;">
	<p style="text-align: center;font-size:14px;">Dear Doctor,<br>
			In order to deliberate and discuss the optimum use of newer antidiabetic agents, <br>
			We invite you to an informative and interesting LIVE discussion at
	</p>
	<img src="images/nextgenlogo.png" alt="Nextgen" class="mx-auto d-block">

	<div class="row" style="margin:10px;">
	    <div class="col-sm" style="border:1px dotted;padding:10px;"><b>		Date:</b><br>      		
	      			<p style="color: red;">Monday, 
			<br>10th August 2020</p>
	    </div>
	    <div class="col-sm" style="border:1px dotted;padding:10px;"><b>	Time:
       		</b>
			<br><p style="color: red;">4 pm onwards</p>
		</div>

		<div class="col-md" style="border:1px dotted;padding:10px;"><b>    Link:</b><br><p style="color: red;">https://bit.ly/3fFm79C
			</p>
		</div>

		<div class="col-md" style="border:1px dotted;padding:10px;">
			 <b>Id:</b><p style="color: red;">86186085979</p>	
			 <b>Password:</b><p style="color: red;">Glenmark</p>
		</div>
    </div> 
    <p style="text-align: center;font-size:14px;"><b><u>Agenda</u></b><br>
		    Cardio Speaker :<b> Dr.Amit Soni, DNB Cardio</b>
		<br>Remogliflozin : Evolving Indian Real World Evidences & Experiences – INSPIRE INDIA study 
		<br>
		Diabeto Speaker :<b> Dr. B S Bhatia, MD, Diabeto</b>
		<br>Teneligliptin : Next Gliptin – Limiting Glycemic Variability
	</p> 
	<img src="images/footerlogo.png" alt="footer" class="mx-auto d-block" style="margin-bottom:40px;">
</div>

</body>
</html>
