<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
 <div class="card">
  <a href="<?php echo base_url();?>index.php/DSM/speaker_agreement"><button type="button" class="btn btn-gradient-info m-1 pull-right">Speaker List</button></a>
      <div class="card-header text-uppercase">Add Speaker</div>
       <div class="card-body">
         	<h4 class="box-title m-b-0 text-center" style="color:red;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('error');?> <?php echo $this->session->flashdata('suspend');?> </h4>				
				<h4 class="box-title m-b-0 text-center" style="color:#00FF00;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('success');?> </h4>

           <div class="row">
			<div class="col-lg-12">
			   
			   <div class="card">
			     <div class="card-body">
				   <form action="<?php echo base_url();?>index.php/DSM/insert_speaker" method="post" enctype="multipart/form-data">
					 <div class="form-group row">
					  <label for="input-21" class="col-sm-2 col-form-label">Speaker Name *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="speaker_name" id="speaker_name" placeholder="Enter Your Speaker Name" required>
					  </div>
					</div>
					<div class="form-group row">
					  <label for="input-22" class="col-sm-2 col-form-label">Headquarters *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="headquarter" id="headquarter" placeholder="Enter Your headquarter" required>
					  </div>
					</div>
					  <div class="form-group row">
						<label for="input-23" class="col-sm-2 col-form-label">Region *</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="region" id="region" placeholder="Enter Your region" required>
						</div>
					  </div>
					<div class="form-group row">
					  <label for="input-24" class="col-sm-2 col-form-label">Mobile No. *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Enter Mobile No." required>
					  </div>
					</div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Email Id *</label>
						<div class="col-sm-10">
						<input type="email" class="form-control" name="email" id="email" placeholder="Email Id" required>
						</div>
					  </div>
					  
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Medical Registration Number *</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="med_reg_no" id="med_reg_no" placeholder="Medical Registration No." required>
						</div>
					  </div>
					  
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">SBU Code *</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="sbu_code" id="sbu_code" placeholder="SBU Code" required>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Address</label>
						<div class="col-sm-10">
						<textarea class="form-control" name="address" id="address" placeholder="Address" required></textarea>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Government Doctor (Y/N) *</label>
						<div class="col-sm-10">
						<select class="form-control single-select" name="is_government_doctor" id="is_government_doctor">
                          <option value="yes" selected>Yes</option>
                          <option value="no">No</option>
                        </select>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Latest CV *</label>
						<div class="col-sm-10">
						<input type="file" class="form-control" name="latest_cv" id="latest_cv" placeholder="Latest CV" accept="image/*,.doc, .docx,.ppt, .pptx, .pdf" required>
							<p>(Only Word, PDF, PPT, Image are allowed)</p>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Photo of the Speaker *</label>
						<div class="col-sm-10">
						<input type="file" class="form-control" name="speaker_photo" id="speaker_photo" placeholder="Latest CV" accept="image/*, .pdf" required>
							<p>(Only PDF and JPEG are allowed)</p>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Cancelled Cheque</label>
						<div class="col-sm-10">
						<input type="file" class="form-control" accept="image/*, .pdf" name="cheque_photo" id="cheque_photo" placeholder="Latest CV">
							<p>(Only PDF and JPEG are allowed)</p>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">PAN Card</label>
						<div class="col-sm-10">
						<input type="file" class="form-control" accept="image/*, .pdf" name="pancard" id="pancard" placeholder="Latest CV">
							<p>(Only PDF and JPEG are allowed)</p>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">GST Declaration Letter</label>
						<div class="col-sm-10">
						<input type="file" class="form-control" accept="image/*, .pdf" name="gst" id="gst" placeholder="Latest CV">
							<p>(Only PDF and JPEG are allowed)</p>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Speciality / Qualification *</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="qualification" id="qualification" placeholder="Qualification" required>
						</div>
					  </div>
				   </div><!--End Row-->
	  
					  
					  
					 <div class="form-group row">
					  <label class="col-sm-2 col-form-label"></label>
					  <div class="col-sm-10">
						<button type="submit" name="edit_doctor" id="edit_doctor" class="btn btn-white px-5"> Add</button>
					  </div>
					</div>
					</form>
				 </div>
			 </div>


       	</div>
	
    
       </div>
     </div>
 </div>  
	  
		

      <!--End Dashboard Content-->
    <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
	
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php // include "footer.php";?>
		<script>
setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
	setTimeout(function() {
            $('#timeout1').fadeToggle('slow');
            }, 3000);
			

</script>