<?php //echo '<pre>';print_r($data);// echo $title;?>
<!DOCTYPE html>  
<html lang="en">
<head>
    <title><?php //echo '<pre>';print_r($data['0']);// echo $title;?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php // echo base_url();?>assets/plugins/images/favicon.ico">
    <script src="<?php // echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
    <table border="1" width="100%" align="center" style="padding:10px 10px">
        <thead>
            <tr><th colspan="3" style="background-color:#ff0000";><h1 style="color:#fff" ><b>Ahmednagar Cyclothone 2020</b></h1></th>
            </tr>
            <tr>
            <th colspan="3">
                <p align="left"> You Have Successfully Registered For the Ahmednagar Cyclothone 2020.</p>   
                <p align="left">Your Registration Invoice No, Date and BIB No. are mentioned below:</p>   
            </th>
            </tr>
            <tr>
            <th ><h5><b>Invoice No: <?php  echo '<br>'.$data['0']['txnId'];?></b></h5></th>
            <th ><h5><b>Invoice Date: <?php  echo '<br>'.date('d-M-Y');?></b></h5></th>
            <th ><h5><b>BIB No: <?php  echo '<br>'.$data['0']['bib_id'];?></b></h5></th>
           
            </tr>
        </thead>
    </table>
    <table width="100%" align="center" style="padding:10px 10px">
        <tr>
            <td>
                <address>
                <h3>To,</h3>
                <h4 ><?php  echo $data['0']['name'];?>,</h4>
                <p ><?php  echo $data['0']['address'].','.$data['0']['city'].','.$data['0']['state'];?></p>
                </address>
            </td>
            <td align="right">
                <address>
                    <p><img src="<?php  echo base_url();?>web_asset/images/cyclothonlogo2020.png" alt="cyclothne" title="cyclothne" height="80px" alt="home" /></p>
                  <h4 > <b>Ahmednagar Cyclothone</b></h4>
                  <p>S.No 103, Ekta Colony, <br/>
                    Kedgaon Devi Road, <br/>
                    Ahmednagar - 414005
                 </p>
                </address>
            </td>
        </tr>
    </table>
                
    <table border="1" width="100%" align="center" style="padding:10px 10px">
        <thead>
            <tr>
                <th width="5%">#</th>
                <th width="55%">Race Category</th>
                <th width="15%">No. of Registration</th>
                <th width="25%">Amount</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="5%">1</td>
                <td width="55%">
                        Category - <?php  echo strtoupper($data['0']['category']);?>  <br>
                  
                </td>
                <td width="15%" align="center">1 </td>
               
                <td width="25%" align="center"> <?php  if($data['0']['category'] =='20KM'){ echo '700';}else if($data['0']['category'] =='50KM'){ echo '800';}else{echo '1000';}?> </td>
            </tr>
            <tr align="right">
                <td colspan="3" ><b>Subtotal:</b> </td>
                <td align="center"> <?php  if($data['0']['category'] =='20KM'){ echo '700';}else if($data['0']['category'] =='50KM'){ echo '800';}else{echo '1000';}?> </td>
            </tr>
            <tr align="right">
                <td colspan="3" ><b>Payment method:</b> </td>
                <td align="center"> Online Payment</td>
            </tr>
            <tr align="right">
                <td colspan="3" ><b>Total:</b> </td>
                <td  align="center"> <?php  if($data['0']['category'] =='20KM'){ echo '700';}else if($data['0']['category'] =='50KM'){ echo '800';}else{echo '1000';}?> </td>
            </tr>
        </tbody>
      
    </table>

    

    <table width="100%" align="center" style="padding:10px 10px">
        <tr>
            
            <td align="right">
                <address>
                    <h5 class="font-bold">Ahmednagar Cyclothone,</h5>
                    <p class="text-muted">Authorised Signatory</p>
                </address>
            </td>
        </tr>
    </table>
    <hr width="100%" align="center">
    <table width="100%" align="center">
        <tr>
            <td>This is a computer generated Invoice and does not require the signature. If you have any question regarding this invoice please email us at support@nagarcycling.com</td>
        </tr>
    </table>
<!-- /#wrapper -->
</body>
</html>
