<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/dashtreme/demo/transparent-admin/vertical-layout/form-advanced.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 11:10:18 GMT -->
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>NEXTGENMEET E2E</title>
  <!-- loader-->
  <link href="<?php echo base_url();?>assets/css/pace.min.css" rel="stylesheet"/>
  <script src="<?php echo base_url();?>assets/js/pace.min.js"></script>
  <!--favicon-->
  <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">
  <!--text editor-->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/summernote/dist/summernote-bs4.css"/> 
  <!--Select Plugins-->
  <link href="<?php echo base_url();?>assets/plugins/select2/css/select2.min.css" rel="stylesheet"/>
  <!--inputtags-->
  <link href="<?php echo base_url();?>assets/plugins/inputtags/css/bootstrap-tagsinput.css" rel="stylesheet" />
  <!--multi select-->
  <link href="<?php echo base_url();?>assets/plugins/jquery-multi-select/multi-select.css" rel="stylesheet" type="text/css">
  <!--Bootstrap Datepicker-->
  <link href="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
  <!--Touchspin-->
  <link href="<?php echo base_url();?>assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css">
  <!-- Dropzone css -->
  <link href="<?php echo base_url();?>assets/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css">
  <!-- simplebar CSS-->
  <link href="<?php echo base_url();?>assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="<?php echo base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Metismenu CSS-->
  <link href="<?php echo base_url();?>assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="<?php echo base_url();?>assets/css/app-style.css" rel="stylesheet"/>
<?php include "dsmhead/header.php";?>
<?php include "dsmhead/sidebar.php";?>
<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Create Event</h4>
		   
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <button type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-cog mr-1"></i><a href="<?php echo site_url('Eventdsm/view_events');?>"> View All Events</a></button>
        
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
	<!-- End Breadcrumb-->
			<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>

  
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header text-uppercase">Select Inputs</div>
            <div class="card-body">
              <form id="eventcreate" method="POST" action="<?php echo site_url('Eventdsm/insert_event'); ?>">
                	<div class="form-group row">
			  <label for="basic-select" class="col-sm-2 col-form-label">Event Title </label>
			<div class="col-sm-10">
            <input type="text" id="eventtitle" name="eventtitle" readonly class="form-control form-control-rounded" id="input-26" value="Nextgen Meet">
            <div class="help-block with-errors"></div>
			</div>
		</div>
		
		
		
		<div class="form-group row">
		  <label for="basic-select" class="col-sm-2 col-form-label" data-error="Business Category required" required>Select Speaker1</label>
		  <div class="col-sm-10">
		<!--  <select class="form-control" id="selUser">
		<option value='0'>-- Select Speaker --</option>
					
			</select>-->
			
			<select class="form-control" name="speakerlist" id="speakerlist">
			<!----- Displaying fetched cities in options using foreach loop ---->
			<?php foreach($speaker as $speakers){?>
			<option value="<?php echo $speakers['name']; ?>"><?php echo $speakers['name']; ?></option>
			<?php } ?>
			</select>
			<div class="help-block with-errors"></div>
		  </div>
		</div>
		
			<div class="form-group row">
		  <label for="basic-select" class="col-sm-2 col-form-label" data-error="Business Category required" required>Select Speaker2</label>
		  <div class="col-sm-10">
		<!--  <select class="form-control" id="selUser">
		<option value='0'>-- Select Speaker --</option>
					
			</select>-->
			
			<select class="form-control" name="speakerlist2" id="speakerlist2">
			<!----- Displaying fetched cities in options using foreach loop ---->
				<?php foreach($speaker as $speakers){?>
			<option value="<?php echo $speakers['name']; ?>"><?php echo $speakers['name']; ?></option>
			<?php } ?>
			</select>
			<div class="help-block with-errors"></div>
		  </div>
		</div>
			
		  <div class="form-group row">
			<label class="col-sm-2 col-form-label">Event Time</label>
			<div class="col-sm-10">
			<input type="datetime-local" id="eventtime" name="eventtime" class="form-control" required>
			<div class="help-block with-errors"></div>
			</div>
		  </div>
          
            
          <div class="form-group row">
            <label for="input-26" class="col-sm-2 col-form-label">DSM Code</label>
            <div class="col-sm-10">
            <input type="text" id="manager_one_code" readonly name="manager_one_code" value="<?php echo $this->session->userdata('username'); ?>" class="form-control form-control-rounded"  placeholder="Enter Your Code" required>
            <div class="help-block with-errors"></div>
			</div>
          </div>

            <div class="form-group">
              <label>Select Doctor</label><br>
              <label>Select Prescriber Doctor List</label>
              
              <select class="form-control multiple-select" multiple="multiple" name="doctor2[]" id="doctor2" required>
                <?php 
                    //    echo'<pre>';
                    //	print_r($prescribers[0]['doctor_name']);
                    //	die();
                foreach($prescribers as $key) {?>
        			<option value="<?php echo $key['doctor_name']; ?>"><?php echo $key['doctor_name']; ?></option>
        			<?php } ?>
			
			
               
              </select>
            </div>
            <br><br><br><br>
          <div class="form-group">
              <label>Select Doctor</label><br>
              <label>Select Non-Prescriber Doctor List</label>
              
              <select class="form-control multiple-select" multiple="multiple" name="doctor1[]" id="doctor1" required>
                <?php 
                    //    echo'<pre>';
                    //	print_r($prescribers[0]['doctor_name']);
                    //	die();
                foreach($nonprescribers as $key) {?>
        			<option value="<?php echo $key['doctor_name']; ?>"><?php echo $key['doctor_name']; ?></option>
        			<?php } ?>
			
			
               
              </select>
            </div>
				<br><br><br><br>	
					
					
					
 <div class="form-group text-center m-t-20">
			<div class="col-xs-12">
				<button id="button" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" name="submit" id="submit" type="submit">Add</button>
			</div>
		</div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->
	  
	 

    
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
		<footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2020 OnBoard Ideas & Innovation
        </div>
      </div>
    </footer>
	
   
  </div><!--End wrapper-->


  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="<?php echo base_url();?>assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- Metismenu js -->
  <script src="<?php echo base_url();?>assets/plugins/metismenu/js/metisMenu.min.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url();?>assets/js/app-script.js"></script>

  <!--Bootstrap Touchspin Js-->
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-touchspin/js/bootstrap-touchspin-script.js"></script>
    <!-- Dropzone JS  -->
    <script src="<?php echo base_url();?>assets/plugins/dropzone/js/dropzone.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/summernote/dist/summernote-bs4.min.js"></script>
	<script>
	   $('#summernoteEditor').summernote({
				height: 400,
				tabsize: 2
			});
	 </script>
    <!--Select Plugins Js-->
    <script src="<?php echo base_url();?>assets/plugins/select2/js/select2.min.js"></script>
    <!--Inputtags Js-->
    <script src="<?php echo base_url();?>assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>

    <!--Bootstrap Datepicker Js-->
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script>
      $('#default-datepicker').datepicker({
        todayHighlight: true
      });
      $('#autoclose-datepicker').datepicker({
        autoclose: true,
        todayHighlight: true
      });

      $('#inline-datepicker').datepicker({
         todayHighlight: true
      });

      $('#dateragne-picker .input-daterange').datepicker({
       });

    </script>

    <!--Multi Select Js-->
    <script src="<?php echo base_url();?>assets/plugins/jquery-multi-select/jquery.multi-select.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/jquery-multi-select/jquery.quicksearch.js"></script>
   
    
    <script>
        $(document).ready(function() {
            $('.single-select').select2();
      
            $('.multiple-select').select2();

        //multiselect start

            $('#my_multi_select1').multiSelect();
            $('#my_multi_select2').multiSelect({
                selectableOptgroup: true
            });

           

         $('.custom-header').multiSelect({
              selectableHeader: "<div class='custom-header'>Selectable items</div>",
              selectionHeader: "<div class='custom-header'>Selection items</div>",
              selectableFooter: "<div class='custom-header'>Selectable footer</div>",
              selectionFooter: "<div class='custom-header'>Selection footer</div>"
            });


          });

    </script>
	
	
	<!--Form Validatin Script-->
    <script src="<?php echo base_url();?>assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
    <script>

    $().ready(function() {

    $("#eventcreate").validate();

   // validate signup form on keyup and submit
    $("#signupForm").validate({
        rules: {
            firstname: "required",
            lastname: "required",
            username: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
             contactnumber: {
                required: true,
                minlength: 10
            },
            topic: {
                required: "#newsletter:checked",
                minlength: 2
            },
            agree: "required"
        },
        messages: {
            firstname: "Please enter your firstname",
            lastname: "Please enter your lastname",
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: "Please enter a valid email address",
            contactnumber: "Please enter your 10 digit number",
            agree: "Please accept our policy",
            topic: "Please select at least 2 topics"
        }
    });

});

    </script>
	
</body>

<!-- Mirrored from codervent.com/dashtreme/demo/transparent-admin/vertical-layout/form-advanced.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 11:10:31 GMT -->
</html>
