<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
   
    <div class="brand-logo">
	  <img src="<?php echo base_url();?>/assets/images/logo-icon.png" class="logo-icon" alt="logo icon">
	  <h5 class="logo-text">NEXTGEN MEET</h5>
	  <div class="close-btn"><i class="zmdi zmdi-close"></i></div>
   </div>
	  
     <ul class="metismenu" id="menu">
		<li>
		  <a href="<?php echo site_url('Event/dashboard');?>">
			<div class="parent-icon"><i class="zmdi zmdi-view-dashboard"></i></div>
			<div class="menu-title">Dashboard</div>
		  </a>
		</li>

		<li>
		  <a href="<?php echo site_url('Event/pending_meeting');?>">
			<div class="parent-icon"> <i class='zmdi zmdi-dot-circle-alt'></i></div>
		   <div class="menu-title">Pending-Meetings</div>
		  </a>
		</li>
		
		<li>
		  <a href="<?php echo site_url('Event/scheduled_meeting');?>">
			<div class="parent-icon"> <i class='zmdi zmdi-grid'></i></div>
		   <div class="menu-title">Scheduled-Meetings</div>
		  </a>
		</li>
	 </ul>
   
</div>