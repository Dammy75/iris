<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>Event Team - Pending Meeting</title>
  <!-- loader-->
  <link href="<?php echo base_url();?>/assets/css/pace.min.css" rel="stylesheet"/>
  <script src="<?php echo base_url();?>/assets/js/pace.min.js"></script>
  <!--favicon-->
  <link rel="icon" href="<?php echo base_url();?>/assets/images/favicon.ico" type="image/x-icon">
  <!-- simplebar CSS-->
  <link href="<?php echo base_url();?>/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>/assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="<?php echo base_url();?>/assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="<?php echo base_url();?>/assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Metismenu CSS-->
  <link href="<?php echo base_url();?>/assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="<?php echo base_url();?>/assets/css/app-style.css" rel="stylesheet"/>
 
</head>

<body class="bg-theme bg-theme1">

<!-- Start wrapper-->
 <div id="wrapper">

    <!--Start sidebar-wrapper-->
    <?php include_once('sidebar.php');?>
    <!--End sidebar-wrapper-->

    <!--Start topbar header-->
    <?php include_once('header.php');?>
    <!--End topbar header-->

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <?php if(empty($row['link'])) {?>
          <h4 class="page-title">Event Approved List</h4>
        <?php }else{?>
          <h4 class="page-title">Event Meeting Scheduled List</h4>
        <?php }?>
        <h6 class="box-title m-b-0 text-center" style="color:red;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('error');?> <?php echo $this->session->flashdata('suspend');?> </h6>				
        <h6 class="box-title m-b-0 text-center" style="color:green;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('success');?> </h6>				
		</div>
	   <div class="col-sm-3">
      
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <form action="<?php echo site_url('Event/create_meeting')?>" method="POST" name="create_meeting">
                  <table class="table">
                      <tr>
                        <td>Event Title *</td>
                        <td><?php echo $row['title']; ?></td>
                      </tr>
                      <tr>
                        <td>Speaker Name *</td>
                        <td><?php echo $row['speaker_name']; ?></td>
                      </tr>
                      <tr>
                        <td>Event Date *</td>
                        <td><?php echo date('d-M-Y g:iA', strtotime($row['date_time'])); ?></td>
                      </tr>
                      <tr>
                        <td>App Link *</td>
                        <td><?php echo '<input type="text" class="form-control" name="link" placeholder="Type App Link" value="'.$row['link'].'" required>'; ?></td>
                      </tr>
                      <tr>
                        <td>App ID </td>
                        <td><?php echo '<input type="text" class="form-control" name="app_username" placeholder="Type App Username" value="'.$row['app_username'].'">'; ?></td>
                      </tr>
                      <tr>
                        <td>App Password </td>
                        <td><?php echo '<input type="text" class="form-control" name="app_pass" placeholder="Type App Password" value="'.$row['app_pass'].'">'; ?></td>
                      </tr>
                      <tr>
                        <td colspan="2"><button type="submit" value="submit" class="btn btn-gradient-info m-1">Update</button></td>
                        <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                        <input type="hidden" name="action" value="update">
                      </tr>            
                  </table>
                </form>
              </div>
            </div>
          </div>
        </div>
		<div class="col-lg-12">
			<h4 class="page-title">Doctors List</h4>
			<div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Doctors List</th>
                      <!-- <th scope="col">Action</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      $rows = explode(',',$row['event_send_names']);
                      $x=1;
                      foreach($rows as $doc){
                    ?>
                    <tr>
                      <th scope="row"><?php echo $x;?></th>
                      <td><?php echo $doc;?></td>
                      <!-- <td><a href="#" class="btn btn-light btn-block">Invite</a></td> -->
                    </tr>
                    <?php $x++; }?>
                  </tbody>
                </table>
           
              </div>
            </div>
          </div>
		</div>
      </div><!--End Row-->
	  
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
    <!-- Start footer -->
    <?php include_once('footer.php');?>
	  <!-- End footer -->
	
   <!--start color switcher-->
   <div class="right-sidebar">
    <div class="switcher-icon">
      <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
    </div>
    <div class="right-sidebar-content">

      <p class="mb-0">Gaussion Texture</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme1"></li>
        <li id="theme2"></li>
        <li id="theme3"></li>
        <li id="theme4"></li>
        <li id="theme5"></li>
        <li id="theme6"></li>
      </ul>

      <p class="mb-0">Gradient Background</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme7"></li>
        <li id="theme8"></li>
        <li id="theme9"></li>
        <li id="theme10"></li>
        <li id="theme11"></li>
        <li id="theme12"></li>
		    <li id="theme13"></li>
        <li id="theme14"></li>
        <li id="theme15"></li>
      </ul>
      
     </div>
   </div>
  <!--end color switcher-->
	
  </div><!--End wrapper-->
  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>/assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/popper.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="<?php echo base_url();?>/assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- Metismenu js -->
  <script src="<?php echo base_url();?>/assets/plugins/metismenu/js/metisMenu.min.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url();?>/assets/js/app-script.js"></script>

  <!-- Chart js -->
  <script src="<?php echo base_url();?>/assets/plugins/Chart.js/Chart.min.js"></script>
  <!-- Index2 js -->
  <script src="<?php echo base_url();?>/assets/js/dashboard-property-listing.js"></script>
  <script>
    setTimeout(function() {
        $('#timeout').fadeToggle('slow');
    }, 3000);
	  setTimeout(function() {
        $('#timeout1').fadeToggle('slow');
    }, 3000);
  </script>
</body>
</html>
