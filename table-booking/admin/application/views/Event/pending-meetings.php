<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>Event Team - Pending Meeting</title>
  <!-- loader-->
  <link href="<?php echo base_url();?>/assets/css/pace.min.css" rel="stylesheet"/>
  <script src="<?php echo base_url();?>/assets/js/pace.min.js"></script>
  <!--favicon-->
  <link rel="icon" href="<?php echo base_url();?>/assets/images/favicon.ico" type="image/x-icon">
  <!-- simplebar CSS-->
  <link href="<?php echo base_url();?>/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>/assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!--Data Tables -->
  <link href="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <!-- animate CSS-->
  <link href="<?php echo base_url();?>/assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="<?php echo base_url();?>/assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Metismenu CSS-->
  <link href="<?php echo base_url();?>/assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="<?php echo base_url();?>/assets/css/app-style.css" rel="stylesheet"/>
  
</head>

<body class="bg-theme bg-theme1">

<!-- Start wrapper-->
 <div id="wrapper">

    <!--Start sidebar-wrapper-->
    <?php include_once('sidebar.php');?>
    <!--End sidebar-wrapper-->

    <!--Start topbar header-->
    <?php include_once('header.php');?>
    <!--End topbar header-->

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Pending Meeting</h4>
            <h5 class="box-title m-b-0 text-center" style="color:black;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('error');?> <?php echo $this->session->flashdata('suspend');?> </h5>				
            <h5 class="box-title m-b-0 text-center" style="color:white;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('success');?> </h5>				
				
		    <!-- <ol class="breadcrumb"> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Dashtreme</a></li> -->
            <!-- <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li> -->
            <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
         </ol>
	   </div>
	   <div class="col-sm-3">
       <!-- <div class="btn-group float-sm-right"> -->
        <!-- <button type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-cog mr-1"></i> Setting</button> -->
        <!-- <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown"> -->
        <!-- <span class="caret"></span> -->
        <!-- </button> -->
        <!-- <div class="dropdown-menu"> -->
          <!-- <a href="javaScript:void();" class="dropdown-item">Action</a> -->
          <!-- <a href="javaScript:void();" class="dropdown-item">Another action</a> -->
          <!-- <a href="javaScript:void();" class="dropdown-item">Something else here</a> -->
          <!-- <div class="dropdown-divider"></div> -->
          <!-- <a href="javaScript:void();" class="dropdown-item">Separated link</a> -->
        <!-- </div> -->
      <!-- </div> -->
     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr No.</th>
                        <th>Event<br> Title</th>
                        <th>Speaker<br> Name</th>
                        <th>DSM<br> Code</th>
                        <th>DSM<br> Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Approved<br> by ZSM</th>
                        <th>Approved<br> by SM</th>
                        <th>Approved<br> by Admin</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $x=1; foreach($pending as $row) {?>
                    <tr>
                        <td><?php echo $x; ?></td>
                        <td><?php echo $row['title']; ?></td>
                        <td><?php echo $row['speaker_name']; ?></td>
                        <td><?php echo $row['manager_code']; ?></td>
                        <td><?php echo $row['manager_one_name']; ?></td>
                        <td><?php echo date('d-M-Y', strtotime($row['date_time'])); ?></td>
                        <td><?php echo date('g:iA', strtotime($row['date_time'])); ?></td>
                        <td><?php echo ($row['approved_by_zsm']=='1') ? 'Yes' : ''; ?></td>
                        <td><?php echo ($row['approved_by_sm']=='1') ? 'Yes' : ''; ?></td>
                        <td><?php echo ($row['approved_by_admin']=='1') ? 'Yes' : ''; ?></td>
                        <td><a href="<?php echo site_url('Event/view_meeting/'.$row['id'])?>"><button type="button" class="btn btn-gradient-info m-1">View</button></a></td>
                    </tr>
                    <?php $x++; }?>
                </tbody>
                <!-- <tfoot>
                    <tr>
                        <th>Sr No.</th>
                        <th>Event<br> Title</th>
                        <th>Speaker<br> Name</th>
                        <th>DSM<br> Code</th>
                        <th>DSM<br> Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Approved<br> by ZSM</th>
                        <th>Approved<br> by SM</th>
                        <th>Action</th>
                    </tr>
                </tfoot> -->
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!-- Start footer -->
    <?php include_once('footer.php');?>
	<!-- End footer -->
	
	<!--start color switcher-->
   <div class="right-sidebar">
    <div class="switcher-icon">
      <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
    </div>
    <div class="right-sidebar-content">

      <p class="mb-0">Gaussion Texture</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme1"></li>
        <li id="theme2"></li>
        <li id="theme3"></li>
        <li id="theme4"></li>
        <li id="theme5"></li>
        <li id="theme6"></li>
      </ul>

      <p class="mb-0">Gradient Background</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme7"></li>
        <li id="theme8"></li>
        <li id="theme9"></li>
        <li id="theme10"></li>
        <li id="theme11"></li>
        <li id="theme12"></li>
		<li id="theme13"></li>
        <li id="theme14"></li>
        <li id="theme15"></li>
      </ul>
      
     </div>
   </div>
  <!--end color switcher-->
   
  </div><!--End wrapper-->


  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>/assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/popper.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="<?php echo base_url();?>/assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- Metismenu js -->
  <script src="<?php echo base_url();?>/assets/plugins/metismenu/js/metisMenu.min.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url();?>/assets/js/app-script.js"></script>

  <!--Data Tables js-->
  <script src="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
      } );

    setTimeout(function() {
        $('#timeout').fadeToggle('slow');
    }, 3000);
	setTimeout(function() {
        $('#timeout1').fadeToggle('slow');
    }, 3000);

    </script>
	
</body>

<!-- Mirrored from codervent.com/dashtreme/demo/transparent-admin/vertical-layout/table-data-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 11:12:17 GMT -->
</html>
