<!--Start footer-->
	<footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2020 <a target="_blank" href="http://onboardideas.com/">OnBoard Ideas & Innovation</a>
        </div>
      </div>
    </footer>
	<!--End footer-->
	
	
   <!--start color switcher-->
   <div class="right-sidebar">
    <div class="switcher-icon">
      <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
    </div>
    <div class="right-sidebar-content">

      <p class="mb-0">Gaussion Texture</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme1"></li>
        <li id="theme2"></li>
        <li id="theme3"></li>
        <li id="theme4"></li>
        <li id="theme5"></li>
        <li id="theme6"></li>
      </ul>

      <p class="mb-0">Gradient Background</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme7"></li>
        <li id="theme8"></li>
        <li id="theme9"></li>
        <li id="theme10"></li>
        <li id="theme11"></li>
        <li id="theme12"></li>
		<li id="theme13"></li>
        <li id="theme14"></li>
        <li id="theme15"></li>
      </ul>
      
     </div>
   </div>
  <!--end color switcher-->
	
  </div><!--End wrapper-->
  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="<?php echo base_url();?>assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- Metismenu js -->
  <script src="<?php echo base_url();?>assets/plugins/metismenu/js/metisMenu.min.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url();?>assets/js/app-script.js"></script>

  <!--Bootstrap Touchspin Js-->
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-touchspin/js/bootstrap-touchspin-script.js"></script>
    <!-- Dropzone JS  -->
    <script src="<?php echo base_url();?>assets/plugins/dropzone/js/dropzone.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/summernote/dist/summernote-bs4.min.js"></script>
	<script>
	   $('#summernoteEditor').summernote({
				height: 400,
				tabsize: 2
			});
	 </script>
    <!--Select Plugins Js-->
    <script src="<?php echo base_url();?>assets/plugins/select2/js/select2.min.js"></script>
    <!--Inputtags Js-->
    <script src="<?php echo base_url();?>assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>

    <!--Bootstrap Datepicker Js-->
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script>
      $('#default-datepicker').datepicker({
        todayHighlight: true
      });
      $('#autoclose-datepicker').datepicker({
        autoclose: true,
        todayHighlight: true
      });

      $('#inline-datepicker').datepicker({
         todayHighlight: true
      });

      $('#dateragne-picker .input-daterange').datepicker({
       });

    </script>

    <!--Multi Select Js-->
    <script src="<?php echo base_url();?>assets/plugins/jquery-multi-select/jquery.multi-select.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/jquery-multi-select/jquery.quicksearch.js"></script>
    
    
</body>

<!-- Mirrored from codervent.com/dashtreme/demo/transparent-admin/vertical-layout/form-advanced.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 11:10:31 GMT -->
</html>
