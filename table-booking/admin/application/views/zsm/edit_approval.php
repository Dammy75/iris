<?php include "header.php";?>

<div class="clearfix"></div>
  
  <div class="content-wrapper">
    <div class="container-fluid">
    <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Edit Approval</h4>
     </div>
       
      
     </div>
    <!-- End Breadcrumb-->
  	<h4 class="box-title m-b-0 text-center" style="color:red;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('error');?> <?php echo $this->session->flashdata('suspend');?> </h4>				
				<h4 class="box-title m-b-0 text-center" style="color:#00FF00;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('success');?> </h4>
     <div class="row">
			<div class="col-lg-12">
			   <?php //echo '<pre>'; print_r($results);?>
			   <div class="card">
			     <div class="card-body">
				   <form action="<?php echo base_url();?>index.php/ZSM/change_status_zsm" method="post" enctype="multipart/form-data">
				       <div class="form-group row">
						<label for="input-25" class="col-sm-4 col-form-label">Event Title</label>
						<div class="col-sm-8">
						    <input readonly type="text" class="form-control" value="<?php echo $results[0]['title'];?>">
    						
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-4 col-form-label">Event Title</label>
						<div class="col-sm-8">
						    <input readonly type="text" class="form-control" value="<?php echo $results[0]['speaker_name'];?>">
    						
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-4 col-form-label">Event Approval by ZSM *</label>
						<div class="col-sm-5">
						    <input type="hidden" name="event_id" id="event_id" value="<?php echo $results[0]['id'];?>">
    						<select class="form-control single-select" name="approved_by_zsm" id="approved_by_zsm">
                                <option <?php if($results[0]['approved_by_zsm']=='1'){ echo 'selected'; } ?> value="yes" >Yes</option>
                                <option <?php if($results[0]['approved_by_zsm']=='0'){ echo 'selected'; } ?> value="no">No</option>
                            </select>
						</div>
						<div class="col-sm-3"></div>
					  </div>
					    
				  <button type="submit" name="edit_zsm_approval" id="edit_zsm_approval" class="btn btn-gradient-info m-1"><i class="icon-lock"></i> Edit</button>
					</form>
				 </div>
			 </div>


       	</div>
	 </div><!--End Row-->
<!--start overlay-->
    <div class="overlay"></div>
  <!--end overlay-->
    </div>
    <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
  
 <?php include "footer.php";?>
 <script>
setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
	setTimeout(function() {
            $('#timeout1').fadeToggle('slow');
            }, 3000);
			

</script>