<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/dashtreme/demo/transparent-admin/vertical-layout/dashboard-property-listing.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 11:08:00 GMT -->
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>NextGenMeets</title>
  <!-- loader-->
  <link href="<?php echo base_url();?>assets/css/pace.min.css" rel="stylesheet"/>
  <script src="<?php echo base_url();?>assets/js/pace.min.js"></script>
  <!--favicon-->
  <link rel="icon" href="<?php echo base_url();?>assets/images/logo-icon.png" type="image/x-icon">
  <!-- simplebar CSS-->
  <link href="<?php echo base_url();?>assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="<?php echo base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Metismenu CSS-->
  <link href="<?php echo base_url();?>assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="<?php echo base_url();?>assets/css/app-style.css" rel="stylesheet"/>
 
</head>

<body class="bg-theme bg-theme1">

<!-- Start wrapper-->
 <div id="wrapper">

 <?php include "sidebar.php"; ?>

<!--Start topbar header-->
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top">
 
	 <div class="toggle-menu">
		 <i class="zmdi zmdi-menu"></i>
	 </div>
	 <div class="search-bar flex-grow-1">
		 <div class="input-group">
		  <div class="input-group-prepend search-arrow-back">
			  <button class="btn btn-search-back" type="button"><i class="zmdi zmdi-long-arrow-left"></i></button>
			</div>
			<input type="text" class="form-control" placeholder="search">
			<div class="input-group-append">
			  <button class="btn btn-search" type="button"><i class="zmdi zmdi-search"></i></button>
			</div>
		  </div>
	  </div>
     
   <ul class="navbar-nav align-items-center right-nav-link ml-auto">
	<li class="nav-item dropdown search-btn-mobile">
		<a class="nav-link position-relative" href="javascript:void();">
		  <i class="zmdi zmdi-search align-middle"></i>
		</a>
	 </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret position-relative" data-toggle="dropdown" href="javascript:void();">
        <span class="user-profile"><img src="<?php echo base_url();?>assets/images/avatars/avatar-13.png" class="img-circle" alt="user avatar"></span>
      </a>
      <ul class="dropdown-menu dropdown-menu-right">
       <li class="dropdown-item user-details">
        <a href="javaScript:void();">
           <div class="media">
             <div class="avatar"><img class="align-self-start mr-3" src="<?php echo base_url();?>assets/images/avatars/avatar-13.png" alt="user avatar"></div>
            <div class="media-body">
            <h6 class="mt-2 user-title"><?php echo $_SESSION['username'];?></h6>
            <p class="user-subtitle"><?php echo $_SESSION['user_email'];?></p>
            </div>
           </div>
          </a>
        </li>
        <li class="dropdown-item"><a href="<?php echo base_url();?>index.php/login/logout"><i class="zmdi zmdi-power mr-3"></i>Logout</a></li>
      </ul>
    </li>
  </ul>
</nav>
</header>
<!--End topbar header-->