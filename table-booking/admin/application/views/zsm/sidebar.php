<!--Start sidebar-wrapper-->
   <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
   
    <div class="brand-logo">
	  <img src="<?php echo base_url();?>assets/images/logo-icon.png" class="logo-icon" alt="logo icon">
	  <h5 class="logo-text">ZSM</h5>
	  <div class="close-btn"><i class="zmdi zmdi-close"></i></div>
   </div>
	  
     <ul class="metismenu" id="menu">
		<li>
		  <a class="" href="<?php echo base_url();?>index.php/ZSM/dashboard">
			<div class="parent-icon"><i class="zmdi zmdi-view-dashboard"></i></div>
			<div class="menu-title">Dashboard</div>
		  </a>
		</li>
		<li>
		  <a class="" href="<?php echo base_url();?>index.php/ZSM/doctor_list">
			<div class="parent-icon"><i class="fa fa-list zmdi-view-dashboard"></i></div>
			<div class="menu-title">Doctor List</div>
		  </a>
		</li>
		<li>
		  <a class="" href="<?php echo base_url();?>index.php/ZSM/speaker_agreement">
			<div class="parent-icon"><i class="fa fa-volume-control-phone zmdi-view-dashboard"></i></div>
			<div class="menu-title">Speaker Agreement</div>
		  </a>
		</li>
		<li >
		  <a class="" href="<?php echo base_url();?>index.php/ZSM/event">
			<div class="parent-icon"><i class="fa fa-codepen zmdi-view-dashboard"></i></div>
			<div class="menu-title">Event List</div>
		  </a>
		</li>
		<li>
		  <a class="" href="<?php echo base_url();?>index.php/ZSM/event_approved_list">
			<div class="parent-icon"><i class="fa fa-list zmdi-view-dashboard"></i></div>
			<div class="menu-title">Event Approved List</div>
		  </a>
		</li>
		<li>
		  <a class="" href="<?php echo base_url();?>index.php/ZSM/event_attended_list">
			<div class="parent-icon"><i class="fa fa-list zmdi-view-dashboard"></i></div>
			<div class="menu-title">Event Attended List</div>
		  </a>
		</li>
		<li>
		  <a class="" href="<?php echo base_url();?>index.php/ZSM/event_final_list">
			<div class="parent-icon"><i class="fa fa-list zmdi-view-dashboard"></i></div>
			<div class="menu-title">Event Final List</div>
		  </a>
		</li>
		
		
		
	   </ul>
   
   </div>
   <!--End sidebar-wrapper-->