<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">

      <!--Start Dashboard Content-->
 <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">View Speakers Data</h5>
              <a href="<?php echo base_url();?>index.php/ZSM/add_speaker"><button type="button" class="btn btn-gradient-info m-1 pull-right">Add Speakers</button></a>
        <div class="table-responsive">
               <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Speaker Name</th>
                      <th scope="col">Mobile Number</th>
                      <th scope="col">Email</th>
                      <th scope="col">Credential</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $i=1; 
                      foreach ($results as $row) {
                      ?>  
                    <tr>
                      <th scope="row"><?php echo $i++;?></th>
                      <td><?php echo $row['name'];?></td>
                      <td><?php echo $row['contact'];?></td>
                      <td><?php echo $row['email'];?></td>
                      <td><?php echo $row['credential_qualification'];?></td>
                      <td><a href="<?php echo base_url();?>index.php/ZSM/edit_speaker/<?php echo $row['id'];?>"><button type="button" class="btn btn-gradient-info m-1">Edit</button></a></td>
                    </tr>
                  <?php }?>
                  
                  </tbody>
                </table>
            </div>
            </div>
          </div>
        </div>
 </div>  
	  
		

      <!--End Dashboard Content-->
    <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
	
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>