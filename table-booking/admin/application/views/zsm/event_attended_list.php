<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Event Attended List</h4>
	   </div>
	   <div class="col-sm-3">
       
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">View Event</h5>
              <div class="table-responsive">
                 <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Sr. No</th>
                      <th scope="col">Event Title</th>
                      <th scope="col">Speaker Name</th>
                      <th scope="col">Approved by ZSM</th>
                      <th scope="col">Approved by SM</th>
					  <th scope="col">Date</th>
                      <th scope="col">Time</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
				  <?php //print_r($event_attended_list);exit; 
					foreach($event_attended_list as $key){ 
			          $speakers=$key['speaker_name']; 
					  $speakers = explode(",",$speakers);
					  $speaker1 = $speakers[0];
					 // $speaker2 = $speakers[1];
			          $date=$key['date_time']; 
					  $date_time = explode(" ",$date);
					  $date = $date_time[0];
					  $time = $date_time[1];
					  $zsm_val=$this->common_model->get_records('event','',array('id'=>$key['id']),'');
					  //print_r($zsm_val[0]['approved_by_zsm']);exit;
			  ?>
                  <tbody>
                    <tr>
					  <td><?php echo $i=1;?></td>
					  <td><?php echo $key['title'];?></td>
                      <td><?php echo $speaker1; ?></td>
                      <td><?php echo "Approved"; ?></td>
                      <td><?php echo "Approved"; ?></td>
                      <td><?php echo $date;?></td>
                      <td><?php echo $time;?></td>
					  <td><a class="btn btn-gradient-info m-1" href="<?php echo base_url();?>index.php/ZSM/view_attended_event/<?php echo $key['id']?>">View</a></td>
                    </tr>
                  </tbody>
				  <?php $i++; } ?>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
	  
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>