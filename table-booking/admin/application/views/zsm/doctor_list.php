<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Doctor List</h4>
		   
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <button type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-plus mr-1"></i><a href="<?php echo base_url();?>index.php/ZSM/add_doctor"> Add New</a></button>
        
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">View Doctors Data</h5>
              <div class="table-responsive">
               <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Code</th>
                      <th scope="col">Name</th>
                      <th scope="col">Zone</th>
                    <!--   <th scope="col">SBU Code</th> -->
                      <th scope="col">Mobile Number</th>
                      <th scope="col">Email</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php
                         $username = $this->session->userdata('username'); 
                      $rs = $this->db->query("select manager_one_zone from manager_one where manager_two_code='$username'");
                      $array = $rs->result_array();
                      $zone = @$array[0]['manager_one_zone'];
                      $i = 1;
                     foreach ($results as $row) {
                      ?>  
                    <tr>
                      <th scope="row"><?php echo $i++;?></th>
                      <td><?php echo $row['manager_one_code'];?></td>
                      <td><?php echo $row['doctor_name'];?></td>
                     <td><?php echo $zone;?></td>
                    <!--   <td><?php //echo $row['sbu_code'];?></td> -->
                      <td><?php echo $row['contact'];?></td>
                      <td><?php echo $row['email'];?></td>
                      <td><a href="<?php echo base_url();?>index.php/ZSM/edit_doctor/<?php echo $row['id'];?>"><button type="button" class="btn btn-gradient-info m-1">Edit</button></a></td>
                    </tr>
                  <?php }?>
                  </tbody>
                  <tfoot>
                      <tr>
                          <td colspan="7" class="text-left">
                            <span class="pagination"><?php echo $links; ?></span>
                          </td>                        
                      </tr>
                    </tfoot>   
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
	  
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>