<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-SM-9">
		    <h4 class="page-title">Event Approved List</h4>
		   
	   </div>
	   <div class="col-SM-3">
      
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
			  <?php //print_r($approved_record);exit;
					foreach($approved_record as $key){
					  $speakers=$key['speaker_name']; 
					  $speakers = explode(",",$speakers);
					  $speaker1 = $speakers[0];
					  $speaker2 = $speakers[1];
			  ?>
                <table class="table">
                    <tr>
                      <td>Event Title *</td>
                      <td><?php echo $key['title'];?></td>
                    </tr>
                    <tr>
                      <td>Speaker 1 *</td>
                      <td><?php echo $speaker1;?></td>
                    </tr>
                    <tr>
                      <td>Speaker 2 *</td>
                      <td><?php echo $speaker2;?></td>
                    </tr>
                    <tr>
                      <td>Meeting App Name *</td>
                      <td><?php echo $key['meeting_app_name'];?></td>
                    </tr>
					<tr>
                      <td>Link *</td>
                      <td><?php echo $key['link'];?></td>
                    </tr>
					<tr>
                      <td>ID *</td>
                      <td><?php echo $key['app_username'];?></td>
                    </tr>
					<tr>
                      <td>Password *</td>
                      <td><?php echo $key['app_pass'];?></td>
                    </tr>
					
                </table>
				<?php } ?>
              </div>
            </div>
          </div>
        </div>
		<div class="col-lg-12">
			<h4 class="page-title">Doctors List</h4>
			<div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Speaker Name</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
                      <td><a href="#" class="btn btn-gradient-info m-1">Invite</a></td>
                    </tr>
                    <tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td><a href="#" class="btn btn-gradient-info m-1">Invite</a></td>
                    </tr>
                    <tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td><a href="#" class="btn btn-gradient-info m-1">Invite</a></td>
                    </tr>
					<tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td><a href="#" class="btn btn-gradient-info m-1">Invite</a></td>
                    </tr>
					<tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td><a href="#" class="btn btn-gradient-info m-1">Invite</a></td>
                    </tr><tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td><a href="#" class="btn btn-gradient-info m-1">Invite</a></td>
                    </tr><tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td><a href="#" class="btn btn-gradient-info m-1">Invite</a></td>
                    </tr><tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td><a href="#" class="btn btn-gradient-info m-1">Invite</a></td>
                    </tr><tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td><a href="#" class="btn btn-gradient-info m-1">Invite</a></td>
                    </tr>
					<tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td><a href="#" class="btn btn-gradient-info m-1">Invite</a></td>
                    </tr>
                  </tbody>
                </table>
           
              </div>
            </div>
          </div>
		</div>
      </div><!--End Row-->
	  
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>