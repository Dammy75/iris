<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Event List</h4>
		   
	   </div>
	   <div class="col-sm-3">
       <!--<a href="<?php echo base_url();?>index.php/sm/add_event" class="btn btn-light btn-block">Add Event</a>-->
     </div>
     </div>
    <!-- End Breadcrumb-->
    
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Export View Data</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                      <th scope="col">Sr. Id</th>
                      <th scope="col">Event Title</th>
                      <th scope="col">Speaker 1</th>
					  <!--<th scope="col">Speaker 2</th>-->
                      <th scope="col">Approved by SM</th>
					  <th scope="col">Approved by ZSM</th>
                      <th scope="col">Date</th>
                      <th scope="col">Time</th>
                      <th scope="col">Action</th>
                    </tr>
                </thead>
                <?php //print_r($event_list);exit;
                   $i=1; 
					foreach($event_list as $key){ 
			          $speakers=$key['speaker_name']; 
					  $speakers = explode(",",$speakers);
					  $speaker1 = $speakers[0];
					  //$speaker2 = $speakers[1];
			          $date=$key['date_time']; 
					  $date_time = explode(" ",$date);
					  $date = $date_time[0];
					  $time = $date_time[1];
					  $zsm_val=$this->common_model->get_records('event','',array('id'=>$key['id']),'');
					  //print_r($zsm_val[0]['approved_by_zsm']);exit;
			  ?>
                <tbody>
				 <tr>
					  <td><?php echo $i++; ?></td>
                      <td><?php echo $key['title'];?></td>
                      <td><?php echo $speaker1; ?></td>
                     <td readonly><?php if($zsm_val[0]['approved_by_sm']==0){ echo "Not Approved"; }else {echo "Approved";} ?></td>
                      <td readonly><?php if($zsm_val[0]['approved_by_zsm']==0){ echo "Not Approved"; }else {echo "Approved";} ?></td>
                      <td><?php echo $date;?></td>
                      <td><?php echo $time;?></td>
                      <td><a href="<?php echo base_url();?>index.php/SM/edit_approval/<?php echo $key['id'];?>"><button type="button" class="btn btn-gradient-info m-1">Edit</button></a></td>
                    </tr>
                </tbody>
                <tfoot>
                    
                </tfoot>
                <?php  } ?>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    
    
    
       <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	<?php include "footer.php";?>
	
	<script>
/*	$("#approved_by_sm").change(function(){
  alert("The text has been changed.");
});*/
	
	
	function cTrig(clickedid,val)
	{
		//alert(clickedid);
		// alert(val);
		 var data = {
						'id': clickedid,
						'approved_by_sm' : val
					};
				//console.log(data);
		 $.ajax(
				{
					type: "POST",
					url: '<?php echo base_url(); ?>index.php/sm/change_status',
					data: data,
					success:function(data)
					{
						//console.log(data);
						if(data=="success")
						{
							alert('SM Approval status changed successfully');
							//window.location.href='<?php echo base_url(); ?>index.php/sm/event';
						}else{
							window.location.href='<?php echo base_url(); ?>index.php/sm/event';

						}
					}
				}
			  );
	}
	
	
  $(function() 
  {
	$('.xxx').change(function() 
	{
		//alert($(this).prop('checked'));
		var data = {
						'id': $(this).val(),
						'approved_by_sm' : $(this).prop('checked')
					};
		$.ajax(
				{
					type: "POST",
					url: '<?php echo base_url(); ?>index.php/sm/change_status',
					data: data
				}
			  );
	})
  })
	
  $(function() 
  {
	$('.xxx').change(function() 
	{
		//alert($(this).prop('checked'));
		var data = {
						'id': $(this).val(),
						'approved_by_sm' : $(this).prop('checked')
					};
		$.ajax(
				{
					type: "POST",
					url: '<?php echo base_url(); ?>index.php/sm/change_status',
					data: data
				}
			  );
	})
  })
  function cTrig1(clickedid,val)
	{
		//alert(clickedid);
		//alert(val);
		 var data = {
						'id': clickedid,
						'approved_by_zsm' : val
					};
				//console.log(data);
		 $.ajax(
				{
					type: "POST",
					url: '<?php echo base_url(); ?>index.php/zsm/change_status_zsm',
					data: data,
					success:function(data)
					{
						//console.log(data);
						if(data=="success")
						{
							alert('ZSM Approval status changed successfully');
							//window.location.href='<?php echo base_url(); ?>index.php/sm/event';
						}else{
							window.location.href='<?php echo base_url(); ?>index.php/sm/event';

						}
					}
				}
			  );
	}
	
	
  $(function() 
  {
	$('.xxxx').change(function() 
	{
		//alert($(this).prop('checked'));
		var data = {
						'id': $(this).val(),
						'approved_by_zsm' : $(this).prop('checked')
					};
		$.ajax(
				{
					type: "POST",
					url: '<?php echo base_url(); ?>index.php/zsm/change_status_zsm',
					data: data
				}
			  );
	})
  })
</script>