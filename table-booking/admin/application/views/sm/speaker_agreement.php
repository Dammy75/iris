<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Speaker Agreement</h4>
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
		
        <a href="<?php echo base_url();?>index.php/sm_zsm/add_speaker" class="btn btn-light btn-block">Add New</a>
        
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Responsive Table</h5>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Speaker Name</th>
                      <th scope="col">Mobile No</th>
                      <th scope="col">Email Id</th>
                      <th scope="col">Credential</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Dr Harsh parekh</td>
                      <td>9100944127</td>
                      <td>endoinfo2018@gmail.com</td>
                      <td>DNB (Endocrinologist)</td>
                      <td><a href="<?php echo base_url();?>index.php/sm_zsm/edit_speaker" class="btn btn-light btn-block">Edit</a></td>
                    </tr>
                    <tr>
                      <th scope="row">1</th>
                      <td>Dr Harsh parekh</td>
                      <td>9100944127</td>
                      <td>endoinfo2018@gmail.com</td>
                      <td>DNB (Endocrinologist)</td>
                      <td><a href="<?php echo base_url();?>index.php/sm_zsm/edit_speaker" class="btn btn-light btn-block">Edit</a></td>
                    </tr>
                    <tr>
                      <th scope="row">1</th>
                      <td>Dr Harsh parekh</td>
                      <td>9100944127</td>
                      <td>endoinfo2018@gmail.com</td>
                      <td>DNB (Endocrinologist)</td>
                      <td><a href="<?php echo base_url();?>index.php/sm_zsm/edit_speaker" class="btn btn-light btn-block">Edit</a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
	  
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>