<?php include "header.php";?>
<style type="text/css">
 
</style>
<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">

      <!--Start Dashboard Content-->
 <div class="col-lg-12">
          <div class="card">
            <div class="card-body" style="height: 1000px">
              <h5 class="card-title">View Doctors Data</h5>
              <a href="<?php echo base_url();?>index.php/SM/add_doctor"><button type="button" class="btn btn-gradient-info m-1 pull-right">Add Doctor</button></a>
              <?php // echo '<pre>';print_r($results); die;?>  
        <div class="table-responsive">
               <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Code</th>
                      <th scope="col">Name</th>
                      <th scope="col">Zone</th>
                    <!--   <th scope="col">SBU Code</th> -->
                      <th scope="col">Mobile Number</th>
                      <th scope="col">Email</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php
                         $username = $this->session->userdata('username'); 
                      $rs = $this->db->query("select manager_one_zone from manager_one where manager_two_code='$username'");
                      $array = $rs->result_array();
                      $zone = @$array[0]['manager_one_zone'];
                      $i = 1;
                     foreach ($results as $row) {
                      ?>  
                    <tr>
                      <th scope="row"><?php echo $i++;?></th>
                      <td><?php echo $row['manager_one_code'];?></td>
                      <td><?php echo $row['doctor_name'];?></td>
                     <td><?php echo $zone;?></td>
                    <!--   <td><?php //echo $row['sbu_code'];?></td> -->
                      <td><?php echo $row['contact'];?></td>
                      <td><?php echo $row['email'];?></td>
                      <td><a href="<?php echo base_url();?>index.php/SM/edit_doctor/<?php echo $row['id'];?>"><button type="button" class="btn btn-gradient-info m-1">Edit</button></a></td>
                    </tr>
                  <?php }?>
                  </tbody>
                  <tfoot>
                      <tr>
                          <td colspan="7" class="text-left">
                            <span class="pagination"><?php echo $links; ?></span>
                          </td>                        
                      </tr>
                    </tfoot>   
                </table>
            </div>
            </div>
          </div>
        </div>
 </div>  
	  
		

      <!--End Dashboard Content-->
    <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
	
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>