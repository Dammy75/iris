<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">View Final List</h4>
		   
	   </div>
	   <div class="col-sm-3">
       
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                    <tr>
                      <td>Event Title *</td>
                      <td>Evidence to Experience Meet</td>
                    </tr>
                    <tr>
                      <td>Speaker Name *</td>
                      <td>Dr Subrata Chakraborty</td>
                    </tr>
                    <tr>
                      <td>Event Date *</td>
                      <td>7.25.2020  08:00 pm</td>
                    </tr>
					<tr>
                      <td>Zoom Link *</td>
                      <td>https://bit.ly/3eWPBPX</td>
                    </tr>
					<tr>
                      <td>Zoom ID *</td>
                      <td>813 2319 2236</td>
                    </tr>
					<tr>
                      <td>Zoom Password *</td>
                      <td>REMO</td>
                    </tr>
					
                </table>
              </div>
            </div>
          </div>
        </div>
		<div class="col-lg-12">
			<h4 class="page-title">Doctors List</h4>
			<div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Speaker Name</th>
                      <th scope="col">Select Attended</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
                      <td>
						<div class="icheck-material-primary">
							Attended
						</div>
					  </td>
                    </tr>
                    <tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td>
						<div class="icheck-material-primary">
							Attended
						</div>
					  </td>
                    </tr>
                    <tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td>
						<div class="icheck-material-primary">
							Attended
						</div>
					  </td>
                    </tr>
					<tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td>
						<div class="icheck-material-primary">
							Attended
						</div>
					  </td>
                    </tr>
					<tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td>
						<div class="icheck-material-primary">
							Attended
						</div>
					  </td>
                    </tr><tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td>
						<div class="icheck-material-primary">
							Attended
						</div>
					  </td>
                    </tr><tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td>
						<div class="icheck-material-primary">
							Attended
						</div>
					  </td>
                    </tr><tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td>
						<div class="icheck-material-primary">
							Attended
						</div>
					  </td>
                    </tr><tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td>
						<div class="icheck-material-primary">
							Attended
						</div>
					  </td>
                    </tr>
					<tr>
                      <th scope="row">1</th>
                      <td>Dr Subrata Chakraborty</td>
					  <td>
						<div class="icheck-material-primary">
							Attended
						</div>
					  </td>	
                    </tr>
                  </tbody>
                </table>
           
              </div>
            </div>
          </div>
		</div>
      </div><!--End Row-->
	  
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>