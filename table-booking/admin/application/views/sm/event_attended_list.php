<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Event Attended List</h4>
	   </div>
	   <div class="col-sm-3">
       
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">View Event</h5>
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Event Title</th>
                      <th scope="col">Speaker Name</th>
                      <th scope="col">Date</th>
                      <th scope="col">Time</th>
                      <th scope="col">Approved by ZSM</th>
                      <th scope="col">Approved by SM</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Evidence to Experience Meet</td>
                      <td>Dr Subrata Chakraborty</td>
                      <td>7.25.2020</td>
                      <td>08:00 pm</td>
                      <td>Approved</td>
                      <td>Approved</td>
                      <td><a href="<?php echo base_url();?>index.php/sm_zsm/view_attended_event" class="btn btn-light btn-block">View</a></td>
                    </tr>
                    <tr>
                      <th scope="row">1</th>
                      <td>Evidence to Experience Meet</td>
                      <td>Dr Subrata Chakraborty</td>
                      <td>7.25.2020</td>
                      <td>08:00 pm</td>
                      <td>Approved</td>
                      <td>Approved</td>
                      <td><a href="<?php echo base_url();?>index.php/sm_zsm/view_attended_event" class="btn btn-light btn-block">View</a></td>
                    </tr>
                    <tr>
                      <th scope="row">1</th>
                      <td>Evidence to Experience Meet</td>
                      <td>Dr Subrata Chakraborty</td>
                      <td>7.25.2020</td>
                      <td>08:00 pm</td>
                      <td>Approved</td>
                      <td>Approved</td>
                      <td><a href="<?php echo base_url();?>index.php/sm_zsm/view_attended_event" class="btn btn-light btn-block">View</a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
	  
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>