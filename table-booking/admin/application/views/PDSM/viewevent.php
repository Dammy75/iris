<?php include "pdsmhead/head.php";?>
<?php include "pdsmhead/header.php";?>
<?php include "pdsmhead/sidebar.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">View All Events</h4>
		    
	   </div>
	   <div class="col-sm-3">
       
     </div>
     </div>
   
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Export View Data</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                       
                        
						<th>SL</th>
						<th>Event Title</th>
						<th>Speaker Name</th>
						<th>DSM Code</th>
						<th>Date</th>
						<th>Time</th>
						<th>Approved By ZSM	</th>
						<th>Approved By SM</th>
						<th>Event Status</th>
						<th>Action</th>

                    </tr>
                </thead>
                <tbody>
					<?php
					
					foreach($event_approved_list as $rows){
					 $date=$rows['date_time']; 
					    $date_time = explode(" ",$date);
					    $date = $date_time[0];
					    $time = $date_time[1];	
					?>
                    <tr>
					
					
                        <td><?php echo $rows['id'];?></td>
                        <td><?php echo $rows['title'];?></td>
                        <td><?php echo $rows['speaker_name'];?></td>
						<td><?php echo $rows['manager_code'];?></td>
						<td><?php echo $date;?></td>
						<td><?php echo $time;?></td>
						<td><?php if($rows['approved_by_zsm']==0){ echo "No"; }else {echo "Yes";} ?></td>
					    <td><?php if($rows['approved_by_sm']==0){ echo "No"; }else {echo "Yes";} ?></td>
						<td><?php if($rows['status']==0){ echo "Inactive"; }else {echo "Active";} ?></td>
						<td>	<a href="<?php echo site_url('Eventpdsm/view_singleevent/'.$rows['id'].'/');?>">View</a></td>
						
					
                    </tr>
                    <?php } ?>	
						
                </tbody>
                <tfoot>
                    <tr>
                       <th>SL</th>
						<th>Event Title</th>
						<th>Speaker Name</th>
						<th>DSM Code</th>
						<th>Date</th>
						<th>Time</th>
						<th>Approved By ZSM	</th>
						<th>Approved By SM</th>
						<th>Event Status</th>
						<th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2020 
        </div>
      </div>
    </footer>
	<!--End footer-->
	
	<!--start color switcher-->
   <div class="right-sidebar">
    <div class="switcher-icon">
      <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
    </div>
    <div class="right-sidebar-content">

      <p class="mb-0">Gaussion Texture</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme1"></li>
        <li id="theme2"></li>
        <li id="theme3"></li>
        <li id="theme4"></li>
        <li id="theme5"></li>
        <li id="theme6"></li>
      </ul>

      <p class="mb-0">Gradient Background</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme7"></li>
        <li id="theme8"></li>
        <li id="theme9"></li>
        <li id="theme10"></li>
        <li id="theme11"></li>
        <li id="theme12"></li>
		<li id="theme13"></li>
        <li id="theme14"></li>
        <li id="theme15"></li>
      </ul>
      
     </div>
   </div>
  <!--end color switcher-->
   
  </div><!--End wrapper-->


  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="<?php echo base_url();?>assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- Metismenu js -->
  <script src="<?php echo base_url();?>assets/plugins/metismenu/js/metisMenu.min.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url();?>assets/js/app-script.js"></script>

  <!--Data Tables js-->
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
      } );

    </script>
	
</body>

<!-- Mirrored from codervent.com/dashtreme/demo/transparent-admin/vertical-layout/table-data-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 11:12:17 GMT -->
</html>
