<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">

      <!--Start Dashboard Content-->

	<div class="card mt-3">
    <div class="card-content">
        <div class="row row-group m-0">
            <div class="col-12 col-lg-4 col-xl-4 border-light">
                 <div class="card-body">
                 <h5 class="mb-0 text-white ">Number Of Doctor <i class="fa fa-hand-o-right"></i> <span class="float-right"><?php echo $ttldoctor = @$results[0]['doctorcount'];?></span></h5>
                </div>
            </div>
            <div class="col-12 col-lg-4 col-xl-4 border-light">
              <div class="card-body">
                 <h5 class="mb-0 text-white ">Total Event <i class="fa fa-hand-o-right"></i> <span class="float-right"><?php echo $ttleventcount = $event[0]['eventcount'];?></span></h5>
                </div>
            </div>
            <div class="col-12 col-lg-4 col-xl-4 border-light">
                <div class="card-body">
                 <h5 class="mb-0 text-white ">Total Approved <i class="fa fa-hand-o-right"></i> <span class="float-right"><?php echo $ttlapprovedevent = $approvedevent[0]['approvedevent'];?></span></h5>
                </div>
            </div>
            <div class="col-12 col-lg-4 col-xl-4 border-light">
               <div class="card-body">
                 <h5 class="mb-0 text-white ">Total Disapproved <i class="fa fa-hand-o-right"></i> <span class="float-right"><?php echo $ttlounapprovedevent = $ttleventcount - $ttlapprovedevent ?></span></h5>
                </div>
            </div>
             <div class="col-12 col-lg-4 col-xl-4 border-light">
                <div class="card-body">
                 <h5 class="mb-0 text-white ">Prescribe Doctors <i class="fa fa-hand-o-right"></i> <span class="float-right"><?php echo $prescribedoctor = $prescribed_doctor[0]['prescribeddoctorcount'];?></span></h5>
                </div>
            </div>
            <div class="col-12 col-lg-4 col-xl-4 border-light">
               <div class="card-body">
                 <h5 class="mb-0 text-white ">Non-Prescribe Doctors <i class="fa fa-hand-o-right"></i> <span class="float-right"><?php echo $nonprescribedoctor = $ttldoctor - $prescribedoctor; ?></span></h5>
                </div>
            </div>
        </div>
    </div>
 </div>  
	  
		

      <!--End Dashboard Content-->
    <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
	
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>