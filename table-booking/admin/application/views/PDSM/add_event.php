<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
 <div class="card">
  <a href="<?php echo base_url();?>index.php/pdsm/view_event"><button type="button" class="btn btn-gradient-info m-1 pull-right">Event List</button></a>
      <div class="card-header text-uppercase">Add Event</div>
       <div class="card-body">
         <form>

           <div class="row">
			<div class="col-lg-12">
			   
			   <div class="card">
			     <div class="card-body">
				   <form>
					 <div class="form-group row">
					  <label for="input-21" class="col-sm-2 col-form-label">Event Title *</label>
					  <div class="col-sm-10">
						Evidence to Experience Meet
					  </div>
					</div>
					 <div class="form-group row">
					  <label for="input-21" class="col-sm-2 col-form-label">Speaker Name *</label>
					  <div class="col-sm-7">
						<select class="form-control single-select">
                          <option>Select</option>
                          <option>Yes</option>
                          <option>No</option>
                        </select>
					  </div>
					  <div class="col-sm-3">
						<a href="#" class="btn btn-light btn-block">Add Speaker</a>
					  </div>
					</div>
					<div class="form-group row">
					  <label for="input-22" class="col-sm-2 col-form-label">Event Date *</label>
					  <div class="col-sm-10">
						<input type="text" id="autoclose-datepicker" class="form-control">
					  </div>
					</div>
					  
				   </div><!--End Row-->
	  
					</form>
				 </div>
			 </div>
			<div class="col-lg-12">
			<h4 class="page-title">Doctors List</h4>
				<center>Prescriber and non-prescriber</center><br>
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
						<table class="table">
							<tr>
							  <th>#</th>
							  <th>Name</th>
							</tr>
							<tr>
							   <td>1</td>
							   <td>
								  <select class="form-control single-select">
								     <option>Select</option>
								     <option>Dr. Danish Patel</option>
								     <option>Dr. Mustakim Sayyed</option>
								     <option>Dr. Harsha Gandhi</option>
								     <option>Dr. Sana Sayyed</option>
								  </select>
							   </td>
							</tr>
							<tr>
							   <td>1</td>
							   <td>
								  <select class="form-control single-select">
									 <option>Select</option>
								     <option>Dr. Danish Patel</option>
								     <option>Dr. Mustakim Sayyed</option>
								     <option>Dr. Harsha Gandhi</option>
								     <option>Dr. Sana Sayyed</option>
								  </select>
							   </td>
							</tr>
							<tr>
							   <td>1</td>
							   <td>
								  <select class="form-control single-select">
								     <option>Select</option>
								     <option>Dr. Danish Patel</option>
								     <option>Dr. Mustakim Sayyed</option>
								     <option>Dr. Harsha Gandhi</option>
								     <option>Dr. Sana Sayyed</option>
								  </select>
							   </td>
							</tr>
							<tr>
							   <td>1</td>
							   <td>
								  <select class="form-control single-select">
								     <option>Select</option>
								     <option>Dr. Danish Patel</option>
								     <option>Dr. Mustakim Sayyed</option>
								     <option>Dr. Harsha Gandhi</option>
								     <option>Dr. Sana Sayyed</option>
								  </select>
							   </td>
							</tr>
							<tr>
							   <td>1</td>
							   <td>
								  <select class="form-control single-select">
								     <option>Select</option>
								     <option>Dr. Danish Patel</option>
								     <option>Dr. Mustakim Sayyed</option>
								     <option>Dr. Harsha Gandhi</option>
								     <option>Dr. Sana Sayyed</option>
								  </select>
							   </td>
							</tr>
							<tr>
							   <td>1</td>
							   <td>
								  <select class="form-control single-select">
								     <option>Select</option>
								     <option>Dr. Danish Patel</option>
								     <option>Dr. Mustakim Sayyed</option>
								     <option>Dr. Harsha Gandhi</option>
								     <option>Dr. Sana Sayyed</option>
								  </select>
							   </td>
							</tr>
							
						</table>
					  </div>
           
					</div>
				</div>
			</div>

       	</div>
	
          </div><!--end row-->

      </form>
       </div>
     </div>
 </div>  
	  
		

      <!--End Dashboard Content-->
    <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
	
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>