<?php include "header.php";?>

<div class="clearfix"></div>
  
  <div class="content-wrapper">
    <div class="container-fluid">
    <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Edit Speaker</h4>
     </div>
       
       <div class="col-sm-3 pull-right">
     <a href="<?php echo base_url();?>index.php/PDSM/speaker_agreement"><button type="button" class="btn btn-gradient-info m-1 pull-right">Speaker List</button></a>
       </div>
     </div>
    <!-- End Breadcrumb-->
  	<h4 class="box-title m-b-0 text-center" style="color:red;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('error');?> <?php echo $this->session->flashdata('suspend');?> </h4>				
				<h4 class="box-title m-b-0 text-center" style="color:#00FF00;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('success');?> </h4>
     <div class="row">
			<div class="col-lg-12">
			   <?php //echo '<pre>'; print_r($results);?>
			   <div class="card">
			     <div class="card-body">
				   <form action="<?php echo base_url();?>index.php/PDSM/update_speaker" method="post" enctype="multipart/form-data">
					 <div class="form-group row">
					  <label for="input-21" class="col-sm-2 col-form-label">Speaker Name *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="speaker_name" id="speaker_name" value="<?php echo @$results[0]['name'];?>" placeholder="Enter Your Speaker Name" required>
					  </div>
					</div>
					<div class="form-group row">
					  <label for="input-22" class="col-sm-2 col-form-label">Headquarters *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="headquarter" id="headquarter" value="<?php echo @$results[0]['headquarters'];?>" placeholder="Enter Your headquarter" required>
					  </div>
					</div>
					  <div class="form-group row">
						<label for="input-23" class="col-sm-2 col-form-label">Region *</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="region" id="region" placeholder="Enter Your region" value="<?php echo @$results[0]['region'];?>" required>
						</div>
					  </div>
					<div class="form-group row">
					  <label for="input-24" class="col-sm-2 col-form-label">Mobile No. *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Enter Mobile No." value="<?php echo @$results[0]['contact'];?>" required>
					  </div>
					</div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Email Id *</label>
						<div class="col-sm-10">
						<input type="email" class="form-control" name="email" id="email" placeholder="Email Id" value="<?php echo @$results[0]['email'];?>" required>
						</div>
					  </div>
					  
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Medical Registration Number *</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="med_reg_no" id="med_reg_no" placeholder="Medical Registration No." value="<?php echo @$results[0]['medical_registration_number'];?>" required>
						</div>
					  </div>
					  
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">SBU Code *</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="sbu_code" id="sbu_code" placeholder="SBU Code" value="<?php echo @$results[0]['sbu_code'];?>" required>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Address</label>
						<div class="col-sm-10">
						<textarea class="form-control" name="address" id="address" placeholder="Address" required><?php echo @$results[0]['address'];?></textarea>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Government Doctor (Y/N) *</label>
						<div class="col-sm-10">
						<select class="form-control single-select" name="is_government_doctor" id="is_government_doctor">
                            <option <?php if($results[0]['government_doctor']=='yes'){ echo 'selected'; } ?> value="yes" >Yes</option>
                          <option <?php if($results[0]['government_doctor']=='no'){ echo 'selected'; } ?> value="no">No</option>
                        </select>
						</div>
					  </div>
					    <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Latest CV *</label>
						<div class="col-sm-10">
						<img hieght="100px" width="60px" src="<?php echo base_url();?>uploads/speakers/<?php echo @$results[0]['id'];?>/<?php echo @$results[0]['latest_cv'];?>"></img>
						<input type="file" class="form-control" name="latest_cv"  <?php if(@$results[0]['latest_cv']==''){ echo 'required'; } ?> id="latest_cv" ccept="image/*,.doc, .docx,.ppt, .pptx, .pdf" placeholder="Latest CV">
							<p>(Only Word, PDF, PPT, Image are allowed)</p>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Photo of the Speaker *</label>
						<div class="col-sm-10">
						<img hieght="100px" width="60px" src="<?php echo base_url();?>uploads/speakers/<?php echo @$results[0]['id'];?>/<?php echo @$results[0]['speaker_photo'];?>"></img>
						<input type="file" class="form-control" name="speaker_photo" id="speaker_photo" <?php if(@$results[0]['speaker_photo']==''){ echo 'required'; } ?> ccept="image/*, .pdf" placeholder="Latest CV">
								<p>(Only PDF and JPEG are allowed)</p>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Cancelled Cheque</label>
						<div class="col-sm-10">
						<img hieght="100px" width="60px" src="<?php echo base_url();?>uploads/speakers/<?php echo @$results[0]['id'];?>/<?php echo @$results[0]['cancelled_cheque'];?>"></img>
						<input type="file" class="form-control" name="cheque_photo" id="cheque_photo" accept="image/*, .pdf" placeholder="Latest CV">
								<p>(Only PDF and JPEG are allowed)</p>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">PAN Card</label>
						<div class="col-sm-10">
						<img hieght="100px" width="60px" src="<?php echo base_url();?>uploads/speakers/<?php echo @$results[0]['id'];?>/<?php echo @$results[0]['pan_card'];?>"></img>
						<input type="file" class="form-control" name="pancard" id="pancard" accept="image/*, .pdf" placeholder="Latest CV">
								<p>(Only PDF and JPEG are allowed)</p>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">GST Declaration Letter</label>
						<div class="col-sm-10">
						<img hieght="100px" width="60px" src="<?php echo base_url();?>uploads/speakers/<?php echo @$results[0]['id'];?>/<?php echo @$results[0]['gst_letter'];?>"></img>
						<input type="file" class="form-control" name="gst" id="gst" accept="image/*, .pdf" placeholder="Latest CV">
								<p>(Only PDF and JPEG are allowed)</p>
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Speciality / Qualification *</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="qualification" id="qualification" value="<?php echo @$results[0]['credential_qualification'];?>" placeholder="Qualification" required>
						</div>
					  </div>
				   </div><!--End Row-->
	  
					  
					  
					 <div class="form-group row">
					  <label class="col-sm-2 col-form-label"></label>
					  <div class="col-sm-10">
					  		<input type="hidden" name="old_latest_cv" value="<?php echo @$results[0]['latest_cv'];?>">
					  		<input type="hidden" name="old_speaker_photo" value="<?php echo @$results[0]['speaker_photo'];?>">
					  		<input type="hidden" name="old_cancelled_cheque" value="<?php echo @$results[0]['cancelled_cheque'];?>">
					  		<input type="hidden" name="old_pan_card" value="<?php echo @$results[0]['pan_card'];?>">
					  		<input type="hidden" name="old_gst_letter" value="<?php echo @$results[0]['gst_letter'];?>">
					  		<input type="hidden" name="id" value="<?php echo @$results[0]['id'];?>">
						<button type="submit" name="edit_doctor" id="edit_doctor" class="btn btn-white px-5"><i class="icon-lock"></i> Edit</button>
					  </div>
					</div>
					</form>
				 </div>
			 </div>


       	</div>
	 </div><!--End Row-->
<!--start overlay-->
    <div class="overlay"></div>
  <!--end overlay-->
    </div>
    <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
  
 <?php include "footer.php";?>
 <script>
setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
	setTimeout(function() {
            $('#timeout1').fadeToggle('slow');
            }, 3000);
			

</script>