<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">

      <!--Start Dashboard Content-->
 <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">View Event Data</h5>
              <a href="<?php echo base_url();?>index.php/dsm/add_event"><button type="button" class="btn btn-gradient-info m-1 pull-right">Add Event</button></a>
        <div class="table-responsive">
               <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Event Title</th>
                      <th scope="col">Speaker Name</th>
                      <th scope="col">Prescriber</th>
                      <th scope="col">Non-Prescriber</th>
                      <th scope="col">Date</th>
                      <th scope="col">Time</th>
                      <th colspan="2" scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Evidence to Experience Meet</td>
                      <td>Dr Subrata Chakraborty</td>
                      <td>0</td>
                      <td>41</td>
                      <td>28/07/2020</td>
                      <td>08:00 pm</td>
                      <td><a href="<?php echo base_url();?>index.php/dsm/edit_event"><button type="button" class="btn btn-gradient-info m-1">Edit</button></a></td>
                      <td><a href="<?php echo base_url();?>index.php/dsm/event"><button type="button" class="btn btn-gradient-info m-1">View</button></a></td>
                    </tr>
                  
                  </tbody>
                </table>
            </div>
            </div>
          </div>
        </div>
 </div>  
	  
		

      <!--End Dashboard Content-->
    <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
	
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>