<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
 <div class="card">
  <a href="event.html"><button type="button" class="btn btn-gradient-info m-1 pull-right">Event List</button></a>
      <div class="card-header text-uppercase">Edit Event</div>
       <div class="card-body">
         <form>
		
          <div class="row">
            <div class="col-12 col-lg-6 col-xl-6">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Event Title</label>
                <div class="col-sm-8">
                <input type="text" class="form-control" value="Mumbai">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Speaker Name </label>
                <div class="col-sm-8">
                <input type="text" class="form-control" value="Dr Harsh parekh">
                </div>
              </div>
               <div class="form-group row">
                <label class="col-sm-4 col-form-label">Event Date  </label>
                <div class="col-sm-8">
                <input type="text" class="form-control" value="25/7/2020 ">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Event time  </label>
                <div class="col-sm-8">
                <input type="time" class="form-control" value="23:05">
                </div>
              </div>
             
             <h5> Doctor List</h5> 
                <div class="table-responsive">
               <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Doctor Name</th>
                     
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Dr. Basu</td>
                    </tr>
                     <tr>
                      <th scope="row">2</th>
                      <td>Dr. Tambe</td>
                    </tr>
                  
                  </tbody>
                </table>
            </div>
 <div class="form-group row">
                <label class="col-sm-2 col-form-label"></label>
                <div class="col-sm-10">
                <input type="submit"  class="btn btn-gradient-info m-1" value="Submit">
                </div> 
              </div>

                

        </div>
            
            
    
            </div>
          
          </div><!--end row-->

      </form>
       </div>
     </div>
 </div>  
	  
		

      <!--End Dashboard Content-->
    <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
	
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>