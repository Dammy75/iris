<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
 <div class="card">
  <a href="<?php echo base_url();?>index.php/dsm/view_final_event"><button type="button" class="btn btn-gradient-info m-1 pull-right">Final Event List</button></a>
      <div class="card-header text-uppercase">View Final Event</div>
       <div class="card-body">
         <form>

          <div class="row">
            <div class="col-12 col-lg-6 col-xl-6">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Event Title</label>
                <div class="col-sm-8">
                <input type="text" class="form-control" value="Mumbai">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Speaker Name </label>
                <div class="col-sm-8">
                <input type="text" class="form-control" value="Dr Harsh parekh">
                </div>
              </div>
               <div class="form-group row">
                <label class="col-sm-4 col-form-label">Event Date  </label>
                <div class="col-sm-8">
                <input type="text" class="form-control" value="7.25.2020  08:00 pm">
                </div>
              </div>
                <div class="form-group row">
                <label class="col-sm-4 col-form-label">Zoom Link </label>
                <div class="col-sm-8">
                <input type="text" class="form-control" value="https://bit.ly/3eWPBPX">
                </div>
              </div>
                <div class="form-group row">
                <label class="col-sm-4 col-form-label">Zoom ID   </label>
                <div class="col-sm-8">
                <input type="text" class="form-control" value="813 2319 2236">
                </div>
              </div>
               <div class="form-group row">
                <label class="col-sm-4 col-form-label">Zoom Password  </label>
                <div class="col-sm-8">
                <input type="text" class="form-control" value="REMO">
                </div>
              </div>
             
             <h5> Doctor List</h5> 
                <div class="table-responsive">
               <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Doctor Name</th>
                      <th scope="col"> Select Attended</th>
                     
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Dr. Basu</td>
                      <td>Attended</td>
                    </tr>
                     <tr>
                      <th scope="row">1</th>
                      <td>Dr. Tambe</td>
                      <td>Not Attended</td>
                    </tr>
                  
                  </tbody>
                </table>
            </div>


                

        </div>
            
            
    
            </div>
          
          </div><!--end row-->

      </form>
       </div>
     </div>
 </div>  
	  
		

      <!--End Dashboard Content-->
    <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
	
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php"; ?>