
<body class="bg-theme bg-theme1">

<!-- Start wrapper-->
 <div id="wrapper">

 <!--Start sidebar-wrapper-->
   <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
   
    <div class="brand-logo">
	  <img src="<?php echo base_url();?>assets/images/logo-icon.png" class="logo-icon" alt="logo icon">
	  <h5 class="logo-text">NEXTGENMEET</h5>
	  <div class="close-btn"><i class="zmdi zmdi-close"></i></div>
   </div>
	  
  <ul class="metismenu" id="menu">
		<li>
		  <a href="<?php echo base_url();?>index.php/PDSM/dashboard">
			<div class="parent-icon"><i class="zmdi zmdi-view-dashboard"></i></div>
			<div class="menu-title">Dashboard</div>
		  </a>
		</li>
		<li>
		  <a href="<?php echo base_url();?>index.php/PDSM/doctor_list">
			<div class="parent-icon"><i class="zmdi zmdi-layers"></i></div>
			<div class="menu-title">Doctor List</div>
		  </a>
		</li>
		<li>
		  <a href="<?php echo base_url();?>index.php/PDSM/speaker_agreement">
			<div class="parent-icon"><i class="zmdi zmdi-card-travel"></i></div>
			<div class="menu-title">Speaker Agreement</div>
		  </a>
		</li>
	
		<li>
		  <a href="<?php echo base_url();?>index.php/eventpdsm">
			<div class="parent-icon"><i class="zmdi zmdi-chart"></i></div>
			<div class="menu-title">Create Event</div>
		  </a>
		</li>
		<li>
		  <a href="<?php echo base_url();?>index.php/PDSM/approved_event_list">
			<div class="parent-icon"><i class="zmdi zmdi-widgets"></i></div>
			<div class="menu-title">Approved Event List</div>
		  </a>
		</li>
		<li>
		  <a href="<?php echo base_url();?>index.php/PDSM/event_attended_list">
			<div class="parent-icon"><i class="zmdi zmdi-format-list-bulleted"></i></div>
			<div class="menu-title"> Event Attended List</div>
		  </a>
		</li>
		<li>
		  <a href="<?php echo base_url();?>index.php/PDSM/event_final_list">
			<div class="parent-icon"><i class="zmdi zmdi-lock"></i></div>
			<div class="menu-title"> Event Final List</div>
		  </a>
		</li>
			<li>
		  <a href="<?php echo base_url();?>index.php/PDSM/event_thank_u_list">
			<div class="parent-icon"><i class="zmdi zmdi-grid"></i></div>
			<div class="menu-title"> Event Thank You List</div>
		  </a>
		</li>
			<li>
		  <a href="<?php echo base_url();?>index.php/PDSM/event_missed_list">
			<div class="parent-icon"><i class="zmdi zmdi-invert-colors"></i></div>
			<div class="menu-title"> Event Missed List</div>
		  </a>
		</li>
		
		
		
		
		
		
		
		
		   </ul></div>
   <!--End sidebar-wrapper-->