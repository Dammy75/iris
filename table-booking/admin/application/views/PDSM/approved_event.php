<?php include "header.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-SM-9">
		    <h4 class="page-title">Event Approved List</h4>
		   
	   </div>
	   <div class="col-SM-3">
       
     </div>
     </div>
    <!-- End Breadcrumb-->
    
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Export View Data</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                      <th scope="col">Sr. No</th>
                      <th scope="col">Event Title</th>
                      <th scope="col">PDSM Code</th>
                      <th scope="col">Speaker 1</th>
                      <th scope="col">Speaker 2</th>
					  <th scope="col">Approved by SM</th>
					  <th scope="col">Approved by ZSM</th>
					  <th scope="col">Approved by Admin</th>
                      <th scope="col">Date</th>
                      <th scope="col">Time</th>
					  <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
				<?php //print_r($event_approved_list);exit; 
			 $i=1; foreach($event_approved_list as $key){ 
					  $speakers=$key['speaker_name']; 
					  $speakers = explode(",",$speakers);
					  $speaker1 = $speakers[0];
					  $speaker2 = $speakers[1];
			          $date=$key['date_time']; 
					  $date_time = explode(" ",$date);
					  $date = $date_time[0];
					  $time = $date_time[1];
			  
			  ?>
                   <tr>
					  <td><?php echo $i++;?></td>
					  <td><?php echo $key['title'];?></td>
					  <td><?php echo $key['manager_code'];?></td>
                      <td><?php echo $speaker1; ?></td>
                      <td><?php echo $speaker2; ?></td>
                      <td><?php if($key['approved_by_sm']==0){ echo "Not Approved"; }else {echo "Approved";} ?></td>
                      <td><?php if($key['approved_by_zsm']==0){ echo "Not Approved"; }else {echo "Approved";} ?></td>
                      <td><?php if($key['approved_by_admin']==0){ echo "Not Approved"; }else {echo "Approved";} ?></td>
                      <td><?php echo $date;?></td>
                      <td><?php echo $time;?></td>
					  <td><a class="btn btn-gradient-info m-1" href="<?php echo base_url();?>index.php/PDSM/view_approved_event/<?php echo $key['id']?>">View</a></td>
                    </tr>
                    <?php $i++; } ?>	
						
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    
     
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>