<?php include "head.php";?>
<?php include "header.php";?>
<?php include "sidebar.php";?>
<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
 <div class="card">
  <a href="<?php echo base_url();?>index.php/SuperAdmin/booking_list"><button type="button" class="btn btn-gradient-info m-1 pull-right">Booking List</button></a>
      <div class="card-header text-uppercase">Edit Booking</div>
       <div class="card-body">
      	<h4 class="box-title m-b-0 text-center" style="color:red;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('error');?> <?php echo $this->session->flashdata('suspend');?> </h4>				
				<h4 class="box-title m-b-0 text-center" style="color:#00FF00;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('success');?> </h4>	
			<div id="result" style="color:red;font-weight: bold;"></div>	
			<div id="resultsuccess" style="color:green;font-weight: bold;"></div>	
          <div class="row">
			<div class="col-lg-12">
			   <?php  //echo '<pre>';print_r($results);die;?>
			   <div class="card">
			     <div class="card-body">
				   <form action="<?php echo base_url();?>index.php/SuperAdmin/update_booking"  id="formedit" method="post" onsubmit="return validation();">
					
					<div class="form-group row">
					  <label for="input-22" class="col-sm-2 col-form-label">Name *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="name" id="name" value="<?php echo @$results[0]['name'];?>" placeholder="Enter Your Name" required>
					  </div>
					</div>
					
					  <div class="form-group row">
						<label for="input-23" class="col-sm-2 col-form-label">Booking Date *</label>
						<div class="col-sm-5">

						<input type="tet" class="form-control" name="oldbooking_date" id="zone" value="<?php echo date('d-M-Y', strtotime(@$results[0]['booking_date_time']));?>" placeholder="Enter Your Zone" >
						</div>
						<div class="col-sm-5">
						<input type="date" class="form-control" name="newbooking_date" id="zone"  placeholder="Enter Your Zone" >
						</div>
					  </div>

					   <div class="form-group row">
						<label for="input-23" class="col-sm-2 col-form-label">Booking Time *</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="oldbooking_time" id="oldbooking_time" value="<?php  echo date('g:iA', strtotime(@$results[0]['booking_date_time']));?>" placeholder="Enter Booking Time" >
						</div>
					  </div>

					  <div class="form-group row">
					  <label for="input-24" class="col-sm-2 col-form-label">Total People. *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="total_people" id="total_people" value="<?php echo @$results[0]['total_people'];?>" placeholder="Enter Total People." required>
					  </div>
					</div>
					<div class="form-group row">
					  <label for="input-24" class="col-sm-2 col-form-label">Total People. *</label>
					  <div class="col-sm-10">
						<select class="form-control" name="type" id="speakerlist">
							<option <?php if(@$results[0]['type']=='1'){ ?> selected <?php } ?> value="1">Elements</option>
							<option <?php if(@$results[0]['type']=='2'){ ?> selected <?php } ?> value="2">EGO Italian</option>
						</select>
					  </div>
					</div>

					<div class="form-group row">
					  <label for="input-24" class="col-sm-2 col-form-label">Mobile No. *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="mobile_no" id="mobile_no" value="<?php echo @$results[0]['mobile'];?>" placeholder="Enter Mobile No." required>
					  </div>
					</div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Email Id *</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="email" id="email" value="<?php echo @$results[0]['email'];?>" placeholder="Email Id" required>
						<input type="hidden" class="form-control" name="id" id="id" value="<?php echo @$results[0]['booking_id'];?>">
						<input type="hidden" class="form-control" name="status" id="id" value="<?php echo @$results[0]['status'];?>">
						</div>
					  </div>
					<div class="form-group row">
					  <label for="input-24" class="col-sm-2 col-form-label">Payment Id</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="payment_id" id="payment_id" value="<?php echo @$results[0]['payment_id'];?>" placeholder="Enter Payment Id">
					  </div>
					</div>
					 <div class="form-group row">
					  <label class="col-sm-2 col-form-label"></label>
					  <div class="col-sm-10">

						<input type="submit" name="edit_doctor" id="edit_doctor" class="btn btn-white px-5"   value="Edit">	
					   </div>
					</div>
					</form>
				 </div>
			 </div>


       	</div>
		  </div><!--End Row-->
 
          </div><!--end row-->

      
       </div>
     </div>
 </div>  
	  
		

      <!--End Dashboard Content-->
    <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
	
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>
		<script>
function validation(){
             /* var name  = $("#name").val();
              var email = $("#email").val();
              var mno   = $("#mobile_no").val();
              var mnolength = mno.length;
              var gender = $("#gender").val();
              var dob   = $("#dob").val();
              var dob1   = $("#dob1").val();*/

             $.ajax({
                       type: "POST",
                       url: "<?php echo base_url()?>index.php/SuperAdmin/validate",
                       data:  $('#formedit').serialize(),
                       success: function(result1)
                       {
                       	//alert(result1);
                       	if(result1 !='Booking is Updated Successfully.'){
                       			 $("#resultsuccess").hide();
                       		 $("#result").html(result1);
                       		return false;
                       	}
                       	else{
                       		  $.ajax({
                       type: "POST",
                       url: "<?php echo base_url()?>index.php/SuperAdmin/update_booking",
                       data:  $('#formedit').serialize(),
                       success: function(result)
                       {
                       	 $("#result").hide();
                       	 $("#resultsuccess").html(result);
                      
                       
                       } 

                   });
                       	}
                         /*if(result1 =='exist'){
                          $('.validemail_err').show();
                          $('.all_err').show();
                             setTimeout(function() {
                          $('.validemail_err').fadeOut('slow');
                            }, 2000);*/
                               
                          /*  return false;
                          }*/
                       
                       } 

                   });

               return false;    
               
            
           
      }


setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
	setTimeout(function() {
            $('#timeout1').fadeToggle('slow');
            }, 3000);
			

</script>