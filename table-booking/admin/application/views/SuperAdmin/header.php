


<!--Start topbar header-->
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top">
 
	 <div class="toggle-menu">
		 <i class="zmdi zmdi-menu"></i>
	 </div>
	 
     
   <ul class="navbar-nav align-items-center right-nav-link ml-auto">
   <li class="nav-item dropdown"><h4>Admin</h4></li>
	<li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret position-relative" data-toggle="dropdown" href="javascript:void();">
        <span class="user-profile"><img src="<?php echo base_url();?>assets/images/avatars/avatar-13.png" class="img-circle" alt="user avatar"></span>
      </a>
      <ul class="dropdown-menu dropdown-menu-right">
       <li class="dropdown-item user-details">
        <a href="javaScript:void();">
           <div class="media">
             <div class="avatar"><img class="align-self-start mr-3" src="<?php echo base_url();?>assets/images/avatars/avatar-13.png" alt="user avatar"></div>
            <div class="media-body">
            <h6 class="mt-2 user-title">Admin</h6>
            <p class="user-subtitle"><?php echo $this->session->userdata('user_email'); ?></p>
            </div>
           </div>
          </a>
        </li>
        <li class="dropdown-item"><a href="<?php echo base_url();?>index.php/Login/logout"><i class="zmdi zmdi-power mr-3"></i>Logout</li></a>
      </ul>
    </li>
  </ul>
</nav>
</header>
<!--End topbar header-->