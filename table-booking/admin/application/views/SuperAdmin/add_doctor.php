<?php include "head.php";?>
<?php include "header.php";?>
<?php include "sidebar.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">

 <div class="card">
      <a href="<?php echo base_url();?>index.php/SuperAdmin/doctor_list"><button type="button" class="btn btn-gradient-info m-1 pull-right">Doctor List</button></a>
      <div class="card-header text-uppercase">Add Doctor</div>

       <div class="card-body">
      	<h4 class="box-title m-b-0 text-center" style="color:red;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('error');?> <?php echo $this->session->flashdata('suspend');?> </h4>				
				<h4 class="box-title m-b-0 text-center" style="color:#00FF00;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('success');?> </h4>

          <div class="row">
			<div class="col-lg-12">
			   
			   <div class="card">
			     <div class="card-body">
				   <form action="<?php echo base_url();?>index.php/SuperAdmin/insert_doctor" method="post">
					 <div class="form-group row">
					  <label for="input-21" class="col-sm-2 col-form-label">Code *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="usercode" id="usercode"  placeholder="Enter Code" required>
					  </div>
					</div>
					<div class="form-group row">
					  <label for="input-22" class="col-sm-2 col-form-label">Doctor Name *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="name" id="name" placeholder="Enter Your Doctor Name" required>
					  </div>
					</div>
					<?php
					 /* $username = $this->session->userdata('username'); 
                      $rs = $this->db->query("select manager_one_zone from manager_one where manager_one_code='$username'");
                      $array = $rs->result_array();
                      $zone = @$array[0]['manager_one_zone'];*/
                      ?>
					  <div class="form-group row">
						<label for="input-23" class="col-sm-2 col-form-label">Zone *</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="region" id="region"  placeholder="Enter Your Zone" required>
						</div>
					  </div>
					<div class="form-group row">
					  <label for="input-24" class="col-sm-2 col-form-label">Mobile No. *</label>
					  <div class="col-sm-10">
						<input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Enter Mobile No." required>
					  </div>
					</div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-2 col-form-label">Email Id *</label>
						<div class="col-sm-10">
						<input type="email" class="form-control" name="email" id="email" placeholder="Email Id" required>
						</div>
					  </div>
					  
					  
				   </div><!--End Row-->
	  
					  
					  
					 <div class="form-group row">
					  <label class="col-sm-2 col-form-label"></label>
					  <div class="col-sm-10">
						<button type="submit" name="edit_doctor" id="edit_doctor" class="btn btn-white px-5"> Add</button>
					  </div>
					</div>
					</form>
				 </div>
			 </div>


       	</div>
	  
          </div><!--end row-->

     
       </div>
     </div>
 </div>  
	  
		

      <!--End Dashboard Content-->
    <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
	
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>
		<script>
setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
	setTimeout(function() {
            $('#timeout1').fadeToggle('slow');
            }, 3000);
			

</script>