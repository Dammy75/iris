

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">User Data</h4>
		    <!--<ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Dashtreme</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Simple Tables</li>
         </ol>-->
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <button type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-plus mr-1"></i><a href="add_doctor.html"> Add New</a></button>
        
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">View User Data</h5>
			  <div class="table-responsive">
               <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">SL</th>
                      <th scope="col">Photo</th>
                      <th scope="col">Email</th>
                      <th scope="col">Password</th>
					  <th scope="col">Role</th>
					   <th scope="col">Status</th>
                      <th scope="col">Action</th>
					 
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td><img class="align-self-start mr-3" src="assets/images/avatars/avatar-13.png" alt="user avatar"></td>
                      <td>demo@gmail.com</td>
                      <td>97a53ee9f45adfe53c762a72f83f6f43</td>
					   <td>Admin</td>
                      <td>Active</td>
                      <td>
					    <button type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-pencil mr-1"></i> Edit</button>
         <button type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-trash mr-1"></i> Delete</button>
        
					  
					  </td>
                    </tr>
                  
                  </tbody>
                </table>
            </div>
            </div>
          </div>
        </div>
        </div><!--End Row-->

 <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	

