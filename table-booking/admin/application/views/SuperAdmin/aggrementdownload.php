
<?php include "head.php";?>
<?php include "header.php";?>
<?php include "sidebar.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">View Speakers Data</h4>
		    
	   </div>
	   <div class="col-sm-3">
       
     </div>
     </div>
   
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <h5 class="card-title"></h5>
              <a href="<?php echo base_url();?>index.php/SuperAdmin/add_speaker"><button type="button" class="btn btn-gradient-info m-1 pull-right">Add Speakers</button></a>
             <div class="col-sm-12">
       <div class="btn-group float-sm-left">
        <form method='post' action="<?= base_url() ?>index.php/SuperAdmin/aggrementdownload" >
          <label><input type="search" class="form-control form-control-sm" name='search' placeholder="Full Records Search" aria-controls="example" value="<?= $search ?>"></label>
        </form>
    
      </div>
     </div> 
            <div class="card-body">
              <div class="table-responsive">
                  <form action="<?php echo base_url();?>index.php/eventadmin/change_status" method="post" name="event_list">
              <table id="jjexample" class="table table-bordered">
                <thead>
                    <tr>
                       <th scope="col">#</th>
                      <th scope="col">Speaker Name</th>
                      <th scope="col">Mobile Number</th>
                      <th scope="col">Email</th>
                      <th scope="col">Credential</th>
                      <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
					<?php
                      $i=1; 
                      foreach ($results as $row) {
                      ?>  
                    <tr>
                      <th scope="row"><?php echo $i++;?></th>
                      <td><?php echo $row['name'];?></td>
                      <td><?php echo $row['contact'];?></td>
                      <td><?php echo $row['email'];?></td>
                      <td><?php echo $row['credential_qualification'];?></td>
                      <td><a href="<?php echo base_url();?>index.php/SuperAdmin/aggrementdownloadexcel/<?php echo $row['id'];?>"><button type="button" class="btn btn-gradient-info m-1">Download</button></a></td>
                    </tr>
                    <?php }?>
                </tbody>
                <tfoot>
                      <tr>
                        <th colspan="12">
                          <!-- Paginate -->
                          <div class="pagination" style='margin-top: 10px;'>
                            <?= $pagination; ?>
                          </div>
                        </th>
                      </tr>
                
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2020 
        </div>
      </div>
    </footer>
	<!--End footer-->
	
	<!--start color switcher-->
   <div class="right-sidebar">
    <div class="switcher-icon">
      <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
    </div>
    <div class="right-sidebar-content">

      <p class="mb-0">Gaussion Texture</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme1"></li>
        <li id="theme2"></li>
        <li id="theme3"></li>
        <li id="theme4"></li>
        <li id="theme5"></li>
        <li id="theme6"></li>
      </ul>

      <p class="mb-0">Gradient Background</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme7"></li>
        <li id="theme8"></li>
        <li id="theme9"></li>
        <li id="theme10"></li>
        <li id="theme11"></li>
        <li id="theme12"></li>
		<li id="theme13"></li>
        <li id="theme14"></li>
        <li id="theme15"></li>
      </ul>
      
     </div>
   </div>
  <!--end color switcher-->
   
  </div><!--End wrapper-->


  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="<?php echo base_url();?>assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- Metismenu js -->
  <script src="<?php echo base_url();?>assets/plugins/metismenu/js/metisMenu.min.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url();?>assets/js/app-script.js"></script>

  <!--Data Tables js-->
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
      } );

    </script>
	
</body>
<style>
  .pagination {
      display: inline-block;
  }
  .pagination strong {
      border: 1px solid #13dafe;
      background-color: #13dafe;
      color: #fff !important;
      box-sizing: border-box;
      display: inline-block;
      min-width: 1.5em;
      padding: 0.4em 0.4em;
      margin-left: 2px;
      text-align: center;
      text-decoration: none !important;
      cursor: pointer;
      border-radius: 2px;
  }
  .pagination a {
      border: 1px solid #13dafe;
      background-color: #13dafe;
      color: #fff !important;
      box-sizing: border-box;
      display: inline-block;
      min-width: 1.5em;
      padding: 0.2em 0.2em;
      margin-left: 2px;
      text-align: center;
      text-decoration: none !important;
      cursor: pointer;
      border-radius: 2px;
  }
  #example_paginate{
    display:none;
  }
</style>
<!-- Mirrored from codervent.com/dashtreme/demo/transparent-admin/vertical-layout/table-data-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 11:12:17 GMT -->
</html>



