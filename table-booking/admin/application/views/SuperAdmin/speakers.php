<?php include "head.php";?>
<?php include "header.php";?>
<?php include "sidebar.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">View Speakers Data</h4>
		    
	   </div>
	   <div class="col-sm-3">
       
     </div>
     </div>
   
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <h5 class="card-title"></h5>
            
                <div class="row">
               <div class="col-sm-6">
              <div class="btn-group float-sm-right">
                  <form method='post' action="<?= base_url() ?>index.php/SuperAdmin/speaker_agreement" >
                   <label><input type="search" class="form-control form-control-sm" name='search' placeholder="Full Records Search" aria-controls="example" value="<?= $search ?>"></label>
                  </form>
    
               </div>
           </div>
            <div class="col-sm-6">
               <a href="<?php echo base_url();?>index.php/SuperAdmin/add_speaker"><button type="button" class="btn btn-gradient-info m-1 pull-right">Add Speakers</button></a>
            </div>
     
      </div>
            <div class="card-body">
              <h4 class="box-title m-b-0 text-center" style="color:red;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('error');?> <?php echo $this->session->flashdata('suspend');?> </h4>        
        <h4 class="box-title m-b-0 text-center" style="color:#00FF00;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('success');?> </h4>
              <div class="table-responsive">
                  <form action="<?php echo base_url();?>index.php/eventadmin/change_status" method="post" name="event_list">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                       <th scope="col">#</th>
                       <th scope="col">Speaker Name</th>
                       <th scope="col">Mobile Number</th>
                       <th scope="col">Email</th>
                       <th scope="col" width="5%">Credential</th>
                       <th scope="col" colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
					<?php
                      $i=1; 
                      foreach ($results as $row) {
                      ?>
                   <tr>
                      <th scope="row"><?php echo $i++;?></th>
                      <td><?php echo $row['name'];?></td>
                      <td><?php echo $row['contact'];?></td>
                      <td><?php echo $row['email'];?></td>
                      <td width="5%"><?php echo $row['credential_qualification'];?></td>
                      <td><a href="<?php echo base_url();?>index.php/SuperAdmin/edit_speaker/<?php echo $row['id'];?>"><button type="button" class="btn btn-gradient-info m-1">Edit</button></a></td>
                      <td><a href="<?php echo base_url();?>index.php/SuperAdmin/delete_speaker/<?php echo $row['id'];?>"><button type="button" class="btn btn-gradient-danger m-1" onclick="return confirm('Are you sure you want to delete this speaker')">Delete</button></a></td>
                    </tr>
                    <?php }?>
						
                </tbody>
                <tfoot>
                   <tr>
                        <th colspan="12">
                          <!-- Paginate -->
                          <div class="pagination" style='margin-top: 10px;'>
                            <?= $pagination; ?>
                          </div>
                        </th>
                      </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2020 
        </div>
      </div>
    </footer>
	<!--End footer-->
	
	<!--start color switcher-->
   <div class="right-sidebar">
    <div class="switcher-icon">
      <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
    </div>
    <div class="right-sidebar-content">

      <p class="mb-0">Gaussion Texture</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme1"></li>
        <li id="theme2"></li>
        <li id="theme3"></li>
        <li id="theme4"></li>
        <li id="theme5"></li>
        <li id="theme6"></li>
      </ul>

      <p class="mb-0">Gradient Background</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme7"></li>
        <li id="theme8"></li>
        <li id="theme9"></li>
        <li id="theme10"></li>
        <li id="theme11"></li>
        <li id="theme12"></li>
		<li id="theme13"></li>
        <li id="theme14"></li>
        <li id="theme15"></li>
      </ul>
      
     </div>
   </div>
  <!--end color switcher-->
   
  </div><!--End wrapper-->


  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="<?php echo base_url();?>assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- Metismenu js -->
  <script src="<?php echo base_url();?>assets/plugins/metismenu/js/metisMenu.min.js"></script>
  
  <!-- Custom scripts -->
  <script src="<?php echo base_url();?>assets/js/app-script.js"></script>

  <!--Data Tables js-->
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
      } );

    </script>
       <script>
setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
  setTimeout(function() {
            $('#timeout1').fadeToggle('slow');
            }, 3000);
      

</script>
  <style>
  .pagination {
      display: inline-block;
  }
  .pagination strong {
      border: 1px solid #13dafe;
      background-color: #13dafe;
      color: #fff !important;
      box-sizing: border-box;
      display: inline-block;
      min-width: 1.5em;
      padding: 0.4em 0.4em;
      margin-left: 2px;
      text-align: center;
      text-decoration: none !important;
      cursor: pointer;
      border-radius: 2px;
  }
  .pagination a {
      border: 1px solid #13dafe;
      background-color: #13dafe;
      color: #fff !important;
      box-sizing: border-box;
      display: inline-block;
      min-width: 1.5em;
      padding: 0.2em 0.2em;
      margin-left: 2px;
      text-align: center;
      text-decoration: none !important;
      cursor: pointer;
      border-radius: 2px;
  }
  #example_paginate{
    display:none;
  }
</style>
	
</body>

<!-- Mirrored from codervent.com/dashtreme/demo/transparent-admin/vertical-layout/table-data-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 Jul 2020 11:12:17 GMT -->
</html>






















