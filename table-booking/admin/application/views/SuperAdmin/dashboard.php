<?php include "head.php";?>
<?php include "header.php";?>
<?php include "sidebar.php";?>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
   <div class="row mt-3">
    <div class="col-12 col-lg-6 col-xl-4">
      <div class="card">
      <div class="card-body">
          <p class="text-white mb-0">Total Number of Booking<span class="float-right badge badge-light"></span></p>
           <div class="">
           <h4 class="mb-0 py-3"><?php echo count($bookings);?><span class="float-right"><i class="fa fa-home"></i></span></h4>
           </div>
           <div class="progress-wrapper">
            <div class="progress" style="height:5px;">
            <div class="progress-bar" style="width:60%"></div>
             </div>
          </div>
          <!--<p class="mb-0 mt-2 text-white small-font">Compare to last month <span class="float-right">+15% <i class="fa fa-long-arrow-up"></i></span></p>-->
        </div>
      </div>
     </div>
		
	

   </div><!--End Row-->
   

<!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
    </div>
    <!-- End container-fluid-->

   </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	
<?php include "footer.php";?>