
<body class="bg-theme bg-theme3">

<!-- Start wrapper-->
 <div id="wrapper">

 <!--Start sidebar-wrapper-->
   <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
   
    <div class="brand-logo">
	  <img src="<?php echo base_url();?>assets/images/logo.png" class="logo-icon" alt="logo icon">
	  <h5 class="logo-text">Iris Premiere</h5>
	  <div class="close-btn"><i class="zmdi zmdi-close"></i></div>
   </div>
	  
     <ul class="metismenu" id="menu">
		
		<li><a href="<?php echo base_url();?>index.php/SuperAdmin/dashboard">	<div class="parent-icon"><i class="zmdi zmdi-view-dashboard"></i></div><div class="menu-title">Dashboard</div>
		 </a>
		 </li>
		 <li><a href="<?php echo base_url();?>index.php/SuperAdmin/booking_list">	<div class="parent-icon"><i class="zmdi zmdi-layers"></i></div><div class="menu-title">Registered Booking</div>
		 </a>
		 </li>
		 
	<!--	 <li><a href="<?php echo base_url();?>index.php/SuperAdmin/users"><div class="parent-icon"><i class="zmdi zmdi-grid"></i></div><div class="menu-title">User Data</div>
		 </a>
		 </li>-->
		
		
		<!-- <li><a href="<?php echo base_url();?>index.php/Eventadmin"><div class="parent-icon"><i class="zmdi zmdi-chart"></i></div><div class="menu-title">Create Event</div>
		 </a></li>
		 <li><a href="<?php echo base_url();?>index.php/Eventadmin/event_list">	<div class="parent-icon"><i class="zmdi zmdi-widgets"></i></div><div class="menu-title">Event List</div>
		 </a></li>
		 <li><a href="<?php echo base_url();?>index.php/Eventadmin/event_approved_list">	<div class="parent-icon"><i class="zmdi zmdi-format-list-bulleted"></i></div><div class="menu-title">Event Approved List</div>
		 </a></li>
		
		 <li><a href="<?php echo base_url();?>index.php/SuperAdmin/speaker_agreement">	<div class="parent-icon"><i class="zmdi zmdi-card-travel"></i></div><div class="menu-title">Speaker Agreement</div>
		 </a> </li>
		 <li><a href="<?php echo base_url();?>index.php/SuperAdmin/aggrementdownload">	<div class="parent-icon"><i class="zmdi zmdi-card-travel"></i></div><div class="menu-title">Speaker Agreement Download</div>
		 </a> </li>
		 
		 <li><a href="<?php echo base_url();?>index.php/SuperAdmin/attendedreport"><div class="parent-icon"><i class="zmdi zmdi-download"></i></div><div class="menu-title">Attended Report</div>
		 </a> </li>
		 
		 <li><a href="<?php echo base_url();?>index.php/SuperAdmin/dsmreport"><div class="parent-icon"><i class="zmdi zmdi-download"></i></div><div class="menu-title">DSM Report</div>
		 </a> </li> -->
		 
		 
		
	   </ul>
   
   </div>
   <!--End sidebar-wrapper-->