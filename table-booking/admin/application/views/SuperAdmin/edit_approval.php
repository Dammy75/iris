<?php include "head.php";?>
<?php include "header.php";?>
<?php include "sidebar.php";?>
<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
 <div class="card">
      <div class="card-header text-uppercase">Edit Event Approval</div>
       <div class="card-body">
      	<h4 class="box-title m-b-0 text-center" style="color:red;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('error');?> <?php echo $this->session->flashdata('suspend');?> </h4>				
				<h4 class="box-title m-b-0 text-center" style="color:#00FF00;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('success');?> </h4>	

          <div class="row">
			<div class="col-lg-12">
			   <?php  //echo '<pre>';print_r($results);die;?>
			   <div class="card">
			     <div class="card-body">
				   <form action="<?php echo base_url();?>index.php/eventadmin/change_status" method="post" enctype="multipart/form-data">
				       <div class="form-group row">
						<label for="input-25" class="col-sm-4 col-form-label">Event Title</label>
						<div class="col-sm-8">
						    <input readonly type="text" class="form-control" value="<?php echo $results[0]['title'];?>">
    						
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-4 col-form-label">Event Title</label>
						<div class="col-sm-8">
						    <input readonly type="text" class="form-control" value="<?php echo $results[0]['speaker_name'];?>">
    						
						</div>
					  </div>
					  <div class="form-group row">
						<label for="input-25" class="col-sm-4 col-form-label">Event Approval by Admin *</label>
						<div class="col-sm-5">
						    <input type="hidden" name="event_id" id="event_id" value="<?php echo $results[0]['id'];?>">
    						<select class="form-control single-select" name="approved_by_admin" id="approved_by_admin">
                                <option <?php if($results[0]['approved_by_admin']=='1'){ echo 'selected'; } ?> value="yes" >Yes</option>
                                <option <?php if($results[0]['approved_by_admin']=='0'){ echo 'selected'; } ?> value="no">No</option>
                            </select>
						</div>
						<div class="col-sm-3"></div>
					  </div>
					    
				  <button type="submit" name="edit_admin_approval" id="edit_admin_approval" class="btn btn-gradient-info m-1"><i class="icon-lock"></i> Edit</button>
					</form>
				 </div>
			 </div>


       	</div>
		  </div><!--End Row-->
 
          </div><!--end row-->

      
       </div>
     </div>
 </div>  
	  
		

      <!--End Dashboard Content-->
    <!--start overlay-->
	  <div class="overlay"></div>
	<!--end overlay-->
	
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<?php include "footer.php";?>
		<script>
setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
	setTimeout(function() {
            $('#timeout1').fadeToggle('slow');
            }, 3000);
			

</script>