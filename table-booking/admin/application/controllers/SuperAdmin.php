<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class SuperAdmin extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
		
		$this->load->library('form_validation');
		$this->load->library('session');		
		$this->load->helper('cookie');			
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('common_model');
		$this->load->database();
	}
	
	public function dashboard()
	{
	       $bookings=$this->common_model->get_records('tbl_booking','','','');
	      
           $this->load->view('SuperAdmin/dashboard',array('bookings'=>$bookings,));

    }
	
	public function users()
	{
           $this->load->view('SuperAdmin/userdata');
	
	}
	
		// Doctor Module
    public function booking_list($rowno=0)
	{
        $this->load->helper('url');
        // Load session
        $this->load->library('session');
        // Load Pagination library
        $this->load->library('pagination');

        // Search text
        $search_text = "";
        if($this->input->post('search') != NULL ){
          $search_text = trim($this->input->post('search'));
          $this->session->set_userdata(array("search"=>$search_text));
        }else{
          if($this->session->userdata('search') != NULL){
            $search_text = $this->session->userdata('search');
          }
        }
    
        // Row per page
        $rowperpage = 10;
    
        // Row position
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
     
        // All records count
         $allcount = $this->base_models->getrecordCountBooking($search_text); 
    
        // Get records
        $users_record = $this->base_models->getBookingData($rowno,$rowperpage,$search_text);
        $this->session->set_userdata(array("search"=>'')); //empty search

        // Pagination Configuration
        $config['base_url'] = base_url().'index.php/SuperAdmin/booking_list';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
    
        // Initialize
        $this->pagination->initialize($config);
     
        $pagedata['pagination'] = $this->pagination->create_links();
        $pagedata['results'] = $users_record;
        $pagedata['row'] = $rowno;
        $pagedata['search'] = $search_text;
	
			
		$this->load->view('SuperAdmin/booking',$pagedata);
	}	


	public function edit_booking($id)
		{
			$rs = $this->db->query("select * from tbl_booking where booking_id='$id'");
	        $pagedata['results'] = $rs->result_array();
			$this->load->view('SuperAdmin/edit_booking',$pagedata);
		}
		
		
		public function update_booking()
	{
		
 		//old date check
        if($this->input->post('newbooking_date')=='')
        	{
        		$date = date('Y-m-d', strtotime($_POST['oldbooking_date']));
        	}
        	else
        	{
        		$date = date('Y-m-d', strtotime($_POST['newbooking_date']));
        	}
		
		 $time              = date('h:i:s', strtotime($_POST['oldbooking_time']));
		 $updated_date_time = $date.' '.$time; 	
		 //status check
		    if($this->input->post('status')!='0' && $this->input->post('payment_id')!=='' )
        	{
        		$status ='2';//payment siccess
        	}
        	else
        	{
        		$status = $this->input->post('status');
        	}

				
		$id = $this->input->post('id');
        $where_array	=	 array('booking_id'=>$id);
				$update_arr	=	array(
									'name'   				=> $this->input->post('name'),
									'mobile'   				=> $this->input->post('mobile_no'),
									'email'   				=> $this->input->post('email'),
									'type'   				=> $this->input->post('type'),
									'total_people'   		=> $this->input->post('total_people'),
									'booking_date_time'     => $updated_date_time,
									'status'     			=> $status,
									'payment_id'   			=> $this->input->post('payment_id'),
									'updated_on'			=> date("Y-m-d H:i:s")
								);
		if($this->base_models->update_records('tbl_booking',$update_arr,$where_array)){
/*echo $this->db->last_query();die;*/

				$this->session->set_flashdata('success','Booking is updated successfully');
			}else{
				$this->session->set_flashdata('error','Not updated Please try again');
			}
			redirect(base_url('index.php/SuperAdmin/edit_booking/'.$id));
	}


	public function delete_booking($id)
	{
			$where_array	=	 array('booking_id'=>$id);
				$update_arr	=	array(
									'status'   			=> '0',
									'deleted_on'		=> date("Y-m-d H:i:s")
								);
		if($this->base_models->update_records('tbl_booking',$update_arr,$where_array)){

				$this->session->set_flashdata('success','Booking is Deleted successfully');
			}else{
				$this->session->set_flashdata('error','Not updated Please try again');
			}
			redirect(base_url('index.php/SuperAdmin/booking_list/'));
	}


	public function validate()
	{
		//print_r($_POST);
		 if($this->input->post('newbooking_date')=='')
        	{
        		$date = date('Y-m-d', strtotime($_POST['oldbooking_date']));
        	}
        	else
        	{
        		$date = date('Y-m-d', strtotime($_POST['newbooking_date']));
        	}
		
		 $time              = date('h:i:s', strtotime($_POST['oldbooking_time']));
		
		$updated_date_time = $date.' '.$time; 	
	    $total_people      = $this->input->post('total_people')	;
	    $type              = $this->input->post('type')	;
	    $mno              = $this->input->post('mobile_no')	;

		
    
		$data = $this->db->query("select sum(total_people) AS NumberOfPeople from tbl_booking  where`booking_date_time`='$updated_date_time' and  `type`='$type' and  `status`='1'")->result_array();
	
			
       $cntdata = @$data[0]['NumberOfPeople'];
       $totalpeoplecnt =$cntdata +  $total_people;

       if($type =='1' && $cntdata > '56'){
               echo "Seats Are Full At That Time Slot. Kindly Change Time Slot.";
               return FALSE;

     }else if($type =='1' && $totalpeoplecnt  > '56'){
               echo "Seats Are Full At That Time Slot. Kindly Change Time Slot Or number of people.";
               return FALSE;

     }else if($type =='2' && $cntdata > '70' || $type =='2' && $totalpeoplecnt  > '70'){
               echo "Seats Are Full At That Time Slot. Kindly Change Time Slot.";
               return FALSE;

     }else {
          $data1 = $this->db->query("select booking_id from tbl_booking  where`booking_date_time`='$updated_date_time' and  `mobile`='$mno' and  `status`='1'")->result_array(); 
          $isrecord = count($data1);
          if($isrecord>0){
          	  echo "You Are Already Registred For This Time Slot.";
 					return FALSE;
          }else{
          //	echo $this->session->set_flashdata('success','Booking is updated successfully');
          	echo "Booking is Updated Successfully.";
          			return TRUE;
          }
     }

	}







/*	public function doctor_list()
	{

	
		   $username = $this->session->userdata('username'); 
		 	$select	 = array('id','manager_one_code,','doctor_name','sbu_code','email','contact');
			$where = array();
			$pagedata['delete_link'] = 'SuperAdmin/delete_item';
			
        
          //Pagination Start
			$config = array();
	        $config["base_url"] = site_url() . "/SuperAdmin/doctor_list";
	        $config["total_rows"] = $this->base_models->get_count('id','doctors', $where);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $pagedata["links"] = $this->pagination->create_links();
	        $pagedata['results'] = $this->base_models->get_pagination('doctors', $where,'id',$config["per_page"], $page);     
           	//Pagination End
			
			$this->load->view('SuperAdmin/doctors',$pagedata);
	}*/
	
	public function add_doctor()
	{
		$this->load->view('SuperAdmin/add_doctor');
	}
	
	public function insert_doctor()
	{
	       
				$insert_arr	=	array(
									'manager_one_code'   	=> $this->input->post('usercode'),
									'doctor_name'   		=> $this->input->post('name'),
									'contact'   			=> $this->input->post('mobile_no'),
									'flag'                 	=>"Non-Rxber",
									'email'   				=> $this->input->post('email'),
									'inserted_on'			=> date("Y-m-d H:i:s")
								);
		if($this->db->insert("doctors",$insert_arr)){

				$this->session->set_flashdata('success','Doctors Data is Added successfully');
			}else{
				$this->session->set_flashdata('error','Not added Please try again');
			}
			redirect(base_url('index.php/SuperAdmin/add_doctor/'));
	}
	
	public function edit_doctor($id)
		{
			$rs = $this->db->query("select * from doctors where id='$id'");
	        $pagedata['results'] = $rs->result_array();
			$this->load->view('SuperAdmin/edit_doctor',$pagedata);
		}
		
		
		public function update_doctor()
	{
		$id = $this->input->post('id');
        $where_array	=	 array('id'=>$id);
				$update_arr	=	array(
									'doctor_name'   			=> $this->input->post('name'),
									'contact'   				=> $this->input->post('mobile_no'),
									'email'   				=> $this->input->post('email'),
									'updated_on'			=> date("Y-m-d H:i:s")
								);
		if($this->base_models->update_records('doctors',$update_arr,$where_array)){

				$this->session->set_flashdata('success','Doctors Data is updated successfully');
			}else{
				$this->session->set_flashdata('error','Not updated Please try again');
			}
			redirect(base_url('index.php/SuperAdmin/edit_doctor/'.$id));
	}

	public function delete_doctor($id)
	{
			$where_array	=	 array('id'=>$id);
				$update_arr	=	array(
									'status'   			=> 2,
									'deleted_on'		=> date("Y-m-d H:i:s")
								);
		if($this->base_models->update_records('doctors',$update_arr,$where_array)){

				$this->session->set_flashdata('success','Doctors Data is Deleted successfully');
			}else{
				$this->session->set_flashdata('error','Not updated Please try again');
			}
			redirect(base_url('index.php/SuperAdmin/doctor_list/'));
	}
	// End Doctor Module
	
	
	
// Speaker Module
	public function speaker_agreement($rowno=0)
	{
        $this->load->helper('url');
        // Load session
        $this->load->library('session');
        // Load Pagination library
        $this->load->library('pagination');

        // Search text
        $search_text = "";
        if($this->input->post('search') != NULL ){
          $search_text = trim($this->input->post('search'));
          $this->session->set_userdata(array("search"=>$search_text));
        }else{
          if($this->session->userdata('search') != NULL){
            $search_text = $this->session->userdata('search');
          }
        }
    
        // Row per page
        $rowperpage = 10;
    
        // Row position
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
     
        // All records count
         $allcount = $this->base_models->getrecordCountSpeaker($search_text); 
    
        // Get records
        $users_record = $this->base_models->getSpeakerData($rowno,$rowperpage,$search_text);
        $this->session->set_userdata(array("search"=>'')); //empty search

        // Pagination Configuration
        $config['base_url'] = base_url().'index.php/SuperAdmin/speaker_agreement';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
    
        // Initialize
        $this->pagination->initialize($config);
     
        $pagedata['pagination'] = $this->pagination->create_links();
        $pagedata['results'] = $users_record;
        $pagedata['row'] = $rowno;
        $pagedata['search'] = $search_text;
        $this->load->view('SuperAdmin/speakers',$pagedata);

	}

/*	public function speaker_agreement()
	{
			$username = $this->session->userdata('username'); 
		 	$select	 = array('id','name,','email','contact','credential_qualification');
			$where = array();
			$pagedata['delete_link'] = 'Adminitems/delete_item';
			
        
          //Pagination Start
			$config = array();
	        $config["base_url"] = site_url() . "/SuperAdmin/doctor_list";
	        $config["total_rows"] = $this->base_models->get_count('id','speakers', $where);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $pagedata["links"] = $this->pagination->create_links();
	        $pagedata['results'] = $this->base_models->get_pagination('speakers', $where,'id',$config["per_page"], $page);     
           	//Pagination End
			
			$this->load->view('SuperAdmin/speakers',$pagedata);
	}*/
	
	public function add_speaker()
	{
		$this->load->view('SuperAdmin/add_speaker');
	}
	
	public function insert_speaker()
	{
		  /*  $username = $this->session->userdata('username'); 
		    $rs = $this->db->query("select manager_one_zone from manager_one where manager_two_code='$username'");
            $array = $rs->result_array();
            $zone = @$array[0]['manager_one_zone'];
            
              $rs1 = $this->db->query("select id from manager_two where manager_two_code='$username'");
            $array1 = $rs1->result_array();
            $ref_id = @$array1[0]['id'];*/
		//print_r($_SESSION);
        $latest_cv			=	time().$_FILES['latest_cv']['name'];
        $speaker_photo		=	time().$_FILES['speaker_photo']['name'];
        $cheque_photo		=	time().$_FILES['cheque_photo']['name'];
        $pancard    		=	time().$_FILES['pancard']['name'];
        $gst 				=	time().$_FILES['gst']['name'];

		$insert_arr	=	array(
									'name'   					=> $this->input->post('speaker_name'),
									'email'   					=> $this->input->post('email'),
									'contact'   				=> $this->input->post('mobile_no'),
									'added_by'   				=> 1,
									'ref_id'   					=> 1,
									'manager_code'   			=> "Super Admin",
									'zone_id'   				=> 0,
									'headquarters'   			=> $this->input->post('headquarter'),
									'region'   					=> $this->input->post('region'),
									'medical_registration_number'=> $this->input->post('med_reg_no'),
									'sbu_code'   				=> $this->input->post('sbu_code'),
									'address'   				=> $this->input->post('address'),
									'government_doctor'   		=> $this->input->post('is_government_doctor'),
									'latest_cv'   				=> $latest_cv,
									'speaker_photo'   			=> $speaker_photo,
									'cancelled_cheque'   		=> $cheque_photo,
									'pan_card'   				=> $pancard,
									'gst_letter'   				=> $gst,
									'credential_qualification'  => $this->input->post('qualification'),
									'inserted_on'				=> date("Y-m-d H:i:s")
								);
		/*	move_uploaded_file($_FILES['latest_cv']['tmp_name'],'uploads/'.$latest_cv);
			move_uploaded_file($_FILES['speaker_photo']['tmp_name'],'uploads/'.$speaker_photo);
			move_uploaded_file($_FILES['cheque_photo']['tmp_name'],'uploads/'.$cheque_photo);
			move_uploaded_file($_FILES['pancard']['tmp_name'],'uploads/'.$pancard);
			move_uploaded_file($_FILES['gst']['tmp_name'],'uploads/'.$gst);*/

		if($this->db->insert("speakers",$insert_arr)){
	       
	       	$last_inserted_id = $this->db->insert_id();
	 		mkdir("uploads/speakers/". 	$last_inserted_id ."/");
			move_uploaded_file($_FILES["latest_cv"]["tmp_name"], "uploads/speakers/". $last_inserted_id ."/".$latest_cv);
			move_uploaded_file($_FILES["speaker_photo"]["tmp_name"], "uploads/speakers/". $last_inserted_id ."/".$speaker_photo);
			move_uploaded_file($_FILES["cheque_photo"]["tmp_name"], "uploads/speakers/". $last_inserted_id ."/".$cheque_photo);
			move_uploaded_file($_FILES["pancard"]["tmp_name"], "uploads/speakers/". $last_inserted_id ."/".$pancard);
			move_uploaded_file($_FILES["gst"]["tmp_name"], "uploads/speakers/". $last_inserted_id ."/".$gst);


				$this->session->set_flashdata('success','Speaker Data is Added successfully');
			}else{
				$this->session->set_flashdata('error','Not added Please try again');
			}
			redirect(base_url('index.php/SuperAdmin/add_speaker/'));

	
	}
	
	public function edit_speaker($id)
	{
		$rs = $this->db->query("select * from speakers where id='$id'");
        $pagedata['results'] = $rs->result_array();
		$this->load->view('SuperAdmin/edit_speaker',$pagedata);
	}
	
	public function update_speaker()
	{
		/*echo '<pre>';
	 
		print_r($_FILES);
		print_r($_POST);
		die;*/
		$id = $this->input->post('id');
		
		$file = "uploads/speakers/".$id ."/";
		if(!is_dir($file)) {
				mkdir("uploads/speakers/". 	$id ."/");
		}

		if($_FILES['latest_cv']['name']==''){
			$latest_cv = $this->input->post('old_latest_cv');
		}else{
			  $latest_cv  =	time().$_FILES['latest_cv']['name'];
			  move_uploaded_file($_FILES['latest_cv']['tmp_name'],"uploads/speakers/". $id ."/".$latest_cv);
		}

		if($_FILES['speaker_photo']['name']==''){
			$speaker_photo = $this->input->post('old_speaker_photo');
		}else{
			  $speaker_photo  =	time().$_FILES['speaker_photo']['name'];
			  move_uploaded_file($_FILES['speaker_photo']['tmp_name'],"uploads/speakers/". $id ."/".$speaker_photo);
		}

		if($_FILES['cheque_photo']['name']==''){
			$cheque_photo = $this->input->post('old_cancelled_cheque');
		}else{
			  $cheque_photo  =	time().$_FILES['cheque_photo']['name'];
			  move_uploaded_file($_FILES['cheque_photo']['tmp_name'],"uploads/speakers/". $id ."/".$cheque_photo);
		}

		if($_FILES['pancard']['name']==''){
			$pancard = $this->input->post('old_pan_card');
		}else{
			  $pancard  =	time().$_FILES['pancard']['name'];
			  move_uploaded_file($_FILES['pancard']['tmp_name'],"uploads/speakers/". $id ."/".$pancard);
		}

		if($_FILES['gst']['name']==''){
			$gst = $this->input->post('old_gst_letter');
		}else{
			  $gst  =	time().$_FILES['gst']['name'];
			  move_uploaded_file($_FILES['gst']['tmp_name'],"uploads/speakers/". $id ."/".$gst);
		}

		
        $where_array	=	 array('id'=>$id);
				$update_arr	=	array(
									'name'   					=> $this->input->post('speaker_name'),
									'email'   					=> $this->input->post('email'),
									'contact'   				=> $this->input->post('mobile_no'),
									'headquarters'   			=> $this->input->post('headquarter'),
									'region'   					=> $this->input->post('region'),
									'medical_registration_number'=> $this->input->post('med_reg_no'),
									'sbu_code'   				=> $this->input->post('sbu_code'),
									'address'   				=> $this->input->post('address'),
									'government_doctor'   		=> $this->input->post('is_government_doctor'),
									'latest_cv'   				=> $latest_cv,
									'speaker_photo'   			=> $speaker_photo,
									'cancelled_cheque'   		=> $cheque_photo,
									'pan_card'   				=> $pancard,
									'gst_letter'   				=> $gst,
									'credential_qualification'  => $this->input->post('qualification'),
									'updated_on'			=> date("Y-m-d H:i:s")
								);
		if($this->base_models->update_records('speakers',$update_arr,$where_array)){

				$this->session->set_flashdata('success','Speakers Data is updated successfully');
			}else{
				$this->session->set_flashdata('error','Not updated Please try again');
			}
			redirect(base_url('index.php/SuperAdmin/edit_speaker/'.$id));
	}

	public function delete_speaker($id)
	{
			$where_array	=	 array('id'=>$id);
				$update_arr	=	array(
									'status'   			=> 2,
									'deleted_on'		=> date("Y-m-d H:i:s")
								);
		if($this->base_models->update_records('speakers',$update_arr,$where_array)){

				$this->session->set_flashdata('success','Speaker Data is Deleted successfully');
			}else{
				$this->session->set_flashdata('error','Not updated Please try again');
			}
			redirect(base_url('index.php/SuperAdmin/speaker_agreement/'));
	}
	//End Speaker Module
	
	public function aggrementdownload($rowno=0)
	{
			
        $this->load->helper('url');
        // Load session
        $this->load->library('session');
        // Load Pagination library
        $this->load->library('pagination');

        // Search text
        $search_text = "";
        if($this->input->post('search') != NULL ){
          $search_text = trim($this->input->post('search'));
          $this->session->set_userdata(array("search"=>$search_text));
        }else{
          if($this->session->userdata('search') != NULL){
            $search_text = $this->session->userdata('search');
          }
        }
    
        // Row per page
        $rowperpage = 10;
    
        // Row position
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
     
        // All records count
         $allcount = $this->base_models->getrecordCountSpeaker($search_text); 
    
        // Get records
        $users_record = $this->base_models->getSpeakerData($rowno,$rowperpage,$search_text);
        $this->session->set_userdata(array("search"=>'')); //empty search

        // Pagination Configuration
        $config['base_url'] = base_url().'index.php/SuperAdmin/aggrementdownload';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
    
        // Initialize
        $this->pagination->initialize($config);
     
        $pagedata['pagination'] = $this->pagination->create_links();
        $pagedata['results'] = $users_record;
        $pagedata['row'] = $rowno;
        $pagedata['search'] = $search_text;
			
			$this->load->view('SuperAdmin/aggrementdownload',$pagedata);
	}

	/*public function aggrementdownload()
	{
			$username = $this->session->userdata('username'); 
		 	$select	 = array('id','name,','email','contact','credential_qualification');
			$where = array();
			$pagedata['delete_link'] = 'Adminitems/delete_item';
			
        
          //Pagination Start
			$config = array();
	        $config["base_url"] = site_url() . "/SuperAdmin/doctor_list";
	        $config["total_rows"] = $this->base_models->get_count('id','speakers', $where);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $pagedata["links"] = $this->pagination->create_links();
	        $pagedata['results'] = $this->base_models->get_pagination('speakers', $where,'id',$config["per_page"], $page);     
           	//Pagination End
			
			$this->load->view('SuperAdmin/aggrementdownload',$pagedata);
	}*/
	
public function aggrementdownloadexcel($id){
	$rs = $this->db->query("select name from speakers where id='$id'");
            $array = $rs->result_array();
    $name = @$array[0]['name'];
   $this->load->library('zip');
   // File name
        $filename =  $name;
        // Directory path (uploads directory stored in project root)
        $path = 'uploads/speakers/'.$id.'/';

        // Add directory to zip
        $this->zip->read_dir($path,FALSE);

        // Save the zip file to archivefiles directory
        $this->zip->archive(FCPATH.'/archivefiles/'.$filename);

        // Download
        $this->zip->download($filename);
	redirect(base_url('index.php/SuperAdmin/aggrementdownload/'));
}

	
/*	public function aggrementdownloadexcel($id){
 $rs = $this->db->query("select name,latest_cv,speaker_photo,cancelled_cheque,pan_card,gst_letter from speakers where id='$id'");
            $array = $rs->result_array();
$name = @$array[0]['name'];
 	$latest_cv = @$array[0]['latest_cv'];
 	$speaker_photo = @$array[0]['speaker_photo'];
 	$cancelled_cheque = @$array[0]['cancelled_cheque'];
 	$gst_letter = @$array[0]['gst_letter'];
 	$pan_card = @$array[0]['pan_card'];
	$this->load->library('zip');

$path = 'uploads/speakers/'.$id;

$this->zip->read_dir($path);

$filename =$name;
$this->zip->download($filename); 

  //Get real path for our folder
$rootPath = realpath($path);
	redirect(base_url('index.php/SuperAdmin/aggrementdownload/'));
}*/
    
   	public function dsmreport($rowno=0)
	{
			
        $this->load->helper('url');
        // Load session
        $this->load->library('session');
        // Load Pagination library
        $this->load->library('pagination');


        	// Search text
        $search_text = "";
         $this->session->unset_userdata('search');
        if($this->input->post('search') != NULL ){
        	
          $search_text = trim($this->input->post('search'));
          $this->session->set_userdata(array("search"=>$search_text));
        }else{
        if($this->session->userdata('search') != NULL){
            $search_text = $this->session->userdata('search');
          }
        }

        // Search date
        $fromdate_text = "";
        if($this->input->post('fromdate') != NULL ){
        	$fromdate = (@$this->input->post('fromdate')) ? $this->input->post('fromdate').' 00:00:00' : '';
          $fromdate_text = date('Y-m-d H:i:s', strtotime($fromdate));
          $this->session->set_userdata(array("fromdate"=>$fromdate_text));
        }else{
          if($this->session->userdata('fromdate') != NULL){
            $fromdate_text = $this->session->userdata('fromdate');
          }
        }
       
       $todate_text = "";
        if($this->input->post('todate') != NULL ){
        	  	$toodate = (@$this->input->post('todate')) ? $this->input->post('todate').' 23:59:00' : '';
          $todate_text = date('Y-m-d H:i:s', strtotime($toodate));;
          $this->session->set_userdata(array("todate"=>$todate_text));
        }else{
          if($this->session->userdata('todate') != NULL){
            $todate_text = $this->session->userdata('todate');
          }
        }
        // Row per page
        $rowperpage = 10;
    
        // Row position
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
      
        // All records count
         $allcount = $this->base_models->getrecordCountDsmEvent($fromdate_text,$todate_text,$search_text); 
        // print_r($this->db->last_query());   die;
    
        // Get records
        $users_record = $this->base_models->getDsmEvent($rowno,$rowperpage,$fromdate_text,$todate_text,$search_text);
       //   print_r($this->db->last_query());   die;
        $this->session->set_userdata(array("fromdate"=>'')); //empty search
        $this->session->set_userdata(array("todate"=>'')); //empty search

        // Pagination Configuration
        $config['base_url'] = base_url().'index.php/SuperAdmin/dsmreport';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
    
        // Initialize
        $this->pagination->initialize($config);
     
        $pagedata['pagination'] = $this->pagination->create_links();
        $pagedata['results'] = $users_record;
        $pagedata['row'] = $rowno;
        $pagedata['fromdate'] = $fromdate_text;
        $pagedata['todate'] = $todate_text;
        $pagedata['search'] = $search_text;
			
			$this->load->view('SuperAdmin/dsmreport',$pagedata);
	}
    
    public function dsmreportdownload(){
		    $data['data'] =$this->db->query("SELECT `event`.`id`,`event`.`title`, `event`.`speaker_name`, `event`.`manager_code`, `event`.`date_time`, `event`.`prescriber`, `event`.`non-prescriber`, `manager_one`.`manager_one_name`,`manager_one`.`manager_one_zone` FROM `event` LEFT JOIN `manager_one` ON `event`.`manager_code` = `manager_one`.`manager_one_code` WHERE `event`.`steps` != '1'")->result_array();					//echo '<pre>';print_r($data);die;
			$this->generate_report_excel($data['data']);
	}				
	//generate to excel	
	public function generate_report_excel($param1){
		// create file name
		$fileName = 'DSMReport'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		
                 $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Event Title');
				 $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Speaker Name');
				 $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Prescriber');
				 $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Non-Prescriber');
				 $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Total Event');
				 $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'DSM Name');
				 $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'DSM Zone');
				 $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Event Date');
				 $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Event Time');
				
	// set Row
		$rowCount = 2;
	//	echo '<pre>';print_r($info);die;
		foreach ($info as $element) {
			
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['title']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount,  $element['speaker_name']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['prescriber']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['non-prescriber']);
				$manager_code = $element['manager_code'];
				$event = $this->db->query("select COUNT(id) as eventcnt from event where `manager_code`='$manager_code'")->result_array();
				$cntevent = @$event[0]['eventcnt']; 
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $cntevent);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['manager_one_name']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['manager_one_zone']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, date('d-M-Y', strtotime($element['date_time'])));
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, date('g:iA', strtotime($element['date_time'])));
            $rowCount++;  
            }
			
			
		
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/excel/'.$fileName));
	}		


	//Attended Report

	public function attendedreport($rowno=0)
	{
			
        $this->load->helper('url');
        // Load session
        $this->load->library('session');
        // Load Pagination library
        $this->load->library('pagination');


        	// Search text
        $search_text = "";
         $this->session->unset_userdata('search');
        if($this->input->post('search') != NULL ){
        	
          $search_text = trim($this->input->post('search'));
          $this->session->set_userdata(array("search"=>$search_text));
        }else{
        if($this->session->userdata('search') != NULL){
            $search_text = $this->session->userdata('search');
          }
        }

        // Search date
        $fromdate_text = "";
        if($this->input->post('fromdate') != NULL ){
        	$fromdate = (@$this->input->post('fromdate')) ? $this->input->post('fromdate').' 00:00:00' : '';
          $fromdate_text = date('Y-m-d H:i:s', strtotime($fromdate));
          $this->session->set_userdata(array("fromdate"=>$fromdate_text));
        }else{
          if($this->session->userdata('fromdate') != NULL){
            $fromdate_text = $this->session->userdata('fromdate');
          }
        }
       
       $todate_text = "";
        if($this->input->post('todate') != NULL ){
        	  	$toodate = (@$this->input->post('todate')) ? $this->input->post('todate').' 23:59:00' : '';
          $todate_text = date('Y-m-d H:i:s', strtotime($toodate));;
          $this->session->set_userdata(array("todate"=>$todate_text));
        }else{
          if($this->session->userdata('todate') != NULL){
            $todate_text = $this->session->userdata('todate');
          }
        }
        // Row per page
        $rowperpage = 10;
    
        // Row position
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
      
        // All records count
         $allcount = $this->base_models->getrecordCountDsmEventAttended($fromdate_text,$todate_text,$search_text); 
        // print_r($this->db->last_query());   die;
    
        // Get records
        $users_record = $this->base_models->getDsmEventAttended($rowno,$rowperpage,$fromdate_text,$todate_text,$search_text);
       //   print_r($this->db->last_query());   die;
        $this->session->set_userdata(array("fromdate"=>'')); //empty search
        $this->session->set_userdata(array("todate"=>'')); //empty search

        // Pagination Configuration
        $config['base_url'] = base_url().'index.php/SuperAdmin/attendedreport';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
    
        // Initialize
        $this->pagination->initialize($config);
     
        $pagedata['pagination'] = $this->pagination->create_links();
        $pagedata['results'] = $users_record;
        $pagedata['row'] = $rowno;
        $pagedata['fromdate'] = $fromdate_text;
        $pagedata['todate'] = $todate_text;
        $pagedata['search'] = $search_text;
			
			$this->load->view('SuperAdmin/attendedreport',$pagedata);
	}

	public function view_approved_event()
	{
		$approved_record=$this->common_model->get_records('event','',array('id'=>$this->uri->segment(3)),'');
		$this->load->view('SuperAdmin/view_approved_eventreport',array('approved_record'=>$approved_record));
	}
    
    public function attendedreportdownload(){
		    $data['data'] =$this->db->query("SELECT `event`.`id`,`event`.`title`, `event`.`speaker_name`, `event`.`manager_code`, `event`.`date_time`, `event`.`prescriber`, `event`.`non-prescriber`,  `event`.`event_atten_pre`, `event`.`event_atten_nonpre`,`event`.`event_atten_names`,`manager_one`.`manager_one_name`,`manager_one`.`manager_one_zone` FROM `event` LEFT JOIN `manager_one` ON `event`.`manager_code` = `manager_one`.`manager_one_code` WHERE `event`.`steps` != '1'")->result_array();					//echo '<pre>';print_r($data);die;
			$this->generate_attendedreport_excel($data['data']);
	}				
	//generate to excel	
	public function generate_attendedreport_excel($param1){
		// create file name
		$fileName = 'AttendedReport'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		
                 $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Event Title');
				 $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Speaker Name');
				 $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Invited Prescriber');
				 $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Invited Non-Prescriber');
				 $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Total Event');
				 $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'DSM Name');
				 $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'DSM Zone');
				 $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Event Date');
				 $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Event Time');
				 $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Attended - Prescriber');
				 $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Attended - Non-Prescriber');
				 $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Attended Event Doctor');
				
	// set Row
		$rowCount = 2;
	//	echo '<pre>';print_r($info);die;
		foreach ($info as $element) {
			
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['title']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount,  $element['speaker_name']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['prescriber']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['non-prescriber']);
				$manager_code = $element['manager_code'];
				$event = $this->db->query("select COUNT(id) as eventcnt from event where `manager_code`='$manager_code' and `steps`='4' ")->result_array();
				$cntevent = @$event[0]['eventcnt']; 
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $cntevent);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['manager_one_name']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['manager_one_zone']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, date('d-M-Y', strtotime($element['date_time'])));
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, date('g:iA', strtotime($element['date_time'])));
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['event_atten_pre']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['event_atten_nonpre']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['event_atten_names']);
            $rowCount++;  
            }
			
			
		
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/excel/'.$fileName));
	}	

}