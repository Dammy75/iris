<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class event_models extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}

	//$type = pending or scheduled
	function count_meetings($type = NULL){
		$this->db->select('id')
			->from('event');
		if($type == 'pending'){
			$this->db->where(array('steps' => '2'));			
		}else if($type == 'scheduled'){
			$this->db->where(array('steps' => '3'));
			$this->db->or_where(array('steps' => '4'));
		}else{
			$this->db->where(array('steps'=>'2'));
		}
		return $this->db->get()->num_rows();
	}

	//$type = pending or scheduled
	function get_meetings($type = NULL){
		$select = array('event.id','event.title','event.speaker_name','event.manager_code','event.date_time','event.approved_by_zsm','event.approved_by_sm','event.approved_by_admin','event.approved_status','event.link','manager_one.manager_one_name');
		$this->db->select($select)
			->from('event')
			->join('manager_one', 'event.manager_code = manager_one.manager_one_code','LEFT');
		if($type == 'pending'){
			$this->db->where(array('event.steps' => '2'));			
		}else if($type == 'scheduled'){
			$this->db->where(array('event.steps' => '3'));
			$this->db->or_where(array('event.steps' => '4'));
		}else{
			$this->db->where(array('event.steps'=>'2'));
		}
		$this->db->order_by("event.id", "desc");
		return $this->db->get()->result_array();
	}

	function get_scheduled_meetings($postData=null){

		$response = array();
   
		## Read value
		$draw = $postData['draw'];
		$start = $postData['start'];
		$rowperpage = $postData['length']; // Rows display per page
		$columnIndex = $postData['order'][0]['column']; // Column index
		$columnName = $postData['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $postData['order'][0]['dir']; // asc or desc
		$searchValue = $postData['search']['value']; // Search value
   
		## Search 
		$searchQuery = "";
		if($searchValue != ''){
		   $searchQuery = " (title like '%".$searchValue."%' or speaker_name like '%".$searchValue."%' or link like '%".$searchValue."%' or app_username like '%".$searchValue."%' or manager_code like'%".$searchValue."%' ) ";
		}
   
		## Total number of records without filtering
		$this->db->select('count(*) as allcount');
		$records = $this->db->get('event')->result();
		$totalRecords = $records[0]->allcount;
   
		## Total number of record with filtering
		$this->db->select('count(*) as allcount');
		if($searchQuery != '')
		   $this->db->where($searchQuery);
		$records = $this->db->get('event')->result();
		$totalRecordwithFilter = $records[0]->allcount;
   
		## Fetch records
		$this->db->select('*');
		if($searchQuery != '')
		   $this->db->where($searchQuery);
		$this->db->order_by($columnName, $columnSortOrder);
		$this->db->limit($rowperpage, $start);
		$records = $this->db->get('event')->result();
   
		$data = array();
   
		foreach($records as $record ){
   
		   $data[] = array( 
			  "link"=>$record->link,
			  "title"=>$record->title,
			  "speaker_name"=>$record->speaker_name,
			  "manager_code"=>$record->manager_code,
			  "scheduled_date"=>date('d-M-Y', strtotime($row['date_time'])),
			  "scheduled_time"=>date('g:iA', strtotime($row['date_time'])),
			  "approved_by_zsm"=>($row['approved_by_zsm']=='1') ? 'Yes' : '',
			  "approved_by_sm"=>($row['approved_by_sm']=='1') ? 'Yes' : '',
			  "approved_by_admin"=>($row['approved_by_admin']=='1') ? 'Yes' : '',
			  "action"=> "<a href=".site_url('Event/view_meeting/'.$row['id'])."><button type='button' class='btn btn-gradient-info m-1'>View</button></a>"
		   ); 
		}
   
		## Response
		$response = array(
		   "draw" => intval($draw),
		   "iTotalRecords" => $totalRecords,
		   "iTotalDisplayRecords" => $totalRecordwithFilter,
		   "aaData" => $data
		);
   
		return $response; 
	  }

	//$type = pending or scheduled
	function get_meetings_by_id($id){
		$this->db->select('*')
			->from('event')
			->where(array('id'=>$id));
		return $this->db->get()->row_array();
	}

	public function add_records($table_name,$insert_array)
	{
		if (is_array($insert_array)){
			if ($this->db->insert($table_name,$insert_array))
				return true;
			else
				return false;
		}else{
			return false; 
		}

	}
	
	public function update_records($table_name,$update_array,$where_array)
	{
		if (is_array($update_array) && is_array($where_array)) 
		{
			$this->db->where($where_array);
			if($this->db->update($table_name,$update_array))
			{
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function get_records($table_name,$filed_name_array=FALSE,$where_array=FALSE,$single_result=FALSE)
	{
		if(is_array($filed_name_array) && isset($filed_name_array)){
	  		$str=implode(',',$filed_name_array);
			$this->db->select($str);
		}	

		if(is_array($where_array)&& isset($where_array)){
			$this->db->where($where_array);
		}
		$result=$this->db->get($table_name);		

		if($single_result==true && isset($single_result)){
			return $result->row_array();
		}else{
			return $result->result_array();
		}
	}

	public function get_dsm_mobile($eventid){
		return	$this->db->select('manager_one.manager_one_contact')
					->from('event')
					->join('manager_one','event.manager_code = manager_one.manager_one_code')
					->where(array('event.id' => $eventid))->get()->row_array();
		
	}

	// Fetch records
	public function getData($rowno,$rowperpage,$search="") {

		$select = array('event.id','event.title','event.speaker_name','event.manager_code','event.date_time','event.approved_by_zsm','event.approved_by_sm','event.approved_by_admin','event.approved_status','event.link','manager_one.manager_one_name');
		$this->db->select($select)
			->from('event')
			->join('manager_one', 'event.manager_code = manager_one.manager_one_code','LEFT');		
		if($search != ''){
			$this->db->like('event.title', $search);
			$this->db->or_like('speaker_name', $search);
			$this->db->or_like('manager_code', $search);
			$this->db->or_like('link', $search);
			$this->db->or_like('app_username', $search);
		}
		$this->db->where(array('event.steps' => '3'));
		$this->db->or_where(array('event.steps' => '4'));		
		$this->db->order_by("event.id", "desc");
		$this->db->limit($rowperpage, $rowno); 
		return $this->db->get()->result_array();
	}
	
	// Select total records
	public function getrecordCount($search = '') {
	
		$this->db->select('count(*) as allcount');
		$this->db->from('event');
	 
		if($search != ''){
			$this->db->like('event.title', $search);
			$this->db->or_like('speaker_name', $search);
			$this->db->or_like('manager_code', $search);
			$this->db->or_like('link', $search);
			$this->db->or_like('app_username', $search);
		}
		$this->db->where(array('event.steps' => '3'));
		$this->db->or_where(array('event.steps' => '4'));
		$query = $this->db->get();
		$result = $query->result_array();
	 
		return $result[0]['allcount'];
	}

}
?>