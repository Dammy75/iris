<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common_model extends CI_Model 
{
	 public function __construct()
    {
        parent::__construct();
    }
	public function add_records($table_name,$insert_array)
	{
		if (is_array($insert_array)) 
		{
			if ($this->db->insert($table_name,$insert_array))
				return true;
			else
				return false;
		}
		else 
		{
			return false; 
		}
	}
	
	
	public function update_records($table_name,$update_array,$where_array)
	{
		if (is_array($update_array) && is_array($where_array)) 
		{
			$this->db->where($where_array);
			if($this->db->update($table_name,$update_array))
			{				 
				return true;
			}	
			else
			{
				return false;
			}	
		} 
		else 
		{
			return false;
		}
	}
	
	
	public function delete_records($table_name,$where_array)
	{ 
		if (is_array($where_array)) 
		{  
			$this->db->where($where_array);
			if($this->db->delete($table_name))
				return true;
			else
				return false;
		} 
		else 
		{
			return false;
		}
	}
	
	public function get_records($table_name,$filed_name_array=FALSE,$where_array=FALSE,$single_result=FALSE)
	{
		if(is_array($filed_name_array) && isset($filed_name_array))
	  	{
	  		$str=implode(',',$filed_name_array);
			$this->db->select($str);
		}
		
		if(is_array($where_array)&& isset($where_array))
		{
			$this->db->where($where_array);
		}

		$result=$this->db->get($table_name);
		
		if($single_result==true && isset($single_result))
		{
			return $result->row_array();
		} 
		else 
		{
			return $result->result_array();			
		}		
	}
	
	public function get_records_with_sort($table_name,$filed_name_array=FALSE,$where_array=FALSE,$single_result=FALSE,$sort_order=FALSE)
	{
		if(is_array($filed_name_array) && isset($filed_name_array))
	  	{
	  		$str=implode(',',$filed_name_array);
			$this->db->select($str);
		}
		
		if(is_array($where_array)&& isset($where_array))
		{
			$this->db->where($where_array);
		}
		
		if(isset($sort_order) && $sort_order!='')
		{
			$this->db->order_by($sort_order);
		}

		$result=$this->db->get($table_name);
		
		if($single_result==true && isset($single_result))
		{
			return $result->row_array();
		} 
		else 
		{
			return $result->result_array();			
		}		
	}
	
	public function get_records_subcat()
	{
		$this->db->select('wwc_manage_subcategory.*,wwc_manage_category.cat_name');
		$this->db->where('wwc_manage_subcategory.status !=','2');
		$this->db->where('wwc_manage_category.status ','1');
		$this->db->join('wwc_manage_category','wwc_manage_category.cat_id=wwc_manage_subcategory.cat_id');
		$result=$this->db->get('wwc_manage_subcategory');
		return $result->result_array();
	}
	
	public function get_records_testomonial()
	{
		$this->db->select('tbl_testomonial.*,tbl_client.*');
		$this->db->where('tbl_testomonial.status !=','2');
		$this->db->where('tbl_client.status ','1');
		$this->db->join('tbl_client','tbl_client.client_id=tbl_testomonial.client_id');
		$result=$this->db->get('tbl_testomonial');
		//print_r($result->result_array());exit;
		return $result->result_array();
	}
	
	public function get_brand($brand_id)
	{
		$this->db->select('*');
		$this->db->where('tbl_client.client_id',$brand_id);
		$this->db->join('tbl_client','tbl_client.client_id=tbl_testomonial.client_id');
		$result=$this->db->get('tbl_testomonial');
		//print_r($result->result_array());exit;
		return $result->result_array();
	}
	public function createThumb($file_name,$path,$width,$height,$maintain_ratio=FALSE)
	{
		$config_1['image_library']='gd2';
		$config_1['source_image']=$path.$file_name;		
		$config_1['create_thumb']=TRUE;
		$config_1['maintain_ratio']=$maintain_ratio;
		$config_1['thumb_marker']='';
		$config_1['new_image']=$path."thumb/".$file_name;
		$config_1['width']=$width;
		$config_1['height']=$height;
		$this->load->library('image_lib',$config_1);	
		if(!$this->image_lib->resize())
		echo $this->image_lib->display_errors();
	}
	public function get_heading($result)
	{
		//print_r($result);exit;
		$this->db->group_by($result);
		$result1=$this->db->get('tbl_capabilities')->result_array();
		print_r($result1);exit;
	}
	public function get_cap($result)
	{
		//print_r($result);exit;
		$this->db->where('heading_id',$result);
		return $result=$this->db->get('tbl_heading')->result_array();
		//print_r($result);exit;
	}
	public function get_all_rec($result)
	{
		$this->db->select("*");
		$this->db->where('tbl_heading.heading_id',$result);
		$this->db->join('tbl_heading','tbl_heading.heading_id=tbl_capabilities.capability');
		return $cap=$this->db->get('tbl_capabilities')->result_array();
		//print_r($cap);exit;
		//return $subitem_list;
		
		//return $result->result_array();
	}
}
