<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class base_models extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	
	function Custome_quary($quarys) {
		$querya = $this->db->query ( $quarys );
		return $querya->result_array ();
	}

	function CustomeQuary($quarys) {
		$querya = $this->db->query ( $quarys );
		return $querya->result_array ();
	}
	
	function AddValues($TableName, $TableValues) {
		$this->db->insert ( $TableName, $TableValues );
		$insert_id = $this->db->insert_id ();
		return $insert_id;
	}
	
	function RemoveValues($TableName, $wherecondition) {
		$this->db->where ( $wherecondition );
		$this->db->delete ( $TableName );
		return $this->db->affected_rows();
	}
	
	function UpadateValue($TableName, $data, $wherecondition) {
		$this->db->where ( $wherecondition );
		return $this->db->update ( $TableName, $data );
	}
	
	function CustomeUpdateQuary($quarys) {
		$querya = $this->db->query ( $quarys );
	}
	
	function Update_Custome_quary($quarys) {
		$querya = $this->db->query ( $quarys );
	}
	
	function GetAllValues($TableName, $wherecondition = null, $select = "*") {
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		$this->db->from ( $TableName );
		$querys = $this->db->get ();
		return $querys->result_array ();
	}
	
	function GetSingleDetails($TableName, $wherecondition = null, $select = "*") {
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		$this->db->from ( $TableName );
		$querys = $this->db->get ();
		return $querys->row();
	}
	
	public function sendSMS($to_mobileno, $sendmsg) {
        $sender = "SMARTX";
        $smslogid = "1";

        $url = 'http://api.myvaluefirst.com/psms/servlet/psms.Eservice2?';


        $xmlstring = '<?xml version="1.0" encoding="ISO-8859-1"?>
                    <!DOCTYPE MESSAGE SYSTEM "http://127.0.0.1:80/psms/dtd/messagev12.dtd">
                    <MESSAGE VER="1.2">
                    <USER USERNAME="sundaytechllp" PASSWORD="stechvlp"/>
                    <SMS  UDH="0" CODING="1" TEXT="' . $sendmsg . '" PROPERTY="0" ID="' . $smslogid . '">
                    <ADDRESS FROM="' . $sender . '" TO="' . $to_mobileno . '" SEQ="1" TAG="some clientside random data"/>
                    </SMS>
                    </MESSAGE>';


        $data = 'data=' . urlencode($xmlstring) . '&' . 'action=send';

        $objURL = curl_init($url);

        curl_setopt($objURL, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($objURL, CURLOPT_POST, 1);
        curl_setopt($objURL, CURLOPT_POSTFIELDS, $data);
        $retval = trim(curl_exec($objURL));

        curl_close($objURL);
    }
	
	
	// ************************************************** Custome ***********************************************//
	public function add_records($table_name,$insert_array)
	{
		if (is_array($insert_array)){
			if ($this->db->insert($table_name,$insert_array))
				return true;
			else
				return false;
		}else{
			return false; 
		}

	}
	public function get_records($table_name,$filed_name_array=FALSE,$where_array=FALSE,$single_result=FALSE)
	{
		if(is_array($filed_name_array) && isset($filed_name_array)){
	  		$str=implode(',',$filed_name_array);
			$this->db->select($str);
		}	

		if(is_array($where_array)&& isset($where_array)){
			$this->db->where($where_array);
		}
		$result=$this->db->get($table_name);		

		if($single_result==true && isset($single_result)){
			return $result->row_array();
		}else{
			return $result->result_array();
		}
	}
	
	public function update_records($table_name,$update_array,$where_array)
	{
		if (is_array($update_array) && is_array($where_array)) 
		{
			$this->db->where($where_array);
			if($this->db->update($table_name,$update_array))
			{
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function delete_records($table_name,$where_array)
	{ 
		if (is_array($where_array)) 
		{
			$this->db->where($where_array);
			if($this->db->delete($table_name))
				return true;
			else
				return false;
		}else{
			return false;
		}
	}
	//Users types 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
	
	function count_users($type = NULL){
		$this->db->select('id')
			->from('admin')
			->where(array('status'=>'1'));
		if($type != NULL){
			$this->db->where(array('type' => $type));			
		}
		return $this->db->get()->num_rows();
	}
	
	function get_users($select = '*', $id = '', $searchText = '', $page='', $segment='')
    {
        $this->db->select($select);
        $this->db->from('admin');
        if(!empty($searchText)) {
            $likeCriteria = "(username  LIKE '%".$searchText."%'
                            OR  email  LIKE '%".$searchText."%'
                            OR  contact  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if(!empty($id)) {
			$this->db->where(array('id'=>$id));
		}
        $this->db->where('status !=', '2');
        // $this->db->order_by('id', 'DESC');
		if(!empty($page)) {
			$this->db->limit($page, $segment);
		}
        $query = $this->db->get();
        
		if(!empty($id)) {
			$result = $query->row(); 
		}else{
			$result = $query->result();
		}
        return $result;
    }
	
	public function fetch_users($limit, $start) { 
       $this->db->limit($limit, $start); 
       $query = $this->db->get("admin"); 
       if ($query->num_rows() > 0) { 
           foreach ($query->result() as $row) { 
               $data[] = $row; 
           } 
           return $data; 
       } 
       return false; 
   }
	//End Users
		
	//Clients
	function count_clients(){
		return $this->db->select('id')
				->from('tbl_client')
				->where(array('status !='=>'2'))
				->get()
				->num_rows();
	}
	
	function get_clients($select = '', $id = '', $searchText = '', $page='', $segment='',$user_id='')
    {
        $this->db->select($select);
        $this->db->from('tbl_client');
        $this->db->join('business_cat','tbl_client.bcat = business_cat.id','Left');
        $this->db->join('admin','tbl_client.user_id = admin.id','Left');
        if(!empty($searchText)) {
            $likeCriteria = "(tbl_client.name  LIKE '%".$searchText."%'
                            OR  tbl_client.business_name  LIKE '%".$searchText."%'
                            OR  tbl_client.email  LIKE '%".$searchText."%'
                            OR  tbl_client.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if(!empty($id)) {
			$this->db->where(array('tbl_client.id'=>$id));
		}
		if(!empty($user_id)) {
			$this->db->where(array('tbl_client.user_id'=> $user_id ));
		}
        $this->db->where('tbl_client.status !=', '2');
        // $this->db->order_by('id', 'DESC');
		if(!empty($page)) {
			$this->db->limit($page, $segment);
		}
        $query = $this->db->get();
        
		if(!empty($id)) {
			$result = $query->row(); 
		}else{
			$result = $query->result();
		}
        return $result;
    }
	
	public function fetch_clients($limit, $start) { 
       $this->db->limit($limit, $start); 
       $query = $this->db->get("tbl_client"); 
       if ($query->num_rows() > 0) { 
           foreach ($query->result() as $row) { 
               $data[] = $row; 
           } 
           return $data; 
       } 
       return false; 
	}
	//End Clients
		
	//Advertsiment types/status 0=pending, 1=rejected, 2=approved, 3=publish
	function count_adv($type = '0',$id = ''){
		$this->db->select('id')
				->from('tbl_adv')
				->where(array('status'=>$type));
			if($id != ''){
				$this->db->where(array('user_id'=>$id));
			}
		$res = $this->db->get();
		return	$res->num_rows();
	}
	
	function get_adv_data($status,$where){
		return $this->db->query(
						"SELECT ta.*, tcl.business_name as business_name, tcl.address as address, wd.username
						FROM tbl_adv as ta
						LEFT JOIN admin as wd
						ON wd.id = ta.user_id
						LEFT JOIN tbl_client as tcl
						ON tcl.id = ta.client_id
						WHERE ta.status = '$status' $where
						ORDER BY ta.id DESC"
						)->result_array();
	}

	function get_adv_data_pagi($status,$where,$limit,$start,$userlogin){
		// $dataPagi = $this->db->query(
		// 				"SELECT ta.*, tcl.business_name as business_name, tcl.address as address, wd.username
		// 				FROM tbl_adv as ta
		// 				LEFT JOIN admin as wd
		// 				ON wd.id = ta.user_id
		// 				LEFT JOIN tbl_client as tcl
		// 				ON tcl.id = ta.client_id
		// 				WHERE ta.status = '$status' $where
		// 				ORDER BY ta.id DESC"
		// 				);
		// return $dataPagi->result_array();

		$select	= array('ta.*', 'tcl.business_name as business_name','tcl.address as address','wd.username');	
		$this->db->select($select);
		$this->db->from('tbl_adv as ta');
		$this->db->join('admin as wd','ON wd.id = ta.user_id','Left');
		$this->db->join('tbl_client as tcl','ON tcl.id = ta.client_id','Left');
		$this->db->where(array('ta.status' => $status));
		if (!empty ( $userlogin )){
			$this->db->where ( $userlogin );
		}
		$this->db->order_by("ta.id", 'DESC');
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		return $this->db->get()->result_array();
		
	}

	function count_adv_data($status,$where){
		return $this->db->query(
						"SELECT ta.id
						FROM tbl_adv as ta
						LEFT JOIN admin as wd
						ON wd.id = ta.user_id
						LEFT JOIN tbl_client as tcl
						ON tcl.id = ta.client_id
						WHERE ta.status = '$status' $where"
						)->num_rows();
	}
	//End Advertsiment
	
	//Daily Sales Report
	function get_dsr_data($where){
		return $this->db->query("SELECT td.*, wa.username
								FROM tbl_dsr as td
								LEFT JOIN admin as wa
								ON wa.id = td.added_by
								WHERE $where ORDER BY id DESC")->result();
	}
	//Daily Sales Report
	
	//Invoice types 1=approved, 2=pending
	function count_invoice($type = '1',$id = ''){
		$this->db->select('id')
				->from('tbl_adv')
				->where(array('status'=>'3', 'invoice_generate' => $type));
			if($id != ''){
				$this->db->where(array('user_id'=>$id));
			}
		$res = $this->db->get();
		return	$res->num_rows();
	}
	function get_invoice_details($id = ''){
		$select = array('td.id','td.user_id','td.client_id','td.city_id','td.ro_no','td.schedule_type','td.time as timming','td.date','td.bank_name','td.pay_method','td.pay_no','td.pay_status','td.amt','td.net_amt','td.net_amt','td.invoice_no','td.invoice_date','td.supplier_ref',
						'wa.fname','wa.lname','tc.gst_no','tc.name as clientName','tc.business_name','tc.address');		
		$where = array('td.id' => $id);
		$data = $this->db->select($select)
					->from('tbl_adv as td')
					->join('admin as wa','ON td.user_id = wa.id','Left')
					->join('tbl_client as tc','ON td.client_id = tc.id','Left')
					->where($where)
					->get()->row();
		return	$data;
	}
	//End Invoice

	//Watcher dashboard 
	function getTodaysBooking($time,$cityid,$date){
		return $this->db->query("SELECT tb.schedule_type, tc.business_name, wd.username, ta.status, ta.image
								FROM tbl_booking as tb
								LEFT JOIN tbl_adv as ta 
								ON tb.adv_id = ta.id
								LEFT JOIN tbl_client as tc
								ON tc.id = ta.client_id
								LEFT JOIN admin as wd
								ON wd.id = ta.user_id
								WHERE tb.city_id = $cityid
								AND tb.time = '$time'
								AND tb.date = '$date'
								AND (ta.status = '2' OR ta.status = '0')")->row();
	}
	
	//Admin Dashboard
	// count regular  client
	function regularClient(){
		$fdate = date('Y-m-01');
		$todate = date('Y-m-d');
		$this->db->select(array('tc.id', 'tc.business_name', 'COUNT(ta.client_id) as regular'))
				->from('tbl_client as tc')
				->join('tbl_adv as ta','ON ta.client_id = tc.id','Left')
				->where(array('ta.date >='=> "$fdate", 'ta.date <='=> "$todate",'tc.status' => '1','ta.status !=' => '4'))
				->group_by('ta.client_id')
				->having(array('COUNT(ta.client_id) >' => '1'));
		$res = $this->db->get();
		return	$res->num_rows();
		//return	$res->result_array();
	}
	
	function nonRegularClient(){
		$fdate = date('Y-m-01');
		$todate = date('Y-m-d');
		$this->db->select(array("tc.id", "tc.business_name",'(SELECT COUNT(client_id) from tbl_adv where client_id = tc.id AND date BETWEEN "$fdate" AND "$todate" AND status != "4") as nonregular'))
				->from('tbl_client as tc')
				->where(array('tc.status !=' => '2'))
				->having(array('nonregular <=' => '1'));
		$res = $this->db->get();
		return	$res->num_rows();
		//return	$res->result_array();
	}
	
	function newClientsInCurrentmonth(){
		$fdate = date('Y-m-01');
		$todate = date('Y-m-d',strtotime(date('Y-m-d') . ' +1 day'));
		$this->db->select(array("id"))
				->from('tbl_client')
				->where(array('created_at >='=> "$fdate", 'created_at <='=> "$todate"));
		$res = $this->db->get();
		return	$res->num_rows();
		//return	$res->result_array();
	}
	
	
	public function get_count($select,$table,$wherecondition = NULL) {
		$this->db->select ( $select );
			  if (isset ( $wherecondition ))
		$this->db->where ( $wherecondition );
  		return $this->db->get( $table )->num_rows();
    // $this->db->last_query(); die;
	}
	public function get_pagination($table,$wherecondition = NULL,$orderby,$limit, $start) {       
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		if(!empty($orderby))
			$this->db->order_by("$orderby", 'DESC');
	/*	$this->db->limit( $limit, $start );*/
		$query = $this->db->get( $table );
		return $query->result_array();

   }
   
   public function get_pagination_data($select,$table,$wherecondition = NULL,$orderby,$limit, $start) {    
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		if(!empty($orderby))
			$this->db->order_by("$orderby", 'DESC');
		$this->db->limit( $limit, $start );
		$query = $this->db->get( $table );
		return $query->result_array();
	}
	
	
		//Super Admin Code	
		function show_speakers(){
		$query = $this->db->get('speakers');
		$query_result = $query->result();
		return $query_result;
		}
		
			
	/*  function get_dsm_doctors($postData){
	       $response = array();
 
    $this->db->select('*');
    $this->db->where('manager_one_code', $postData);
    $q = $this->db->get('doctors');
    $response = $q->result_array();

    return $response;
	      
	  
			}	*/
			
			 function get_dsm_doctor($dsccode){
        $query = $this->db->get_where('doctors', array('manager_one_code' => $dsccode,'flag' => 'Rxber'));
        return $query;
			}
			
			  function get_dsm_doctor1($dsccode1){
        $query = $this->db->get_where('doctors', array('manager_one_code' => $dsccode1,'flag' => 'Non-Rxber'));
        return $query;
			}		
		
		
	function show_events(){
		$query = $this->db->get('event');
		$query_result = $query->result();
		return $query_result;
		}
		//Super Admin Code	

// speaker count
		public function getrecordCountSpeaker($search = '') {
	
		$this->db->select('count(*) as allcount');
		$this->db->from('speakers');
	 
		if($search != ''){
			$this->db->like('speakers.name', $search);
			$this->db->or_like('manager_code', $search);
			$this->db->or_like('sbu_code', $search);
		
		}
		/*$this->db->where(array('event.steps' => '3'));
		$this->db->or_where(array('event.steps' => '4'));*/
		$this->db->where(array('speakers.status' => '1'));
		$query = $this->db->get();
		$result = $query->result_array();
	 
		return $result[0]['allcount'];
	}

	// speaker Fetch records
	public function getSpeakerData($rowno,$rowperpage,$search="") {

		$select = array('speakers.id','speakers.name','speakers.contact','speakers.email','speakers.credential_qualification');
		$this->db->select($select)
			->from('speakers');
	/*		->join('manager_one', 'event.manager_code = manager_one.manager_one_code','LEFT');	*/	
		if($search != ''){
			$this->db->like('speakers.name', $search);
			$this->db->or_like('email', $search);
			$this->db->or_like('contact', $search);
			$this->db->or_like('credential_qualification', $search);
			/*$this->db->or_like('app_username', $search);*/
		}
		/*$this->db->where(array('event.steps' => '3'));
		$this->db->or_where(array('event.steps' => '4'));	*/	
		$this->db->where(array('speakers.status' => '1'));
		$this->db->order_by("speakers.id", "desc");
		$this->db->limit($rowperpage, $rowno); 
		return $this->db->get()->result_array();
	}

		// DSM report count records
		public function getrecordCountDsmEvent($from_date = '',$to_date = '',$search = '') {
	
		$this->db->select('count(*) as allcount');
		$this->db->from('event');
	 
		if($from_date  != '' && $to_date != ''){
			$this->db->where('date_time >=', $from_date);
			$this->db->where('date_time <=', $to_date);
		
		}

		if($search != ''){
			$this->db->like('event.title', $search);
			$this->db->or_like('manager_code', $search);
			$this->db->or_like('speaker_name', $search);
			/*$this->db->or_like('credential_qualification', $search);*/
			/*$this->db->or_like('app_username', $search);*/
		}

		$this->db->where(array('event.steps !=' => '1'));
		/*$this->db->where(array('event.steps' => '2'));
		$this->db->or_where(array('event.steps' => '3'));
		$this->db->or_where(array('event.steps' => '4'));*/
		$query = $this->db->get();
		$result = $query->result_array();
	 
		return $result[0]['allcount'];
	}

// DSM report Fetch records
	public function getDsmEvent($rowno,$rowperpage,$from_date='',$to_date='',$search="") {

		$select = array('event.id','event.title','event.speaker_name','event.manager_code','event.date_time','event.prescriber','event.non-prescriber','manager_one.manager_one_name');
		$this->db->select($select)
			->from('event')
			->join('manager_one', 'event.manager_code = manager_one.manager_one_code','LEFT');
		if($from_date != '' && $to_date !=''){
				$this->db->where('date_time >=', $from_date);
			$this->db->where('date_time <=', $to_date);

		}

		if($search != ''){
			$this->db->like('event.title', $search);
			$this->db->or_like('event.manager_code', $search);
			$this->db->or_like('event.speaker_name', $search);
			$this->db->or_like('manager_one.manager_one_name', $search);
			/*$this->db->or_like('credential_qualification', $search);*/
			/*$this->db->or_like('app_username', $search);*/
		}
		
		$this->db->where(array('event.steps !=' => '1'));
		/*$this->db->or_where(array('event.steps' => '3'));
		$this->db->or_where(array('event.steps' => '4'));*/
		$this->db->order_by("event.id", "desc");
		$this->db->limit($rowperpage, $rowno); 
		return $this->db->get()->result_array();
	}


		// Attended report count records
		public function getrecordCountDsmEventAttended($from_date = '',$to_date = '',$search = '') {
	
		$this->db->select('count(*) as allcount');
		$this->db->from('event');
	 
		if($from_date  != '' && $to_date != ''){
			$this->db->where('date_time >=', $from_date);
			$this->db->where('date_time <=', $to_date);
		
		}

		if($search != ''){
			$this->db->like('event.title', $search);
			$this->db->or_like('manager_code', $search);
			$this->db->or_like('speaker_name', $search);
			/*$this->db->or_like('credential_qualification', $search);*/
			/*$this->db->or_like('app_username', $search);*/
		}

		$this->db->where(array('event.steps !=' => '1'));
		/*$this->db->where(array('event.steps' => '2'));
		$this->db->or_where(array('event.steps' => '3'));
		$this->db->or_where(array('event.steps' => '4'));*/
		$query = $this->db->get();
		$result = $query->result_array();
	 
		return $result[0]['allcount'];
	}

// Attended report Fetch records
	public function getDsmEventAttended($rowno,$rowperpage,$from_date='',$to_date='',$search="") {

		$select = array('event.id','event.title','event.speaker_name','event.manager_code','event.date_time','event.prescriber','event.non-prescriber','manager_one.manager_one_name');
		$this->db->select($select)
			->from('event')
			->join('manager_one', 'event.manager_code = manager_one.manager_one_code','LEFT');
		if($from_date != '' && $to_date !=''){
				$this->db->where('date_time >=', $from_date);
			$this->db->where('date_time <=', $to_date);

		}

		if($search != ''){
			$this->db->like('event.title', $search);
			$this->db->or_like('event.manager_code', $search);
			$this->db->or_like('event.speaker_name', $search);
			$this->db->or_like('manager_one.manager_one_name', $search);
			/*$this->db->or_like('credential_qualification', $search);*/
			/*$this->db->or_like('app_username', $search);*/
		}
		
		$this->db->where(array('event.steps !=' => '1'));
		/*$this->db->or_where(array('event.steps' => '3'));
		$this->db->or_where(array('event.steps' => '4'));*/
		$this->db->order_by("event.id", "desc");
		$this->db->limit($rowperpage, $rowno); 
		return $this->db->get()->result_array();
	}


	// Doctor count
		public function getrecordCountBooking($search = '') {
	
		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_booking');
	 
		if($search != ''){
				$this->db->like('name', $search);
			$this->db->or_like('email', $search);
			$this->db->or_like('mobile', $search);
		
		}
	$this->db->where(array('status !=' => '0'));
		/*$this->db->where(array('event.steps' => '3'));
		$this->db->or_where(array('event.steps' => '4'));*/
		$query = $this->db->get();
		$result = $query->result_array();
	 
		return $result[0]['allcount'];
	}

	// Doctor Fetch records
	public function getBookingData($rowno,$rowperpage,$search="") {
		$select = array('booking_id','name','mobile','email','type','total_people','booking_date_time','status');
		$this->db->select($select)
			->from('tbl_booking');
	/*		->join('manager_one', 'event.manager_code = manager_one.manager_one_code','LEFT');	*/	
		if($search != ''){
			$this->db->like('name', $search);
			$this->db->or_like('email', $search);
			$this->db->or_like('mobile', $search);
			/*$this->db->or_like('sbu_code', $search);*/
			/*$this->db->or_like('app_username', $search);*/
		}
		$this->db->where(array('status !=' => '0'));
		/*$this->db->or_where(array('event.steps' => '4'));	*/	
		$this->db->order_by("booking_id", "desc");
		$this->db->limit($rowperpage, $rowno); 
		return $this->db->get()->result_array();
	}
}
?>