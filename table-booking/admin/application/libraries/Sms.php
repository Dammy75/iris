<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sms
{
    private $_CI;
    public function __construct()
    {
        $this->_CI = & get_instance();
        $this->_CI->load->database();
        $this->_CI->load->model('event_models','em',true);
    }

    public function send_sms($eventid){      
        $eventDetails = $this->_CI->em->get_event_details($eventid);
        $smsToSend = array();

        switch($eventDetails['added_by']){
          //if added by Admin
          case 1 :
            $msg = 'ZSM/SM not found because Event was added by SuperAdmin';
            break;
          
          //if added by DSM/PDSM
          case 2 :
            $dsm_id = $eventDetails['ref_id'];
            $zsmCode = $this->_CI->db->query("SELECT manager_two_code
                      FROM manager_one
                      WHERE id = $dsm_id")->row()->manager_two_code;
            $zsmMob = $this->_CI->db->query("SELECT manager_two_contact, sm_code FROM `manager_two` WHERE manager_two_code = $zsmCode")->row();
            
            if(empty($zsmMob->manager_two_contact) || $zsmMob->manager_two_contact == '0'){
              $msg = 'ZSM mob not found';
              break;
            }
            array_push($smsToSend,$zsmMob->manager_two_contact); //push zsm mob

            $sm_code = $zsmMob->sm_code; // get sm code
            if(!empty($sm_code) || $sm_code == '0'){
              $smMob = $this->_CI->db->query("SELECT mt.manager_two_contact, mt.id
                      FROM  manager_two as mt
                      WHERE mt.manager_two_code = $sm_code")->row()->manager_two_contact;
              array_push($smsToSend,$smMob); //push sm mob
            }
            break;  
          
          //if added by ZSM
          case 5 :
            $zsm_id = $eventDetails['ref_id'];
            $zsmMob = $this->_CI->db->query("SELECT manager_two_contact, sm_code FROM `manager_two` WHERE id = $zsm_id")->row();
            
            // if(empty($zsmMob->manager_two_contact) || $zsmMob->manager_two_contact == '0'){
            //   $msg = 'ZSM mob not found';
            //   break;
            // }
            if(!empty($zsmMob->manager_two_contact) || $zsmMob->manager_two_contact != '0'){
                array_push($smsToSend,$zsmMob->manager_two_contact); //push zsm mob
            }

            $sm_code = $zsmMob->sm_code; // get sm code
            if(!empty($sm_code) || $sm_code == '0'){
              $smMob = $this->_CI->db->query("SELECT mt.manager_two_contact, mt.id
                      FROM  manager_two as mt
                      WHERE mt.manager_two_code = $sm_code")->row()->manager_two_contact;
              array_push($smsToSend,$smMob); //push sm mob
            }
            break;
          
          //if added by SM
          case 6 :
            $sm_id = $eventDetails['ref_id'];
            $smMob = $this->_CI->db->query("SELECT manager_two_contact, sm_code FROM `manager_two` WHERE id = $sm_id")->row();
            
            if(empty($smMob->manager_two_contact) || $smMob->manager_two_contact == '0'){
              $msg = 'SM mob not found';
              break;
            }
            array_push($smsToSend,$smMob->manager_two_contact); //push sm mob
            break;

          //if id missed matched
          default :
            $msg = 'Mobile numbers not found';
            break;
        }
        
        if(!empty($smsToSend)){
            $smsMsg = 'Dear Manager, The NextGen E-meets event has been created in the portal. Please login into www.nextgenmeets.in to approve the same';
            $smsToSend = array('9175917762');
            $sms_res = $this->message_send_bulk($smsMsg, $smsToSend);
            return json_encode(array('status'=>'success','msg'=>'SMS Send Successfully..!'));
        }else{
            return json_encode(array('status'=>'error','msg'=>"$msg"));
        }
    }

    public function message_send_bulk($message1, $number) {
        $numsStr = implode(',',$number);
        // \"sender\": \"MHCHES\", 
        $postData = "{ 
          \"sender\": \"NXTGEN\", 
          \"route\": \"4\", 
          \"country\": \"91\", 
          \"unicode\": \"1\",
          \"sms\": [ 
                { 
                  \"message\": \"$message1\", 
                  \"to\": [ $numsStr ] 
                }
              ] 
        }";
          
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $postData,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_HTTPHEADER => array(
          // "authkey: 164123Aby7N2r6qn5f2915b4P1",
          "authkey: 228445AqdIYICptZd5d36a66d",
          "content-type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          "cURL Error #:" . $err;
        } else {
          $response;
        }
    }

}

?>