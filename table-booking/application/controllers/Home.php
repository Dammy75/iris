<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public function __construct()
	{
		parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
        
        //user_type 7 = event team user
		
	//	$this->load->model('common_model','em',true);
		$this->load->library('form_validation');
		$this->load->library('session');		
		$this->load->helper('cookie');			
		$this->load->helper('url');
		$this->load->helper('form');
		//$this->load->model('common_model');
		$this->load->database();
		
	}
	public function index()
	{
		$this->load->view('index');
	}
	
	public function get_otp()
	{
			$mobile=$_POST['mobile'];
			$otp=rand(100000, 999999);
			$otpmsg = "Your OTP For IRIS Premiere Booking Is $otp"; 
			//$curl = curl_init();
				
			$postData = "{
				\"sender\": \"IRISBK\",
				\"route\": \"4\",
				\"country\": \"91\",
				\"unicode\": \"1\",
				\"sms\": [
				{
				\"message\": \"$otpmsg\",
				\"to\": [ $mobile ]
				}
				]
				}";
				
			$curl = curl_init();
				curl_setopt_array($curl, array(
				 CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
				 CURLOPT_RETURNTRANSFER => true,
				 CURLOPT_ENCODING => "",
				 CURLOPT_MAXREDIRS => 10,
				 CURLOPT_TIMEOUT => 30,
				 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				 CURLOPT_CUSTOMREQUEST => "POST",
				 CURLOPT_POSTFIELDS => $postData,
				 CURLOPT_SSL_VERIFYHOST => 0,
				 CURLOPT_SSL_VERIFYPEER => 0,
				 CURLOPT_HTTPHEADER => array(
				"authkey: 228445AqdIYICptZd5d36a66d",
				"content-type: application/json"
				 ),
				));
				
		

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) 
			{
				echo "cURL Error #:" . $err;
			} 
			else 
			{
				//print_r($response);exit;
				$response_array=array();
				$response_array=array('result'=>$response,'otp'=>$otp);
				$user_data = array(
                   'mobile'       => $mobile,
                   'otp'          => $otp
               );
				$this->session->set_userdata($user_data);   
				echo json_encode($response_array);
			}
			//$this->send_otp($mobile,$otp);
	}



	public function message_send_bulk($message1, $number) {
	$postData = "{
				\"sender\": \"IRISBK\",
				\"route\": \"4\",
				\"country\": \"91\",
				\"unicode\": \"1\",
				\"sms\": [
				{
				\"message\": \"$message1\",
				\"to\": [ $number ]
				}
				]
				}";
				
			$curl = curl_init();
				curl_setopt_array($curl, array(
				 CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
				 CURLOPT_RETURNTRANSFER => true,
				 CURLOPT_ENCODING => "",
				 CURLOPT_MAXREDIRS => 10,
				 CURLOPT_TIMEOUT => 30,
				 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				 CURLOPT_CUSTOMREQUEST => "POST",
				 CURLOPT_POSTFIELDS => $postData,
				 CURLOPT_SSL_VERIFYHOST => 0,
				 CURLOPT_SSL_VERIFYPEER => 0,
				 CURLOPT_HTTPHEADER => array(
				"authkey: 228445AqdIYICptZd5d36a66d",
				"content-type: application/json"
				 ),
				));
				
		

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
		   "cURL Error #:" . $err;
		} else {
		  $response;
		}
	}

	public function get_data()	
	{
		$mobile  =   $this->session->userdata('mobile');
		$otp  =   $this->session->userdata('otp');
				//print_r($mobile);print_r($otp);exit;
		
			$insert_arr	=	array(
									'booking_date_time'   	=> date('Y-m-d H:i:s', strtotime($this->input->post('date').''.$this->input->post('time_slot'))),
									'total_people'   		=> $this->input->post('people'),
									'mobile'                => $mobile,
									'email'                 => $this->input->post('email') ,
									'name'                  => $this->input->post('name') ,
									'type'   				=> $this->input->post('type'),
									'otp'   				=> $otp,
									'inserted_on'			=> date("Y-m-d H:i:s")
								);
								//print_r($insert_arr);exit;
			if($this->db->insert("tbl_booking",$insert_arr))
			{
				redirect(base_url('index.php/home/success'));
				$this->session->set_flashdata('success','Data is Added successfully');
			}else
			{
				$this->session->set_flashdata('error','Not added Please try again');
			}
	
		
		//$this->load->view('index');
	}
	public function success()
	{
		$this->load->view('success');
	}
	public function random_number()
	{
		$digits = 6;
		$number= rand(pow(10, $digits-1), pow(10, $digits)-1);
		return $number;
	}
	
	public function validate()
	{

		
		
		$updated_date_time = date('Y-m-d H:i:s', strtotime($this->input->post('date').''.$this->input->post('time_slot'))); 	
	    $total_people      = $this->input->post('people');
	    $type              = $this->input->post('type');
	    $mno               = $this->session->userdata('mobile');

		
    
		$data = $this->db->query("select sum(total_people) AS NumberOfPeople from tbl_booking  where`booking_date_time`='$updated_date_time' and  `type`='$type' and  `status`='1'")->result_array();
	
			
       $cntdata = @$data[0]['NumberOfPeople'];
       $totalpeoplecnt =$cntdata +  $total_people;

       if($type =='1' && $cntdata > '56'){
               echo "Seats Are Full At That Time Slot. Kindly Change Time Slot.";
               return FALSE;

     }else if($type =='1' && $totalpeoplecnt  > '56'){
               echo "Seats Are Full At That Time Slot. Kindly Change Time Slot Or number of people.";
               return FALSE;

     }else if($type =='2' && $cntdata > '70' || $type =='2' && $totalpeoplecnt  > '70'){
               echo "Seats Are Full At That Time Slot. Kindly Change Time Slot.";
               return FALSE;

     }else {
          $data1 = $this->db->query("select booking_id from tbl_booking  where`booking_date_time`='$updated_date_time' and  `mobile`='$mno' and  `status`='1'")->result_array(); 
          $isrecord = count($data1);
          if($isrecord>0){
          	  echo "You Are Already Registred For This Time Slot.";
 					return FALSE;
          }else{
          //	echo $this->session->set_flashdata('success','Booking is updated successfully');
          	$name = $this->input->post('name');
          	$type = $this->input->post('type');
          	if($type =='1'){
          		$typename = "Elements";
          	}
          	else{
				$typename = "EGO Italian";
          	}
  
          	$booking_time =  date('Y-m-d g:iA', strtotime($updated_date_time));
		    $msg = "Dear $name, You Have Successfully Done Booking At the IRIS Premiere. Booking Type -  $typename, Time :$booking_time, No. Of people:$total_people ";
			$this->message_send_bulk($msg, $mno);

			 $adminmno = "7350001552 ";	
			  $adminmno1 = "9011595000 ";	
		    $adminmsg = "$name, Have Successfully Done Booking At the IRIS Premiere. Booking Type -  $typename, Time :$booking_time, No. Of people:$total_people Mobile:  $mno";
			$this->message_send_bulk($adminmsg, $adminmno);
				$this->message_send_bulk($adminmsg, $adminmno1);
          	echo "Booking is Added Successfully.";
          			return TRUE;
          }
     }

	}
}
