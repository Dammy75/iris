<!DOCTYPE html>
<html lang="en">


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<!-- Basic page needs
	============================================ -->
	<title>IRIS Booking System</title>
	<meta charset="utf-8">
	<meta name="keywords" content="html5 template, best html5 template, best html template, html5 basic template, multipurpose html5 template, multipurpose html template, creative html templates, creative html5 templates" />
	<meta name="description" content="PortKey is a beautiful and creative travel booking HTML template for any travel designs" />
	<meta name="author" content="Magentech">
	<meta name="robots" content="index, follow" />
<!-- Mobile specific metas
	============================================ -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Favicon
	============================================ -->
	<link rel="icon" href="<?php echo base_url();?>assets/image/favicon.png" type="image/gif" sizes="16x16">
<!-- Libs CSS
	============================================ -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css">
	<link href="<?php echo base_url();?>assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/js/owl-carousel/owl.carousel.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/themecss/lib.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/js/minicolors/miniColors.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/js/slick-slider/slick.css" rel="stylesheet">
<!-- Theme CSS
	============================================ -->
	<link href="<?php echo base_url();?>assets/css/themecss/so-listing-tabs.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/themecss/so-newletter-popup.css" rel="stylesheet">
	<link id="color_scheme" href="<?php echo base_url();?>assets/css/home2.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/themecss/so_sociallogin.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/themecss/so_searchpro.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/themecss/so_megamenu.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/themecss/so-categories.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/themecss/so-listing-tabs.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/themecss/so-category-slider.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/themecss/so-newletter-popup.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/footer/footer1.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/header/header2.css" rel="stylesheet">
	<link id="color_scheme" href="<?php echo base_url();?>assets/css/theme.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/quickview/quickview.css" rel="stylesheet">
<!-- Google web fonts
	============================================ -->
	<link href="../../../fonts.googleapis.com/css1fd7.css?family=Roboto:400,500,700" rel="stylesheet" type="text/css">
	<link href="../../../fonts.googleapis.com/cssa441.css?family=Libre+Franklin:400,500,600,700,800&amp;display=swap" rel="stylesheet">
	<style type="text/css">
	body{font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;}
		.typeheader-2 .megamenu-style-dev .horizontal ul.megamenu > li > a strong {
    font-weight: 800 !important;
    color: #191919 !important;
} 
</style>
</head>

<body class="account res layout-1 layout-subpage">

	<div id="wrapper" class="wrapper-fluid banners-effect-10">


		<!-- Header Container  -->
		
		<header id="header" class="typeheader-2">
			<!-- Header Top -->
			<div class="header-top hidden-compact">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-xs-3 header-logo pull-left">
							<div class="navbar-logo">
								<a href="http://www.iloveahmednagar.com/irispremiere/"><img src="<?php echo base_url();?>assets/image/logo/iris_logo.png" alt="Your Store" width="100" height="75" title="Your Store"></a>
							</div>
						</div>
						
						<div class="book pull-right">
							<a href="#">INR R</a>
						</div>
						<div class="bonus-login pull-right">
							<a href="#"><i class="fa fa-power-off"></i></a>
						</div>
						<!-- Menuhome -->
						<div class="header-menu pull-right">
							<div class="megamenu-style-dev megamenu-dev">
								<div class="responsive">
									<nav class="navbar-default">
										<div class="container-megamenu horizontal">
											<div class="navbar-header">
												<button type="button" id="show-megamenu" data-toggle="collapse" class="navbar-toggle">
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</button>
											</div>
											<div class="megamenu-wrapper">
												<span id="remove-megamenu" class="fa fa-times"></span>
												<div class="megamenu-pattern">
													<div class="container">
														<ul class="megamenu" data-transition="slide" data-animationtime="500">
															<li class="style-page with-sub-menu hover">
																<p class="close-menu"></p>
																<a href="http://www.iloveahmednagar.com/irispremiere/" class="clearfix">
																	<strong>
																		Home
																	</strong>
																</a>
															</li>
															<li class="style-page with-sub-menu hover">
																<p class="close-menu"></p>
																<a href="http://www.iloveahmednagar.com/irispremiere/rooms" class="clearfix">
																	<strong>
																		Rooms
																	</strong>
																</a>
															</li>
															<li class="style-page with-sub-menu hover">
																<p class="close-menu"></p>
																<a  class="clearfix">
																	<strong>
																		Dinning
																	</strong>
																	<span class="labelwordpress"></span>
																</a>
																<div class="sub-menu">
																	<div class="content">
																		<div class="row">
																			<div class="col-md-12">
																			<a class="subcategory_item" href="http://www.iloveahmednagar.com/irispremiere/element-ego">Elements and Ego</a>
																				
																			</div>
																		</div>
																	</div>
																</div>
															</li>
															<li class="style-page with-sub-menu hover">
																<p class="close-menu"></p>
																<a href="http://www.iloveahmednagar.com/irispremiere/banquet" class="clearfix">
																	<strong>
																		Banquet
																	</strong>
																</a>
															</li>
															<li class="style-page with-sub-menu hover">
																<p class="close-menu"></p>
																<a href="http://www.iloveahmednagar.com/irispremiere/amenities" class="clearfix">
																	<strong>
																		Amenities
																	</strong>
																</a>
															</li>
															<li class="style-page with-sub-menu hover">
																<p class="close-menu"></p>
																<a href="http://www.iloveahmednagar.com/irispremiere/explore" class="clearfix">
																	<strong>
																		Explore
																	</strong>
																</a>
															</li>
															<li class="style-page with-sub-menu hover">
																<p class="close-menu"></p>
																<a href="http://www.iloveahmednagar.com/irispremiere/promotions" class="clearfix">
																	<strong>
																		Promotions
																	</strong>
																</a>
															</li>
															<li class="style-page with-sub-menu hover">
																<p class="close-menu"></p>
																<a href="http://www.iloveahmednagar.com/irispremiere/contact" class="clearfix">
																	<strong>
																		Contact
																	</strong>
																</a>
															</li>
															
															
															</ul>
													</div>
												</div>
											</div>
										</div>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- //Header Top -->
		</header><!-- //Header Container  -->
		<!-- //Header Container  -->

			<div id="content">
			<div class="so-page-builder">
				<div class="page-builder-ltr">
					<div class="row row_a90w row-style">
						<!--- SLider right--> 
						<div class="module sohomepage-slider so-homeslider-ltr">
							<div class="modcontent">
								<div id="sohomepage-slider2" class="slider-home2">
									<div class="so-homeslider yt-content-slider full_slider owl-drag" data-rtl="yes" data-autoplay="yes" data-autoheight="no" data-delay="4" data-speed="0.6" data-items_column00="1" data-items_column0="1" data-items_column1="1" data-items_column2="1"  data-items_column3="1" data-items_column4="1" data-arrows="yes" data-pagination="yes" data-lazyload="yes" data-loop="yes" data-hoverpause="yes">
										<div class="item">
											<a href="#" title="slide 1 - 1" >
												<img class="responsive" src="<?php echo base_url();?>assets/image/Slider1.png" alt="slide 1 - 1">
											</a>
										</div>
										<div class="item">
											<a href="#" title="slide 1 - 2" >
												<img class="responsive" src="<?php echo base_url();?>assets/image/Slider2.png" alt="slide 1 - 1">
											</a>
										</div>
											<div class="item">
											<a href="#" title="slide 1 - 3" >
												<img class="responsive" src="<?php echo base_url();?>assets/image/Slider3.png" alt="slide 1 - 1">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- //Main Container -->

<div id="content">
			<div class="so-page-builder">
				<div class="page-builder-ltr">
					<div class="row row_a90w row-style">
						<h3 style="color:green;text-align: center;"><b>Booking is Added Successfully At IRIS Premiere.Kindly check SMS Notification.</b></h3>
					</div>
				</div>
			</div>
</div>

		<!-- Footer Container -->
		<footer class="footer-container typefooter-1" style="background-color:#333;">
			<div class="footer-has-toggle" id="collapse-footer">
				<div class="so-page-builder">
					<div class="container-fluid page-builder-ltr">
						<div class="row row_mvtd footer--center2 row-color ">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_6fdl float_none container">
								<div class="row row_hwmc  ">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 col_6ps1 footer--link">
										<p style="color:white;text-transform: uppercase;font-weight: 600;letter-spacing: 1px;word-spacing: 4px;">ABOUT US</p>
										<p style="color:white;text-align:justify;">Centrally located Iris Premiere is situated in close proximity of 5 minutes from Ahmednagar station & bus terminal. Well connected to major cities in western India including Mumbai, Pune, Aurangabad , Shirdi & Shingnapur Iris offers a world of comfort & convenience.</p>						
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 col_6ps1 footer--link" style="color:white;">
										<p style="text-transform: uppercase;font-weight: 600;letter-spacing: 1px;word-spacing: 4px;color:white;">IRIS PREMIERE</p>
										<i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;<span class="ab-pd">Opp Anand Rishiji Hospital, Station Road, Ahmednagar-414001, Maharashtra. India.</span><br>
										<i class="fa fa-phone" aria-hidden="true"></i>&nbsp;<span class="ab-pd">+91 90111 37000</span><br>
										<i class="fa fa-mobile" aria-hidden="true"></i>&nbsp;<span class="ab-pd">+91 241 2327000</span><br>
										<i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;<span class="ab-pd">danish@onevoicetransmedia.com</span>
										
										
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 col_6ps1 footer--link">
										<p style="text-transform: uppercase;font-weight: 600;letter-spacing: 1px;word-spacing: 4px;color:white;">FIND US</p>
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.5982501700364!2d74.73033961485136!3d19.081393487084867!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bdcb0f7c80fffff%3A0xcddedd274f08707a!2sIris%20Premiere!5e0!3m2!1sen!2sin!4v1601540808665!5m2!1sen!2sin" width="427" height="151" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="footer-bottom" style="background-color:#000000;">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 copyright">
							<span style="font-size:16px;color:white;">© 2020 Iris Premiere All rights reserved</span>
						</div>
						<div class="col-md-6 col-sm-6 social">
							<a href="http://www.iloveahmednagar.com/irispremiere/feed/"><img src="<?php echo base_url();?>assets/image/rss_feed.png"></a>
							&nbsp;<a href="http://www.iloveahmednagar.com/irispremiere/contact"><span style="color:white;font-size:16px;">Contact</span></a>
							&nbsp;<a href="http://www.iloveahmednagar.com/irispremiere/legal-notices"><span style="color:white;font-size:16px;">Legal notices</span></a>
							&nbsp;<a href="http://www.iloveahmednagar.com/irispremiere/sitemap"><span style="color:white;font-size:16px;">Sitemap</span></a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- //end Footer Container -->

	</div>

	<!-- <div class="back-to-top"><i class="fa fa-angle-up"></i></div>  -->

<!-- Include Libs & Plugins
	============================================ -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/themejs/so_megamenu.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/owl-carousel/owl.carousel.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/slick-slider/slick.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/themejs/libs.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/unveil/jquery.unveil.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/countdown/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/datetimepicker/moment.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/modernizr/modernizr-2.6.2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/minicolors/jquery.miniColors.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nav.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/quickview/jquery.magnific-popup.min.js"></script>
		<!-- Theme files
			============================================ -->
			<script type="text/javascript" src="<?php echo base_url();?>assets/js/themejs/application.js"></script>
			<script type="text/javascript" src="<?php echo base_url();?>assets/js/themejs/homepage.js"></script>
			<script type="text/javascript" src="<?php echo base_url();?>assets/js/themejs/custom_h1.js"></script>
			<script type="text/javascript" src="<?php echo base_url();?>assets/js/themejs/nouislider.js"></script>

		</body>
		
</html>

<script>
	function get_otp_fun()
{
	document.getElementById("btn_booking").disabled = true;
	//alert(document.getElementById("book_phone").value);
   // setTimeout(function(){document.getElementById("btn_get_otp").disabled = true;},5000);
    mobile=document.getElementById("book_phone").value;
    var valid_phone=/^[789]\d{9}$/;
    //alert(mobile);
    if(mobile=="")
	{
		book_phone_error="Phone no must be filled out";
		document.getElementById("book_phone_error").innerHTML = book_phone_error;
	}
	else if(!mobile.match(valid_phone))
	{
		alert('hi');
		book_phone_error="Enter 10 digits mobile no.";
		document.getElementById("book_phone_error").innerHTML = book_phone_error;
	}
	else
	{
        //console.log(mobile);
        $.ajax({
            url:'<?php echo base_url();?>index.php/home/get_otp',
            data:{'mobile':mobile},
            type:"POST",
            success:function(data)
            {
				da=jQuery.parseJSON(data);   
                $('#otp_val').val(da.otp);
                console.log(da);
            }
        });
	}
}
function verify_otp(val)
{
    otp=$('#otp_val').val();
    if(val==otp)
    {
        $('#book_otp_error').css("color", "green");
        $('#book_otp_error').html('Your OTP is verified successfully.');
        document.getElementById("btn_booking").disabled = false;
        document.getElementById("step1").disabled = true;
        document.getElementById("step2").disabled = false;
		
        //alert('OTP Verify successfully');
    }else{
        $('#book_otp_error').css("color", "red");
        $('#book_otp_error').html('Please enter valid OTP');
    }
    
}
 $(document).ready(function(){
	 document.getElementById("btn_booking").disabled = true;
			$("#btn_booking").click(function(){
				$( "#step1" ).hide();
				$( "#step2" ).show();
			});
			$("#btn_final_booking").click(function(){
				$( "#step2" ).hide();
				$( "#step3" ).show();
			});
		});

</script>