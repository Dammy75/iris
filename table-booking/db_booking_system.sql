-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2020 at 11:58 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_booking_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `fname` varchar(20) DEFAULT NULL,
  `mname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL COMMENT '123456',
  `email` varchar(50) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `type` enum('1','2') NOT NULL COMMENT '1=SuperAdmin,2=Admin',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active, 2=deleted',
  `inserted_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL,
  `recent_login` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `fname`, `mname`, `lname`, `username`, `password`, `email`, `contact`, `profile_pic`, `type`, `status`, `inserted_on`, `updated_on`, `deleted_on`, `recent_login`) VALUES
(1, 'Super Admin', '', NULL, 'superadmin', '123456', 'sadmin@gmail.com', '', '', '1', 1, '2018-11-26 00:00:00', '2019-01-02 12:19:52', '2019-04-29 11:25:22', NULL),
(2, 'Admin', '', NULL, 'admin', '123456', 'admin@gmail.com', '', '', '1', 1, '2018-11-26 00:00:00', '2019-01-02 12:19:52', '2019-04-29 11:25:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_booking`
--

CREATE TABLE `tbl_booking` (
  `booking_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(14) NOT NULL,
  `email` varchar(255) NOT NULL,
  `total_people` int(11) NOT NULL,
  `type` enum('1','2') NOT NULL,
  `booking_date_time` datetime NOT NULL,
  `inserted_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0-Deleted 1-Active 2-payment success',
  `payment_id` varchar(255) NOT NULL,
  `otp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_booking`
--

INSERT INTO `tbl_booking` (`booking_id`, `name`, `mobile`, `email`, `total_people`, `type`, `booking_date_time`, `inserted_on`, `updated_on`, `status`, `payment_id`, `otp`) VALUES
(1, 'Samm sharma', '7894561240', 'fff@gmail.com', 5, '1', '2020-09-30 15:00:00', '2020-09-30 12:18:36', '0000-00-00 00:00:00', '1', '', 0),
(2, '', '9595872327', '', 4, '2', '2020-10-01 11:00:00', '2020-09-30 23:14:37', '0000-00-00 00:00:00', '1', '', 968231),
(3, 'letsupdemo', '9595872327', 'prajutiwari11@gmail.com', 3, '2', '2020-10-05 09:00:00', '2020-09-30 23:40:28', '0000-00-00 00:00:00', '1', '', 730639),
(4, 'Prajakta', '9595872327', 'prajutiwari11@gmail.com', 3, '2', '2020-10-08 11:00:00', '2020-10-01 10:28:31', '0000-00-00 00:00:00', '1', '', 118455),
(5, 'Prajakta', '8668655934', 'prajutiwari11@gmail.com', 4, '1', '2020-10-13 12:00:00', '2020-10-01 12:17:16', '0000-00-00 00:00:00', '1', '', 504633),
(6, 'Prajakta', '9595872327', 'prajutiwari11@gmail.com', 5, '2', '2020-10-21 10:00:00', '2020-10-01 12:50:54', '0000-00-00 00:00:00', '1', '', 820905);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_booking`
--
ALTER TABLE `tbl_booking`
  ADD PRIMARY KEY (`booking_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_booking`
--
ALTER TABLE `tbl_booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
