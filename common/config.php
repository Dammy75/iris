<?php
define('SITE_TITLE', 'IRIS Premiere');
define('TIME_ZONE', 'Asia/Kolkata');
define('DATE_FORMAT', '%e %B %Y');
define('TIME_FORMAT', '%R');
define('CURRENCY_ENABLED', '1');
define('CURRENCY_POS', 'before'); // before or after
define('LANG_ENABLED', '0');
define('ADMIN_LANG_FILE', 'en.ini');
define('ENABLE_COOKIES_NOTICE', '1');
define('MAINTENANCE_MODE', '0');
define('MAINTENANCE_MSG', '<h1><i class=\"fa fa-rocket\"></i> Coming soon...</h1><p>We are sorry, our website is down for maintenance.</p>');
define('TEMPLATE', 'default');
define('OWNER', 'Iris Premiere');
define('EMAIL', 'live@irispremiere.in');
define('ADDRESS', 'Opp Anand Rishiji Hospital, Station Road, Ahmednagar-414001, Maharashtra. India.');
define('PHONE', '+91 90111 37000');
define('MOBILE', '+91 241 2327000');
define('FAX', '');
define('DB_NAME', 'nextgp2c_dbiris');
define('DB_HOST', 'localhost');
define('DB_PORT', '3306');
define('DB_USER', 'root');
define('DB_PASS', '');
define('SENDER_EMAIL', 'support@irispremiere.in');
define('SENDER_NAME', 'Iris Premiere');
define('USE_SMTP', '1');
define('SMTP_SECURITY', '');
define('SMTP_AUTH', '0');
define('SMTP_HOST', 'mail.nextgenmeets.in');
define('SMTP_USER', 'support@irispremiere.in');
define('SMTP_PASS', 'support@123');
define('SMTP_PORT', '587');
define('GMAPS_API_KEY', '');
define('ANALYTICS_CODE', '');
define('ADMIN_FOLDER', 'admin');
define('CAPTCHA_PKEY', ''); // ReCAPTCHA public key
define('CAPTCHA_SKEY', ''); // ReCAPTCHA secret key
define('AUTOGEOLOCATE', '0'); // Change the currency according to the country (https required)
define('PAYMENT_TYPE', 'razorpay,arrival'); // 2checkout,paypal,check,arrival
define('PAYPAL_EMAIL', '');
define('VENDOR_ID', ''); // 2Checkout.com account number
define('SECRET_WORD', ''); // 2Checkout.com secret word
define('PAYMENT_TEST_MODE', '1');
define('ENABLE_DOWN_PAYMENT', '1');
define('DOWN_PAYMENT_RATE', '1'); // %
define('DOWN_PAYMENT_AMOUNT', '1'); // amount required to activate the down payment
define('ENABLE_TOURIST_TAX', '1');
define('TOURIST_TAX', '0');
define('TOURIST_TAX_TYPE', 'fixed');
define('ALLOW_COMMENTS', '1');
define('ALLOW_RATINGS', '1'); // If comments enabled
define('ENABLE_BOOKING_REQUESTS', '0'); // Possibility to make a reservation request if no availability
define('BRAINTREE_MERCHANT_ID', '');
define('BRAINTREE_PUBLIC_KEY', '');
define('BRAINTREE_PRIVATE_KEY', '');
define('CURRENCY_CONVERTER_KEY', ''); // currencyconverterapi.com API key
define('RAZORPAY_KEY_ID', 'rzp_live_py3MbD0n5OhRLU');
define('RAZORPAY_KEY_SECRET', 'ENJeodqzlasjVd');
define('ENABLE_ICAL', '1');
define('ENABLE_AUTO_ICAL_SYNC', '1');
define('ICAL_SYNC_INTERVAL', 'daily'); // daily | hourly
define('ICAL_SYNC_CLOCK', '3'); // 0-23h mode, required if ICAL_SYNC_INTERVAL = daily
