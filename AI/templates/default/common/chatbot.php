<meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8"/>
<link rel= "stylesheet" type= "text/css" href= "/AI/common/css/style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<div class="widget_bot">
    <div class="chat_header">
        <!--Add the name of the bot here -->
        <span style="color:#b4d342;margin-left: 10px;">Iris Premiere Chatbot</span>
        <span style="color:#b4d342;margin-right: 5px;float:right;margin-top: 5px;" id="close">
        <i class="material-icons">close</i>
        </span>
    </div>
    <!--Chatbot contents goes here -->
    <div class="chats" id="chats">
        <div class="clearfix"></div>
    </div>
    <!--user typing indicator -->
    <div class="keypad" >
        <input type="text" id="keypad" class="usrInput browser-default" placeholder="Type a message..." autocomplete="off">
        <div class="badge" style="margin-left: 15%; margin-top: 10px; margin-bottom:10px;">Powered by Automaton AI Infosystems</div>
    </div>
    </div>
    <!--bot widget -->
    <div class="profile_div" id="profile_div">
        <img class="imgProfile" src="/AI/common/images/logo.png"/>
    </div>
</div>

<!--Main Script -->
<!--<script type="text/javascript" src="/iris/AI/common/js/materialize.min.js"></script>-->
<script type="text/javascript" src="/AI/common/js/script.js"></script>
<script>
    $(document).ready(function(){

    $.ajax({
        url: 'https://aichatpro.in:5005/webhooks/rest/webhook',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            "message": "hi",
            "sender": uniqueId
        }),
        success: function (data, textStatus) {
            if(data != null){
                setBotResponse(data);
            }
        },
        error: function (errorMessage) {
            setBotResponse("");
            console.log('Error' + errorMessage);
        }
    });

    });
</script>
