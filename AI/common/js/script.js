var json_data = {}
// on input/text enter--------------------------------------------------------------------------------------
$('.usrInput').on('keyup keypress', function (e) {
	var keyCode = e.keyCode || e.which;
	var text = $(".usrInput").val();
	if (keyCode === 13) {
		if (text == "" || $.trim(text) == '') {
			e.preventDefault();
			return false;
		} else {
			$(".usrInput").blur();
			setUserResponse(text);
			send(text);
			e.preventDefault();
			return false;
		}
	}
});

function generateId(length) {
	var res           = '';
	var chars       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var charLen = chars.length;
	for ( var i = 0; i < length; i++ ) {
	   res += chars.charAt(Math.floor(Math.random() * charLen));
	}
	return res;
 }
var uniqueId = generateId(32)

$.getJSON("https://api.ipify.org?format=json", 
function(data) { 
	json_data['client_ip'] = data.ip;
})  

function hideAndSet(){
	$("#keypad").hide();
	$(".chats").css('height','430px');
	$(".keypad").css('bottom','0px');
	$(".keypad").css('height','40px');
	scrollToBottomOfResults();
}


function getBackToOriginal(){
	$("#keypad").show();
	$(".chats").css('height','400px');
	$(".keypad").css('height','60px');
	$(".keypad").css('bottom','10px');
	scrollToBottomOfResults();
}

//------------------------------------- Set user response------------------------------------
function setUserResponse(val) {


	var UserResponse = '<img class="userAvatar" src=' + "/AI/common/images/botAvatar_2.png" + '><p class="userMsg">' + val + ' </p><div class="clearfix"></div>';
	$(UserResponse).appendTo('.chats').show('slow');
	$(".usrInput").val('');
	scrollToBottomOfResults();
	$('.suggestions').remove();
}

//---------------------------------- Scroll to the bottom of the chats-------------------------------
function scrollToBottomOfResults() {
	var terminalResultsDiv = document.getElementById('chats');
	terminalResultsDiv.scrollTop = terminalResultsDiv.scrollHeight;
}

function send(message) {
	console.log("User Message:", message)
	$.ajax({
		url: 'https://aichatpro.in:5005/webhooks/rest/webhook',
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify({
			"message": message,
			"sender": uniqueId
		}),
		success: function (data, textStatus) {
			if(data != null){
					console.log("Printed" ,data)
					setBotResponse(data);
			}
			console.log("Rasa Response: ", data, "\n Status:", textStatus)
		},
		error: function (errorMessage) {
			setBotResponse("");
			console.log('Error' + errorMessage);

		}
	});
	$("#keypad").focus()
}

//------------------------------------ Set bot response -------------------------------------
function setBotResponse(val) {
	setTimeout(function () {
		if (val.length < 1) {
			//if there is no response from Rasa
			msg = 'I couldn\'t get that. Let\' try something else!';

			var BotResponse = '<img class="botAvatar" src="/AI/common/images/iris_logo.png"><p class="botMsg">' + msg + '</p> <div class="clearfix"></div>';
			$(BotResponse).appendTo('.chats').hide().fadeIn(1000);

		} else {
			//if we get response from Rasa
			for (i = 0; i < val.length; i++) {
				//check if there is text message
				if (val[i].hasOwnProperty("text")) {
					getBackToOriginal();
					var BotResponse = '<img class="botAvatar" src="/AI/common/images/iris_logo.png"><p class="botMsg">' + val[i].text + '</p><div class="clearfix"></div>';
					$(BotResponse).appendTo('.chats').hide().fadeIn(1000,function(){
						scrollToBottomOfResults();
					});
				}

				//check if there is image
				if (val[i].hasOwnProperty("image")) {
					var BotResponse = '<div class="singleCard">' +
						'<img class="imgcard" src="/AI/' + val[i].image + '">' +
						'</div><div class="clearfix">'
					$(BotResponse).appendTo('.chats').hide().fadeIn(1000);
				}


				if (val[i].hasOwnProperty("attachment")) {
					if (val[i].attachment[0].hasOwnProperty("slider_list")){
						$("#myCarousel").remove();
						// $(".carousel-indicators").innerHTML = '';
						var Carousel = '<div id="myCarousel" class="carousel slide" data-ride="carousel" style="width:300px; margin:auto;height:200px;border-radius: 10px;">' +
						'<ol class="carousel-indicators">' +
						'</ol>' +
						'<div class="carousel-inner">' + 
						'</div>' + 
					
						// '<a class="left carousel-control" href="#myCarousel" data-slide="prev">'+
						//    '<span class="glyphicon glyphicon-chevron-left"></span>' +
						//    '<span class="sr-only">Previous</span>' +
						// '</a>' +
						// '<a class="right carousel-control" href="#myCarousel" data-slide="next">' +
						//    '<span class="glyphicon glyphicon-chevron-right"></span>' +
						//    '<span class="sr-only">Next</span>' +
						// '</a>' +
					 '</div>'
					 $(Carousel).appendTo('.chats').hide().fadeIn(1000);
						var BotResponse = ""
						var listRsponse = ""
						var cnt = 0
						for (j in val[i].attachment[0].slider_list){
							if (cnt == 0){
								BotResponse = BotResponse + '<div class="item active"><img src="/AI/' + val[i].attachment[0].slider_list[j].url + '" style="margin:auto;;height:200px;"><div class="carousel-caption"><p>' + val[i].attachment[0].slider_list[j].text + '</p></div></div>'
								listRsponse = listRsponse + '<li data-target="#myCarousel" data-slide-to="'+cnt+'" class="active"></li>'
							}else{
								BotResponse = BotResponse + '<div class="item"><img src="/AI/' + val[i].attachment[0].slider_list[j].url + '" style="margin:auto;height:200px;"><div class="carousel-caption"><p>' + val[i].attachment[0].slider_list[j].text + '</p></div></div>'
								listRsponse = listRsponse + '<li data-target="#myCarousel" data-slide-to="'+cnt+'"></li>'
							}
							cnt = cnt + 1
							
						}
						console.log(BotResponse)	
						$(BotResponse).appendTo('.carousel-inner').hide().fadeIn(1000);
						$(listRsponse).appendTo('.carousel-indicators').hide().fadeIn(1000);

					}
					if (val[i].attachment[0].hasOwnProperty("date_picker")){
						var BotResponse = ""
						BotResponse = BotResponse + '<div class="container-fluid custom-container">' +
						'<div class="row" style="margin-bottom:0;">' +
							  '<div class="col-sm-6">' +
								 '<label for="startdate" ><h6 style="font-size: 12px; color:#333; font-weight: bold;">Check-In Date</h6></label>' +
							  '</div>' +
							  '<div class="col-sm-6">' +
								 '<label for="enddate" ><h6 style="font-size: 12px; color:#333; font-weight: bold;">Check-Out Date</h6></label>' +
							  '</div>' +
						'</div>' +
						'<div class="row">' +
							  '<div class="col-sm-6">' +
								 '<input type="date" name="startdate" id="startdate" class="form-control" style="font-size: 0.8rem">' +
							  '</div>' +
							  '<div class="col-sm-6">' +
								 '<input type="date" name="enddate" id="enddate"  class="form-control" style="font-size: 0.8rem">' +
							  '</div>' +
						'</div>' +
					 '</div>'
						$(BotResponse).appendTo('.chats')
						if (val[i].attachment[0].date_picker.hasOwnProperty("buttons")) {
							hideAndSet();
							addCustomSuggestion(val[i].attachment[0].date_picker.buttons)
							setTimeout(function(){
								$('.suggestions').hide();
							},1000);
						}
						var payload = {}
						today = new Date().toISOString().slice(0, 10)
						document.getElementById("startdate").setAttribute('min', today);
						document.getElementById("enddate").setAttribute('min', today);
						

						//////////////////////////// I was Here ////////////////////////////////
						$('#startdate').on('change',function() {
							payload['startdate']= $("#startdate").val();
							// button_text = button_text + JSON.stringify(payload)
							// $(".menu .menuChips").attr('data-payload',JSON.stringify(payload))
							document.getElementById("enddate").setAttribute('min', $("#startdate").val());
							if (!($("#startdate").val()=='') && !($("#enddate").val()=='')){
								$('.suggestions').show();
								scrollToBottomOfResults();
							}
							if(new Date($('#startdate').val())> new Date($('#enddate').val())){
								$('#enddate').val("");
								$('.suggestions').hide();
								scrollToBottomOfResults();
							}
						 });
						 $('#enddate').on('change',function() {
							payload['enddate']=  $("#enddate").val();
							
							// $(".menu .menuChips").attr('data-payload',JSON.stringify(payload))
							if (!($("#startdate").val()=='') && !($("#enddate").val()=='')){
								$('.suggestions').show();
								scrollToBottomOfResults();
							}
						 });
						
						
						// var text = this.innerText;
						// var payload= this.getAttribute('data-payload');
						
						// on click of suggestions, get the value and send to rasa
						$(document).on("click", ".menu .sendChips", function () {
							button_text = this.getAttribute('data-payload')
							metatext = JSON.parse("{"+button_text.split(/{(.+)/)[1])
							metatext['metadata']=JSON.stringify(payload)
							var intent = button_text.split(/{(.+)/)[0]
							button_text = intent +":"+ JSON.stringify(metatext)
							send(button_text);
							json_data[intent] = button_text
							$('.suggestions').remove(); //delete the suggestions 
						});

					}			
					if (val[i].attachment[0].hasOwnProperty("event_picker")){
						var BotResponse = ""
						BotResponse = BotResponse + '<div class="container-fluid custom-container">' +
						'<div class="row" style="margin-bottom:0;">' +
							  '<div class="col-sm-4" style="margin-top:10px;">' +
								 '<label for="eventdate" ><h6 style="font-size: 12px; color:#333; font-weight: bold;">Event Date</h6></label>' +
							  '</div>' +

							  '<div class="col-sm-8">' +
								 '<input type="date" name="eventdate" id="eventdate" class="form-control" style="font-size: 0.8rem">' +
							  '</div>' +
						'</div>' +
					 '</div>'
						$(BotResponse).appendTo('.chats')
						if (val[i].attachment[0].event_picker.hasOwnProperty("buttons")) {
							hideAndSet();
							addCustomEventSuggestion(val[i].attachment[0].event_picker.buttons)
							setTimeout(function(){
								$('.suggestions').hide();
							},1000);
						}
						var payload = {}
						today = new Date().toISOString().slice(0, 10)
						document.getElementById("eventdate").setAttribute('min', today);
						
						$('#eventdate').on('change',function() {
							payload['eventdate']= $("#eventdate").val();
							if (!($("#eventdate").val()=='')){
								$('.suggestions').show();
								scrollToBottomOfResults();
							}
						 });
						
						// on click of suggestions, get the value and send to rasa
						$(document).on("click", ".menu .sendEventChips", function () {
							button_text = this.getAttribute('data-payload')
							metatext = JSON.parse("{"+button_text.split(/{(.+)/)[1])
							metatext['eventdata']=JSON.stringify(payload)
							var intent = button_text.split(/{(.+)/)[0]
							button_text = intent +":"+ JSON.stringify(metatext)
							send(button_text);
							json_data[intent] = button_text
							console.log(json_data)
							$('.suggestions').remove(); //delete the suggestions 
						});

					}
				}

				//check if there is  button message
				if (val[i].hasOwnProperty("buttons")) {
					hideAndSet();
					addSuggestion(val[i].buttons);

				}

			}
			scrollToBottomOfResults();
		}

	}, 500);
}


// ------------------------------------------ Toggle chatbot -----------------------------------------------
$('#profile_div').click(function () {
	$('.profile_div').toggle();
	$('.widget_bot').toggle();
	scrollToBottomOfResults();
	$("#keypad").focus();
});

$('#close').click(function () {
	$('.profile_div').toggle();
	$('.widget_bot').toggle();
});


// ------------------------------------------ Suggestions -----------------------------------------------

function addSuggestion(textToAdd) {
	setTimeout(function () {
		var suggestions = textToAdd;
		var suggLength = textToAdd.length;
		$(' <div class="singleCard"> <div class="suggestions"><div class="menu"></div></div></diV>').appendTo('.chats').hide().fadeIn(1000);
		// Loop through suggestions
		for (i = 0; i < suggLength; i++) {
			$('<div class="menuChips" data-payload=\''+(suggestions[i].payload)+'\'>' + suggestions[i].title + "</div>").appendTo(".menu");
		}
		scrollToBottomOfResults();
	}, 1000);
}


function addCustomSuggestion(textToAdd) {
	setTimeout(function () {

		var suggestions = textToAdd;
		var suggLength = textToAdd.length;
		$(' <div class="singleCard"> <div class="suggestions"><div class="menu"></div></div></diV>').appendTo('.chats').hide().fadeIn(1000);
		// Loop through suggestions
		for (i = 0; i < suggLength; i++) {
			$('<div class="sendChips" data-payload=\''+(suggestions[i].payload)+'\'>' + suggestions[i].title + "</div>").appendTo(".menu");
		}
		scrollToBottomOfResults();
	}, 1000);
}


function addCustomEventSuggestion(textToAdd) {
	setTimeout(function () {

		var suggestions = textToAdd;
		var suggLength = textToAdd.length;
		$(' <div class="singleCard"> <div class="suggestions"><div class="menu"></div></div></diV>').appendTo('.chats').hide().fadeIn(1000);
		// Loop through suggestions
		for (i = 0; i < suggLength; i++) {
			$('<div class="sendEventChips" data-payload=\''+(suggestions[i].payload)+'\'>' + suggestions[i].title + "</div>").appendTo(".menu");
		}
		scrollToBottomOfResults();
	}, 1000);
}

// on click of suggestions, get the value and send to rasa
$(document).on("click", ".menu .menuChips", function () {
	var text = this.innerText;
	var payload= this.getAttribute('data-payload');
	console.log("button payload: ",this.getAttribute('data-payload'))
	setUserResponse(text);
	send(payload);
	json_data[text] = payload;
	$('.suggestions').remove(); //delete the suggestions 
	console.log("json data", json_data)
});
